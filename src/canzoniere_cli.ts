import {generate} from "./lib_canzoniereonline"
import {Job} from "./utils/types"
import {fileToList} from "./utils/utils"

const help = () => {
    return `
        Canzoniereonline - Command Line Interface

        Usage: canzoniere_cli [OPTIONS] SONGS
        OPTIONS: valid options
        SONGS: space-separated list of song identifiers
    `
}

const run = async (): Promise<Job> => {
    const args = process.argv.slice(2)
    let songs = args.filter(arg => !arg.startsWith("--"))

    const options = args
        .filter(arg => arg.startsWith("--"))
        .map(arg => arg.replace("--", ""))
        .map(arg => (arg.includes("=") ? arg.split("=") : [arg, arg]))
    const optionsObj = Object.fromEntries(options)

    if (optionsObj.help !== undefined) {
        console.log(help())
        return {result: true}
    } else if (optionsObj.file !== undefined) {
        songs = await fileToList(optionsObj.file)
    }

    const result = await generate({
        songs: songs.map(s => {
            const [id, transpose] = s.split("+")
            return {id: id, transpose: transpose ?? "0"}
        }),
        options: optionsObj,
    })
    return result
}
const runAndOutput = async (): Promise<Job> => {
    const result = await run()
    console.log(result)
    return result
}
runAndOutput()
