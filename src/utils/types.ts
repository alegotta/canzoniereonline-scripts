class OperationalError extends Error {
  description: string;
  constructor(message: string, description: string) {
    super(message + ". " + description);
    this.message = message;
    this.description = description;
  }
}
interface SongStatus {
  type: SongType;
  content?: SongContent;
}
type SongType = "sclearpage" | "brk" | "space" | "comment" | "song";
interface SongContent {
  id: string;
  exists: boolean;
  transpose: number;
}
interface Job {
  result: boolean;
}
interface SuccessfulJob extends Job {
  path: string;
}
interface FailedJob extends Job {
  message: string;
  description: string;
}

interface Receipe {
  songs: SongRequest[];
  options: Options;
}
interface SongRequest {
  id: string;
  transpose?: string;
}
interface Options {
  songs: string;
  out: string;
  tex: string;
  format?:
    | "standard"
    | "singolo"
    | "a5vert"
    | "a5oriz"
    | "a6vert"
    | "smart"
    | "slides";
  chords?: boolean;
  font?: "libertinus" | "palatino";
  minorsign?: "-" | "m";
  columns?: string;
  cover?: boolean;
  indexes?: Index[];
  runner?: "pdflatex" | "tectonic";
}
type Index = "title" | "authors" | "tematic";

interface OptionDescriptor {
  name?: string;
  prefix?: string;
  suffix?: string;
  ifTrue?: string;
  ifFalse?: string;
}

interface SongHeader {
  id: string;
  title: string;
  alt_title: string;
  author: string[];
  composer: string[];
  album: string;
  tone: string;
  transpose: string;
  meter: string; // Tipicamente 4/4
  famiglia: string;
  momenti: string[];
  trascrizione: string[];
  revisione: string; // Formato YYYY-MM-DD, TODO fix mancanti
  video: string[];
}
type SongHeaderProp =
  | "id"
  | "title"
  | "alt_title"
  | "author"
  | "composer"
  | "album"
  | "tone"
  | "transpose"
  | "trasposizione"
  | "meter"
  | "famiglia"
  | "momenti"
  | "trascrizione"
  | "revisione"
  | "video";

interface Song {
  header: SongHeader;
  content: string;
}

export {
  OperationalError,
  SongStatus,
  SongType,
  SongContent,
  Job,
  SuccessfulJob,
  FailedJob,
  Receipe,
  SongRequest,
  Options,
  OptionDescriptor,
  Index,
  SongHeader,
  SongHeaderProp,
  Song,
};
