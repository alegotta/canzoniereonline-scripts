import { createReadStream, promises as fsPromises } from "fs";
import readline from "readline";

const readFile = async (
  path: string,
  until: string[] = [],
  exclude: string[] = []
): Promise<[string, string]> => {
  const fileStream = createReadStream(path);
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });
  return await readFileFromStream(rl, until, exclude);
};

const readFileFromStream = async (
  rl: readline.Interface,
  until: string[] = [],
  exclude: string[] = []
): Promise<[string, string]> => {
  const lines: string[] = [];
  const rest: string[] = [];
  let cur = lines;
  for await (const line of rl) {
    if (until.some((elem) => line.includes(elem))) {
      cur = rest;
    } else if (exclude.some((elem) => line.includes(elem))) {
      //Skip this line
    } else {
      cur.push(line);
    }
  }
  return [lines.join("\n"), rest.join("\n")];
};

const fileToList = async (path: string): Promise<string[]> => {
  const [content, _] = await readFile(path);
  return content.trim().split("\n");
};

const writeFile = async (path: string, content: string): Promise<void> => {
  await fsPromises.writeFile(path, content);
};

const getDirContent = async (path: string): Promise<string[]> => {
  return await fsPromises.readdir(path);
};

export { readFile, readFileFromStream, fileToList, writeFile, getDirContent };
