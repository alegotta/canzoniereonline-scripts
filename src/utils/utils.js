"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDirContent = exports.writeFile = exports.fileToList = exports.readFileFromStream = exports.readFile = void 0;
const fs_1 = require("fs");
const readline_1 = __importDefault(require("readline"));
const readFile = async (path, until = [], exclude = []) => {
    if (until.length === 0)
        return [(await fs_1.promises.readFile(path)).toString(), ""];
    const fileStream = (0, fs_1.createReadStream)(path);
    const rl = readline_1.default.createInterface({
        input: fileStream,
        crlfDelay: Infinity,
    });
    return await readFileFromStream(rl, until, exclude);
};
exports.readFile = readFile;
const readFileFromStream = async (rl, until = [], exclude = []) => {
    const lines = [];
    const rest = [];
    let cur = lines;
    for await (const line of rl) {
        if (until.some(elem => line.includes(elem))) {
            cur = rest;
        }
        else if (exclude.some(elem => line.includes(elem))) {
            //Skip this line
        }
        else {
            cur.push(line);
        }
    }
    return [lines.join("\n"), rest.join("\n")];
};
exports.readFileFromStream = readFileFromStream;
const fileToList = async (path) => {
    const [content, _] = await readFile(path);
    return content.trim().split("\n");
};
exports.fileToList = fileToList;
const writeFile = async (path, content) => {
    await fs_1.promises.writeFile(path, content);
};
exports.writeFile = writeFile;
const getDirContent = async (path) => {
    return await fs_1.promises.readdir(path);
};
exports.getDirContent = getDirContent;
