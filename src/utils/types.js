"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperationalError = void 0;
class OperationalError extends Error {
    constructor(message, description) {
        super(message + ". " + description);
        this.message = message;
        this.description = description;
    }
}
exports.OperationalError = OperationalError;
