import {execSync} from "child_process"
import {createHash} from "crypto"
import {promises as fsPromises} from "fs"
import {copyFile} from "fs/promises"
import validator from "validator"
import {
    FailedJob,
    OptionDescriptor,
    Receipe,
    SongHeader,
    SongRequest,
    SongStatus,
    SuccessfulJob,
} from "./utils/types"
import {OperationalError} from "./utils/types.js"
import {readFile, writeFile} from "./utils/utils.js"

const paths = {
    songs: "",
    pdf: "",
    tex: "",
}

let index: SongHeader[] = []

const generate = async (receipe: Receipe): Promise<SuccessfulJob | FailedJob> => {
    receipe = validateReceipe(receipe)
    readPaths(receipe)
    index = await readIndex()
    console.debug("Songs:", receipe.songs)
    console.debug("Options:", receipe.options)
    await createDirs()

    const songs = await getSongsStatus(receipe.songs)
    console.log("Parsed songs:", songs)
    await checkInexistingSongs(songs)
    await copyAndTransposeSongs(songs)
    await writeSongsList(songs)

    try {
        return {result: true, ...(await generatePdf(receipe, songs))}
    } catch (err) {
        console.error(err)
        if (err instanceof OperationalError)
            return {result: false, message: err.message, description: err.message}
        else if (err instanceof Error)
            return {result: false, message: err.name, description: err.message}
        else return {result: false, message: "Error", description: JSON.stringify(err)}
    }
}

const readIndex = async (): Promise<any> => {
    const [indexFile, _] = await readFile(`${paths.songs}/songs.json`)
    return JSON.parse(indexFile)
}

const validateReceipe = (receipe: Receipe): Receipe => {
    const songs = receipe.songs.map(song => {
        if (
            song.transpose !== undefined &&
            !(
                validator.isNumeric(song.transpose) &&
                Number(song.transpose) >= -5 &&
                Number(song.transpose) <= 5
            )
        )
            throw new OperationalError(
                "Invalid transposition",
                `Found ${song.transpose} for song ${song.id}`,
            )
        return {
            transpose: song.transpose,
            id: validator.whitelist(song.id, "[w\n_]+"),
        }
    })
    const columns = receipe.options.columns
    if (
        columns !== undefined &&
        !(validator.isNumeric(columns) && Number(columns) <= 3 && Number(columns) >= 1)
    )
        throw new OperationalError(
            "Invalid number of columns",
            `Expected a number between 1 and 3, found ${columns}`,
        )

    return {songs, options: receipe.options}
}

const readPaths = (receipe: Receipe): void => {
    paths.songs = receipe.options.songs
    paths.pdf = receipe.options.out
    paths.tex = receipe.options.tex
}

const createDirs = async (): Promise<boolean> => {
    const dirs = [paths.pdf, `${paths.pdf}/canzonieri`, `${paths.pdf}/tmp`]
    let alreadyExists = false

    for (const d of dirs) {
        try {
            await fsPromises.mkdir(d)
            alreadyExists = false
        } catch (error) {
            alreadyExists = true
        }
    }
    return !alreadyExists
}

const checkInexistingSongs = async (songs: SongStatus[]): Promise<boolean> => {
    let verified = false

    if (songs.length === 0) throw new OperationalError("Empty songs list", "No song was specified")

    const nonExistingSongs = songs
        .filter(song => song.type === "song" && song.content?.exists === false)
        .map(check => check.content?.id)
    if (nonExistingSongs.length !== 0) {
        const desc = `Invalid songs list. The following were not found: ${nonExistingSongs.join(
            ",",
        )}`
        throw new Error(desc)
    }

    verified = true
    return verified
}

const getSongsStatus = async (songs: SongRequest[]): Promise<SongStatus[]> => {
    return await Promise.all(songs.map(async song => await getSongStatus(song)))
}
const getSongStatus = async (song: SongRequest): Promise<SongStatus> => {
    if (song.id === "sclearpage" || song.id === "brk") return {type: song.id}
    else if (song.id.trim().length === 0) return {type: "space"}
    else if (song.id.startsWith("%"))
        return {
            type: "comment",
            content: {id: song.id, exists: true, transpose: 0},
        }
    else {
        const songInIndex = index.find(songIdx => songIdx.id === song.id)
        if (songInIndex !== undefined)
            return {
                type: "song",
                content: {id: song.id, exists: true, transpose: Number(song?.transpose) ?? 0},
            }
        else return {type: "song", content: {id: song.id, exists: false, transpose: 0}}
    }
}

const copyAndTransposeSongs = async (songs: SongStatus[]): Promise<void> => {
    const songsPath = `${paths.songs}/archivio-canzoni`
    const actualSongs = songs.filter(song => song.type === "song")

    for await (const song of actualSongs) {
        if (song.content!.transpose !== 0) {
            let [songContent, _] = await readFile(`${songsPath}/${song.content!.id}.tex`)
            const matches = songContent.match(
                /([\w\W\n]+)(\\beginsong{[^}]+}(\[[^\]]+\])?)([\w\W\n]+)/,
            )
            if (matches === null)
                throw new OperationalError(
                    "Invalid song content",
                    `Song ${song.content?.id} is not correctly formatted`,
                )
            else
                songContent =
                    matches[1] +
                    matches[2] +
                    "\n\\transpose{" +
                    song.content?.transpose +
                    "}" +
                    matches[4]

            await writeFile(`${paths.pdf}/tmp/${song.content?.id}.tex`, songContent)
        } else {
            copyFile(
                `${songsPath}/${song.content?.id}.tex`,
                `${paths.pdf}/tmp/${song.content?.id}.tex`,
            )
        }
    }
}

const writeSongsList = async (songs: SongStatus[]): Promise<void> => {
    const formattedSongs = songs
        .filter(song => song.type === "sclearpage" || song.type === "brk" || song.type === "song")
        .map(song => {
            if (song.type === "sclearpage") return "\\sclearpage"
            else if (song.type === "brk") return "\\brk"
            else return `\\input{../../${paths.pdf}/tmp/${song.content?.id}.tex}`
        })
        .join("\n")

    await writeFile(`${paths.tex}/_songs_list.tex`, formattedSongs)
}

const generatePdf = async (receipe: Receipe, status: SongStatus[]): Promise<{path: string}> => {
    const runOptions = await buildDocumentClass(receipe)
    let jobName = receipe.songs[0].id
    if (runOptions.format !== "singoli") jobName = `canzoniere_${generateHash(status)}`

    const outDir = runOptions.format === "singoli" ? paths.pdf : `${paths.pdf}/canzonieri`
    let command = `cd ${paths.tex} && pdflatex -shell-escape -jobname ${jobName} -output-directory ../../${outDir} canzoniere.tex`
    if (receipe.options?.runner === "tectonic")
        command = `tectonic --outdir ${outDir} ${paths.tex}/canzoniere.tex`

    console.log(`Executing ${command}`)
    try {
        execSync(command, {timeout: 30 * 1000})
        removeTmpFiles(outDir, jobName)
        return {path: `${outDir}/${jobName}.pdf`}
    } catch (err: any) {
        console.log(err)
        removeTmpFiles(outDir, jobName, false)
        if ("status" in err && err.status === "ETIMEDOUT")
            throw new OperationalError(
                "Timeout reached while running pdflatex",
                "See the logs in the output directory",
            )
        else
            throw new OperationalError(
                "Error while running pdflatex",
                `Error Code: ${err?.status ?? "unkwnown"}. See the logs in the output directory`,
            )
    }
}

const buildDocumentClass = async (receipe: Receipe) => {
    const mapper: Map<string, OptionDescriptor> = new Map([
        ["format", {name: "__value__"}],
        ["chords", {ifTrue: "chorded", ifFalse: "lyric"}],
        ["font", {name: "__value__"}],
        ["minorsign", {prefix: "minorsign=", name: "__value__"}],
        ["columns", {prefix: "columns=", name: "__value__"}],
        ["cover", {ifTrue: "cover", ifFalse: "nocover"}],
        ["indexes", {suffix: "index", name: "__join__"}],
    ])
    const runOptions: {[key: string]: string} = {
        format: "standard",
        chords: "chorded",
        font: "libertinus",
        minorsign: "minorsign=-",
        columns: "columns=2",
        cover: "cover",
        indexes: "",
    }

    for (const option of mapper.keys()) {
        if (option in receipe.options) {
            let content: string = receipe.options[option]
            if (mapper.get(option)?.ifTrue !== undefined)
                content =
                    content === "true"
                        ? mapper.get(option)?.ifTrue!!
                        : mapper.get(option)?.ifFalse!!

            let arrContent: string[] = []
            if (mapper.get(option)?.name === "__join__") arrContent = content.split(",")
            else if (!Array.isArray(content)) arrContent = [content]
            else arrContent = content

            if (mapper.get(option)?.prefix !== undefined)
                arrContent = arrContent.map(each => mapper.get(option)?.prefix + each)
            if (mapper.get(option)?.suffix !== undefined)
                arrContent = arrContent.map(each => each + mapper.get(option)?.suffix)

            runOptions[option] = arrContent.join(",")
        }
    }

    if (receipe.songs.length === 1) runOptions.format = "singoli"

    await writeFile(
        `${paths.tex}/_preamble.tex`,
        `\\documentclass[${Object.values(runOptions).join(",")}]{canzoniereonline}`,
    )
    console.debug("Latex options:", runOptions)
    return runOptions
}

const generateHash = (status: SongStatus[]): string => {
    const sha = createHash("sha1")
    const payload = status
        .filter(song => song.type === "song")
        .map(song => song.content?.id ?? "")
        .join("")
    return sha.update(payload).digest("hex")
}

const removeTmpFiles = async (
    outDir: string,
    jobName: string,
    exitedSuccessfully = true,
): Promise<void> => {
    const extensions = [
        exitedSuccessfully ? ["log"] : [],
        ...["aux", "out", "idx", "ilg", "ind"],
    ].map(ext => `${jobName}.${ext}`)
    const allFiles = [
        ...extensions,
        "autori.idx",
        "autori.ind",
        "autori.ilg",
        "tematico.idx",
        "tematico.ind",
        "tematico.ilg",
    ]
    allFiles.forEach(async file => {
        try {
            await fsPromises.unlink(`${outDir}/${file}`)
        } catch (e) {}
    })
    fsPromises.rm(`${paths.pdf}/tmp`, {recursive: true})
}

export {generate}
