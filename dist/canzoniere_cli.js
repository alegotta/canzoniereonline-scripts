var $ifjKf$process = require("process");
var $ifjKf$child_process = require("child_process");
var $ifjKf$crypto = require("crypto");
var $ifjKf$fs = require("fs");
var $ifjKf$fspromises = require("fs/promises");
var $ifjKf$readline = require("readline");

function $parcel$interopDefault(a) {
  return a && a.__esModule ? a.default : a;
}
var $parcel$global =
typeof globalThis !== 'undefined'
  ? globalThis
  : typeof self !== 'undefined'
  ? self
  : typeof window !== 'undefined'
  ? window
  : typeof global !== 'undefined'
  ? global
  : {};
var $parcel$modules = {};
var $parcel$inits = {};

var parcelRequire = $parcel$global["parcelRequire8aa0"];
if (parcelRequire == null) {
  parcelRequire = function(id) {
    if (id in $parcel$modules) {
      return $parcel$modules[id].exports;
    }
    if (id in $parcel$inits) {
      var init = $parcel$inits[id];
      delete $parcel$inits[id];
      var module = {id: id, exports: {}};
      $parcel$modules[id] = module;
      init.call(module.exports, module, module.exports);
      return module.exports;
    }
    var err = new Error("Cannot find module '" + id + "'");
    err.code = 'MODULE_NOT_FOUND';
    throw err;
  };

  parcelRequire.register = function register(id, init) {
    $parcel$inits[id] = init;
  };

  $parcel$global["parcelRequire8aa0"] = parcelRequire;
}
parcelRequire.register("5AZVd", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $413058eccba620ca$var$toDate;

var $413058eccba620ca$var$_assertString = $413058eccba620ca$var$_interopRequireDefault((parcelRequire("1khYO")));
function $413058eccba620ca$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $413058eccba620ca$var$toDate(date) {
    (0, $413058eccba620ca$var$_assertString.default)(date);
    date = Date.parse(date);
    return !isNaN(date) ? new Date(date) : null;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("1khYO", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $0f759b258393c5a0$var$assertString;
function $0f759b258393c5a0$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $0f759b258393c5a0$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $0f759b258393c5a0$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $0f759b258393c5a0$var$_typeof(obj);
}
function $0f759b258393c5a0$var$assertString(input) {
    var isString = typeof input === "string" || input instanceof String;
    if (!isString) {
        var invalidType = $0f759b258393c5a0$var$_typeof(input);
        if (input === null) invalidType = "null";
        else if (invalidType === "object") invalidType = input.constructor.name;
        throw new TypeError("Expected a string but received a ".concat(invalidType));
    }
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("b9wlP", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $81e9a13637dc27ff$var$toFloat;

var $81e9a13637dc27ff$var$_isFloat = $81e9a13637dc27ff$var$_interopRequireDefault((parcelRequire("deri3")));
function $81e9a13637dc27ff$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $81e9a13637dc27ff$var$toFloat(str) {
    if (!(0, $81e9a13637dc27ff$var$_isFloat.default)(str)) return NaN;
    return parseFloat(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("deri3", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $9a21c06979a6a07e$var$isFloat;
module.exports.locales = void 0;

var $9a21c06979a6a07e$var$_assertString = $9a21c06979a6a07e$var$_interopRequireDefault((parcelRequire("1khYO")));

var $hfuvR = parcelRequire("hfuvR");
function $9a21c06979a6a07e$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $9a21c06979a6a07e$var$isFloat(str, options) {
    (0, $9a21c06979a6a07e$var$_assertString.default)(str);
    options = options || {};
    var float = new RegExp("^(?:[-+])?(?:[0-9]+)?(?:\\".concat(options.locale ? $hfuvR.decimal[options.locale] : ".", "[0-9]*)?(?:[eE][\\+\\-]?(?:[0-9]+))?$"));
    if (str === "" || str === "." || str === "-" || str === "+") return false;
    var value = parseFloat(str.replace(",", "."));
    return float.test(str) && (!options.hasOwnProperty("min") || value >= options.min) && (!options.hasOwnProperty("max") || value <= options.max) && (!options.hasOwnProperty("lt") || value < options.lt) && (!options.hasOwnProperty("gt") || value > options.gt);
}
var $9a21c06979a6a07e$var$locales = Object.keys($hfuvR.decimal);
module.exports.locales = $9a21c06979a6a07e$var$locales;

});
parcelRequire.register("hfuvR", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.commaDecimal = module.exports.dotDecimal = module.exports.farsiLocales = module.exports.arabicLocales = module.exports.englishLocales = module.exports.decimal = module.exports.alphanumeric = module.exports.alpha = void 0;
var $c8eb81a02ddcdc36$var$alpha = {
    "en-US": /^[A-Z]+$/i,
    "az-AZ": /^[A-VXYZÇƏĞİıÖŞÜ]+$/i,
    "bg-BG": /^[А-Я]+$/i,
    "cs-CZ": /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
    "da-DK": /^[A-ZÆØÅ]+$/i,
    "de-DE": /^[A-ZÄÖÜß]+$/i,
    "el-GR": /^[Α-ώ]+$/i,
    "es-ES": /^[A-ZÁÉÍÑÓÚÜ]+$/i,
    "fa-IR": /^[ابپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی]+$/i,
    "fi-FI": /^[A-ZÅÄÖ]+$/i,
    "fr-FR": /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
    "it-IT": /^[A-ZÀÉÈÌÎÓÒÙ]+$/i,
    "nb-NO": /^[A-ZÆØÅ]+$/i,
    "nl-NL": /^[A-ZÁÉËÏÓÖÜÚ]+$/i,
    "nn-NO": /^[A-ZÆØÅ]+$/i,
    "hu-HU": /^[A-ZÁÉÍÓÖŐÚÜŰ]+$/i,
    "pl-PL": /^[A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
    "pt-PT": /^[A-ZÃÁÀÂÄÇÉÊËÍÏÕÓÔÖÚÜ]+$/i,
    "ru-RU": /^[А-ЯЁ]+$/i,
    "sl-SI": /^[A-ZČĆĐŠŽ]+$/i,
    "sk-SK": /^[A-ZÁČĎÉÍŇÓŠŤÚÝŽĹŔĽÄÔ]+$/i,
    "sr-RS@latin": /^[A-ZČĆŽŠĐ]+$/i,
    "sr-RS": /^[А-ЯЂЈЉЊЋЏ]+$/i,
    "sv-SE": /^[A-ZÅÄÖ]+$/i,
    "th-TH": /^[ก-๐\s]+$/i,
    "tr-TR": /^[A-ZÇĞİıÖŞÜ]+$/i,
    "uk-UA": /^[А-ЩЬЮЯЄIЇҐі]+$/i,
    "vi-VN": /^[A-ZÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴĐÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]+$/i,
    "ku-IQ": /^[ئابپتجچحخدرڕزژسشعغفڤقکگلڵمنوۆھەیێيطؤثآإأكضصةظذ]+$/i,
    ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/,
    he: /^[א-ת]+$/,
    fa: /^['آاءأؤئبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهةی']+$/i,
    "hi-IN": /^[\u0900-\u0961]+[\u0972-\u097F]*$/i
};
module.exports.alpha = $c8eb81a02ddcdc36$var$alpha;
var $c8eb81a02ddcdc36$var$alphanumeric = {
    "en-US": /^[0-9A-Z]+$/i,
    "az-AZ": /^[0-9A-VXYZÇƏĞİıÖŞÜ]+$/i,
    "bg-BG": /^[0-9А-Я]+$/i,
    "cs-CZ": /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]+$/i,
    "da-DK": /^[0-9A-ZÆØÅ]+$/i,
    "de-DE": /^[0-9A-ZÄÖÜß]+$/i,
    "el-GR": /^[0-9Α-ω]+$/i,
    "es-ES": /^[0-9A-ZÁÉÍÑÓÚÜ]+$/i,
    "fi-FI": /^[0-9A-ZÅÄÖ]+$/i,
    "fr-FR": /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]+$/i,
    "it-IT": /^[0-9A-ZÀÉÈÌÎÓÒÙ]+$/i,
    "hu-HU": /^[0-9A-ZÁÉÍÓÖŐÚÜŰ]+$/i,
    "nb-NO": /^[0-9A-ZÆØÅ]+$/i,
    "nl-NL": /^[0-9A-ZÁÉËÏÓÖÜÚ]+$/i,
    "nn-NO": /^[0-9A-ZÆØÅ]+$/i,
    "pl-PL": /^[0-9A-ZĄĆĘŚŁŃÓŻŹ]+$/i,
    "pt-PT": /^[0-9A-ZÃÁÀÂÄÇÉÊËÍÏÕÓÔÖÚÜ]+$/i,
    "ru-RU": /^[0-9А-ЯЁ]+$/i,
    "sl-SI": /^[0-9A-ZČĆĐŠŽ]+$/i,
    "sk-SK": /^[0-9A-ZÁČĎÉÍŇÓŠŤÚÝŽĹŔĽÄÔ]+$/i,
    "sr-RS@latin": /^[0-9A-ZČĆŽŠĐ]+$/i,
    "sr-RS": /^[0-9А-ЯЂЈЉЊЋЏ]+$/i,
    "sv-SE": /^[0-9A-ZÅÄÖ]+$/i,
    "th-TH": /^[ก-๙\s]+$/i,
    "tr-TR": /^[0-9A-ZÇĞİıÖŞÜ]+$/i,
    "uk-UA": /^[0-9А-ЩЬЮЯЄIЇҐі]+$/i,
    "ku-IQ": /^[٠١٢٣٤٥٦٧٨٩0-9ئابپتجچحخدرڕزژسشعغفڤقکگلڵمنوۆھەیێيطؤثآإأكضصةظذ]+$/i,
    "vi-VN": /^[0-9A-ZÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴĐÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]+$/i,
    ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]+$/,
    he: /^[0-9א-ת]+$/,
    fa: /^['0-9آاءأؤئبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهةی۱۲۳۴۵۶۷۸۹۰']+$/i,
    "hi-IN": /^[\u0900-\u0963]+[\u0966-\u097F]*$/i
};
module.exports.alphanumeric = $c8eb81a02ddcdc36$var$alphanumeric;
var $c8eb81a02ddcdc36$var$decimal = {
    "en-US": ".",
    ar: "٫"
};
module.exports.decimal = $c8eb81a02ddcdc36$var$decimal;
var $c8eb81a02ddcdc36$var$englishLocales = [
    "AU",
    "GB",
    "HK",
    "IN",
    "NZ",
    "ZA",
    "ZM"
];
module.exports.englishLocales = $c8eb81a02ddcdc36$var$englishLocales;
for(var $c8eb81a02ddcdc36$var$locale, $c8eb81a02ddcdc36$var$i = 0; $c8eb81a02ddcdc36$var$i < $c8eb81a02ddcdc36$var$englishLocales.length; $c8eb81a02ddcdc36$var$i++){
    $c8eb81a02ddcdc36$var$locale = "en-".concat($c8eb81a02ddcdc36$var$englishLocales[$c8eb81a02ddcdc36$var$i]);
    $c8eb81a02ddcdc36$var$alpha[$c8eb81a02ddcdc36$var$locale] = $c8eb81a02ddcdc36$var$alpha["en-US"];
    $c8eb81a02ddcdc36$var$alphanumeric[$c8eb81a02ddcdc36$var$locale] = $c8eb81a02ddcdc36$var$alphanumeric["en-US"];
    $c8eb81a02ddcdc36$var$decimal[$c8eb81a02ddcdc36$var$locale] = $c8eb81a02ddcdc36$var$decimal["en-US"];
} // Source: http://www.localeplanet.com/java/
var $c8eb81a02ddcdc36$var$arabicLocales = [
    "AE",
    "BH",
    "DZ",
    "EG",
    "IQ",
    "JO",
    "KW",
    "LB",
    "LY",
    "MA",
    "QM",
    "QA",
    "SA",
    "SD",
    "SY",
    "TN",
    "YE"
];
module.exports.arabicLocales = $c8eb81a02ddcdc36$var$arabicLocales;
for(var $c8eb81a02ddcdc36$var$_locale, $c8eb81a02ddcdc36$var$_i = 0; $c8eb81a02ddcdc36$var$_i < $c8eb81a02ddcdc36$var$arabicLocales.length; $c8eb81a02ddcdc36$var$_i++){
    $c8eb81a02ddcdc36$var$_locale = "ar-".concat($c8eb81a02ddcdc36$var$arabicLocales[$c8eb81a02ddcdc36$var$_i]);
    $c8eb81a02ddcdc36$var$alpha[$c8eb81a02ddcdc36$var$_locale] = $c8eb81a02ddcdc36$var$alpha.ar;
    $c8eb81a02ddcdc36$var$alphanumeric[$c8eb81a02ddcdc36$var$_locale] = $c8eb81a02ddcdc36$var$alphanumeric.ar;
    $c8eb81a02ddcdc36$var$decimal[$c8eb81a02ddcdc36$var$_locale] = $c8eb81a02ddcdc36$var$decimal.ar;
}
var $c8eb81a02ddcdc36$var$farsiLocales = [
    "IR",
    "AF"
];
module.exports.farsiLocales = $c8eb81a02ddcdc36$var$farsiLocales;
for(var $c8eb81a02ddcdc36$var$_locale2, $c8eb81a02ddcdc36$var$_i2 = 0; $c8eb81a02ddcdc36$var$_i2 < $c8eb81a02ddcdc36$var$farsiLocales.length; $c8eb81a02ddcdc36$var$_i2++){
    $c8eb81a02ddcdc36$var$_locale2 = "fa-".concat($c8eb81a02ddcdc36$var$farsiLocales[$c8eb81a02ddcdc36$var$_i2]);
    $c8eb81a02ddcdc36$var$alphanumeric[$c8eb81a02ddcdc36$var$_locale2] = $c8eb81a02ddcdc36$var$alphanumeric.fa;
    $c8eb81a02ddcdc36$var$decimal[$c8eb81a02ddcdc36$var$_locale2] = $c8eb81a02ddcdc36$var$decimal.ar;
} // Source: https://en.wikipedia.org/wiki/Decimal_mark
var $c8eb81a02ddcdc36$var$dotDecimal = [
    "ar-EG",
    "ar-LB",
    "ar-LY"
];
module.exports.dotDecimal = $c8eb81a02ddcdc36$var$dotDecimal;
var $c8eb81a02ddcdc36$var$commaDecimal = [
    "bg-BG",
    "cs-CZ",
    "da-DK",
    "de-DE",
    "el-GR",
    "en-ZM",
    "es-ES",
    "fr-CA",
    "fr-FR",
    "id-ID",
    "it-IT",
    "ku-IQ",
    "hi-IN",
    "hu-HU",
    "nb-NO",
    "nn-NO",
    "nl-NL",
    "pl-PL",
    "pt-PT",
    "ru-RU",
    "sl-SI",
    "sr-RS@latin",
    "sr-RS",
    "sv-SE",
    "tr-TR",
    "uk-UA",
    "vi-VN"
];
module.exports.commaDecimal = $c8eb81a02ddcdc36$var$commaDecimal;
for(var $c8eb81a02ddcdc36$var$_i3 = 0; $c8eb81a02ddcdc36$var$_i3 < $c8eb81a02ddcdc36$var$dotDecimal.length; $c8eb81a02ddcdc36$var$_i3++)$c8eb81a02ddcdc36$var$decimal[$c8eb81a02ddcdc36$var$dotDecimal[$c8eb81a02ddcdc36$var$_i3]] = $c8eb81a02ddcdc36$var$decimal["en-US"];
for(var $c8eb81a02ddcdc36$var$_i4 = 0; $c8eb81a02ddcdc36$var$_i4 < $c8eb81a02ddcdc36$var$commaDecimal.length; $c8eb81a02ddcdc36$var$_i4++)$c8eb81a02ddcdc36$var$decimal[$c8eb81a02ddcdc36$var$commaDecimal[$c8eb81a02ddcdc36$var$_i4]] = ",";
$c8eb81a02ddcdc36$var$alpha["fr-CA"] = $c8eb81a02ddcdc36$var$alpha["fr-FR"];
$c8eb81a02ddcdc36$var$alphanumeric["fr-CA"] = $c8eb81a02ddcdc36$var$alphanumeric["fr-FR"];
$c8eb81a02ddcdc36$var$alpha["pt-BR"] = $c8eb81a02ddcdc36$var$alpha["pt-PT"];
$c8eb81a02ddcdc36$var$alphanumeric["pt-BR"] = $c8eb81a02ddcdc36$var$alphanumeric["pt-PT"];
$c8eb81a02ddcdc36$var$decimal["pt-BR"] = $c8eb81a02ddcdc36$var$decimal["pt-PT"]; // see #862
$c8eb81a02ddcdc36$var$alpha["pl-Pl"] = $c8eb81a02ddcdc36$var$alpha["pl-PL"];
$c8eb81a02ddcdc36$var$alphanumeric["pl-Pl"] = $c8eb81a02ddcdc36$var$alphanumeric["pl-PL"];
$c8eb81a02ddcdc36$var$decimal["pl-Pl"] = $c8eb81a02ddcdc36$var$decimal["pl-PL"]; // see #1455
$c8eb81a02ddcdc36$var$alpha["fa-AF"] = $c8eb81a02ddcdc36$var$alpha.fa;

});



parcelRequire.register("aXy21", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $7fa9d008c7185f69$var$toInt;

var $7fa9d008c7185f69$var$_assertString = $7fa9d008c7185f69$var$_interopRequireDefault((parcelRequire("1khYO")));
function $7fa9d008c7185f69$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $7fa9d008c7185f69$var$toInt(str, radix) {
    (0, $7fa9d008c7185f69$var$_assertString.default)(str);
    return parseInt(str, radix || 10);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("cDMZB", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $933f217b020f88bb$var$toBoolean;

var $933f217b020f88bb$var$_assertString = $933f217b020f88bb$var$_interopRequireDefault((parcelRequire("1khYO")));
function $933f217b020f88bb$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $933f217b020f88bb$var$toBoolean(str, strict) {
    (0, $933f217b020f88bb$var$_assertString.default)(str);
    if (strict) return str === "1" || /^true$/i.test(str);
    return str !== "0" && !/^false$/i.test(str) && str !== "";
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("61jgp", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $4621c76c97e352f3$var$equals;

var $4621c76c97e352f3$var$_assertString = $4621c76c97e352f3$var$_interopRequireDefault((parcelRequire("1khYO")));
function $4621c76c97e352f3$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $4621c76c97e352f3$var$equals(str, comparison) {
    (0, $4621c76c97e352f3$var$_assertString.default)(str);
    return str === comparison;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("5sIaV", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $3fa1d591e933eb36$var$contains;

var $3fa1d591e933eb36$var$_assertString = $3fa1d591e933eb36$var$_interopRequireDefault((parcelRequire("1khYO")));

var $3fa1d591e933eb36$var$_toString = $3fa1d591e933eb36$var$_interopRequireDefault((parcelRequire("liR8H")));

var $3fa1d591e933eb36$var$_merge = $3fa1d591e933eb36$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $3fa1d591e933eb36$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $3fa1d591e933eb36$var$defaulContainsOptions = {
    ignoreCase: false,
    minOccurrences: 1
};
function $3fa1d591e933eb36$var$contains(str, elem, options) {
    (0, $3fa1d591e933eb36$var$_assertString.default)(str);
    options = (0, $3fa1d591e933eb36$var$_merge.default)(options, $3fa1d591e933eb36$var$defaulContainsOptions);
    if (options.ignoreCase) return str.toLowerCase().split((0, $3fa1d591e933eb36$var$_toString.default)(elem).toLowerCase()).length > options.minOccurrences;
    return str.split((0, $3fa1d591e933eb36$var$_toString.default)(elem)).length > options.minOccurrences;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("liR8H", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $f8247fca32583820$var$toString;
function $f8247fca32583820$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $f8247fca32583820$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $f8247fca32583820$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $f8247fca32583820$var$_typeof(obj);
}
function $f8247fca32583820$var$toString(input) {
    if ($f8247fca32583820$var$_typeof(input) === "object" && input !== null) {
        if (typeof input.toString === "function") input = input.toString();
        else input = "[object Object]";
    } else if (input === null || typeof input === "undefined" || isNaN(input) && !input.length) input = "";
    return String(input);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("lIjC8", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $fcecebfc4f896180$var$merge;
function $fcecebfc4f896180$var$merge() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var defaults = arguments.length > 1 ? arguments[1] : undefined;
    for(var key in defaults)if (typeof obj[key] === "undefined") obj[key] = defaults[key];
    return obj;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("g72VB", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $bbaf9239d2f5a99f$var$matches;

var $bbaf9239d2f5a99f$var$_assertString = $bbaf9239d2f5a99f$var$_interopRequireDefault((parcelRequire("1khYO")));
function $bbaf9239d2f5a99f$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $bbaf9239d2f5a99f$var$matches(str, pattern, modifiers) {
    (0, $bbaf9239d2f5a99f$var$_assertString.default)(str);
    if (Object.prototype.toString.call(pattern) !== "[object RegExp]") pattern = new RegExp(pattern, modifiers);
    return pattern.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("kVDMK", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $f3c7fe0ef5c4cd15$var$isEmail;

var $f3c7fe0ef5c4cd15$var$_assertString = $f3c7fe0ef5c4cd15$var$_interopRequireDefault((parcelRequire("1khYO")));

var $f3c7fe0ef5c4cd15$var$_merge = $f3c7fe0ef5c4cd15$var$_interopRequireDefault((parcelRequire("lIjC8")));

var $f3c7fe0ef5c4cd15$var$_isByteLength = $f3c7fe0ef5c4cd15$var$_interopRequireDefault((parcelRequire("bOAiS")));

var $f3c7fe0ef5c4cd15$var$_isFQDN = $f3c7fe0ef5c4cd15$var$_interopRequireDefault((parcelRequire("9NQBl")));

var $f3c7fe0ef5c4cd15$var$_isIP = $f3c7fe0ef5c4cd15$var$_interopRequireDefault((parcelRequire("gIxCs")));
function $f3c7fe0ef5c4cd15$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $f3c7fe0ef5c4cd15$var$default_email_options = {
    allow_display_name: false,
    require_display_name: false,
    allow_utf8_local_part: true,
    require_tld: true,
    blacklisted_chars: "",
    ignore_max_length: false,
    host_blacklist: []
};
/* eslint-disable max-len */ /* eslint-disable no-control-regex */ var $f3c7fe0ef5c4cd15$var$splitNameAddress = /^([^\x00-\x1F\x7F-\x9F\cX]+)</i;
var $f3c7fe0ef5c4cd15$var$emailUserPart = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~]+$/i;
var $f3c7fe0ef5c4cd15$var$gmailUserPart = /^[a-z\d]+$/;
var $f3c7fe0ef5c4cd15$var$quotedEmailUser = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f]))*$/i;
var $f3c7fe0ef5c4cd15$var$emailUserUtf8Part = /^[a-z\d!#\$%&'\*\+\-\/=\?\^_`{\|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+$/i;
var $f3c7fe0ef5c4cd15$var$quotedEmailUserUtf8 = /^([\s\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|(\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*$/i;
var $f3c7fe0ef5c4cd15$var$defaultMaxEmailLength = 254;
/* eslint-enable max-len */ /* eslint-enable no-control-regex */ /**
 * Validate display name according to the RFC2822: https://tools.ietf.org/html/rfc2822#appendix-A.1.2
 * @param {String} display_name
 */ function $f3c7fe0ef5c4cd15$var$validateDisplayName(display_name) {
    var display_name_without_quotes = display_name.replace(/^"(.+)"$/, "$1"); // display name with only spaces is not valid
    if (!display_name_without_quotes.trim()) return false;
     // check whether display name contains illegal character
    var contains_illegal = /[\.";<>]/.test(display_name_without_quotes);
    if (contains_illegal) {
        // if contains illegal characters,
        // must to be enclosed in double-quotes, otherwise it's not a valid display name
        if (display_name_without_quotes === display_name) return false;
         // the quotes in display name must start with character symbol \
        var all_start_with_back_slash = display_name_without_quotes.split('"').length === display_name_without_quotes.split('\\"').length;
        if (!all_start_with_back_slash) return false;
    }
    return true;
}
function $f3c7fe0ef5c4cd15$var$isEmail(str, options) {
    (0, $f3c7fe0ef5c4cd15$var$_assertString.default)(str);
    options = (0, $f3c7fe0ef5c4cd15$var$_merge.default)(options, $f3c7fe0ef5c4cd15$var$default_email_options);
    if (options.require_display_name || options.allow_display_name) {
        var display_email = str.match($f3c7fe0ef5c4cd15$var$splitNameAddress);
        if (display_email) {
            var display_name = display_email[1]; // Remove display name and angle brackets to get email address
            // Can be done in the regex but will introduce a ReDOS (See  #1597 for more info)
            str = str.replace(display_name, "").replace(/(^<|>$)/g, ""); // sometimes need to trim the last space to get the display name
            // because there may be a space between display name and email address
            // eg. myname <address@gmail.com>
            // the display name is `myname` instead of `myname `, so need to trim the last space
            if (display_name.endsWith(" ")) display_name = display_name.substr(0, display_name.length - 1);
            if (!$f3c7fe0ef5c4cd15$var$validateDisplayName(display_name)) return false;
        } else if (options.require_display_name) return false;
    }
    if (!options.ignore_max_length && str.length > $f3c7fe0ef5c4cd15$var$defaultMaxEmailLength) return false;
    var parts = str.split("@");
    var domain = parts.pop();
    var lower_domain = domain.toLowerCase();
    if (options.host_blacklist.includes(lower_domain)) return false;
    var user = parts.join("@");
    if (options.domain_specific_validation && (lower_domain === "gmail.com" || lower_domain === "googlemail.com")) {
        /*
      Previously we removed dots for gmail addresses before validating.
      This was removed because it allows `multiple..dots@gmail.com`
      to be reported as valid, but it is not.
      Gmail only normalizes single dots, removing them from here is pointless,
      should be done in normalizeEmail
    */ user = user.toLowerCase(); // Removing sub-address from username before gmail validation
        var username = user.split("+")[0]; // Dots are not included in gmail length restriction
        if (!(0, $f3c7fe0ef5c4cd15$var$_isByteLength.default)(username.replace(/\./g, ""), {
            min: 6,
            max: 30
        })) return false;
        var _user_parts = username.split(".");
        for(var i = 0; i < _user_parts.length; i++){
            if (!$f3c7fe0ef5c4cd15$var$gmailUserPart.test(_user_parts[i])) return false;
        }
    }
    if (options.ignore_max_length === false && (!(0, $f3c7fe0ef5c4cd15$var$_isByteLength.default)(user, {
        max: 64
    }) || !(0, $f3c7fe0ef5c4cd15$var$_isByteLength.default)(domain, {
        max: 254
    }))) return false;
    if (!(0, $f3c7fe0ef5c4cd15$var$_isFQDN.default)(domain, {
        require_tld: options.require_tld
    })) {
        if (!options.allow_ip_domain) return false;
        if (!(0, $f3c7fe0ef5c4cd15$var$_isIP.default)(domain)) {
            if (!domain.startsWith("[") || !domain.endsWith("]")) return false;
            var noBracketdomain = domain.substr(1, domain.length - 2);
            if (noBracketdomain.length === 0 || !(0, $f3c7fe0ef5c4cd15$var$_isIP.default)(noBracketdomain)) return false;
        }
    }
    if (user[0] === '"') {
        user = user.slice(1, user.length - 1);
        return options.allow_utf8_local_part ? $f3c7fe0ef5c4cd15$var$quotedEmailUserUtf8.test(user) : $f3c7fe0ef5c4cd15$var$quotedEmailUser.test(user);
    }
    var pattern = options.allow_utf8_local_part ? $f3c7fe0ef5c4cd15$var$emailUserUtf8Part : $f3c7fe0ef5c4cd15$var$emailUserPart;
    var user_parts = user.split(".");
    for(var _i = 0; _i < user_parts.length; _i++){
        if (!pattern.test(user_parts[_i])) return false;
    }
    if (options.blacklisted_chars) {
        if (user.search(new RegExp("[".concat(options.blacklisted_chars, "]+"), "g")) !== -1) return false;
    }
    return true;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("bOAiS", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $89a0865230e13f9b$var$isByteLength;

var $89a0865230e13f9b$var$_assertString = $89a0865230e13f9b$var$_interopRequireDefault((parcelRequire("1khYO")));
function $89a0865230e13f9b$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $89a0865230e13f9b$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $89a0865230e13f9b$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $89a0865230e13f9b$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $89a0865230e13f9b$var$_typeof(obj);
}
/* eslint-disable prefer-rest-params */ function $89a0865230e13f9b$var$isByteLength(str, options) {
    (0, $89a0865230e13f9b$var$_assertString.default)(str);
    var min;
    var max;
    if ($89a0865230e13f9b$var$_typeof(options) === "object") {
        min = options.min || 0;
        max = options.max;
    } else {
        // backwards compatibility: isByteLength(str, min [, max])
        min = arguments[1];
        max = arguments[2];
    }
    var len = encodeURI(str).split(/%..|./).length - 1;
    return len >= min && (typeof max === "undefined" || len <= max);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("9NQBl", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $72317e2a3ab96a86$var$isFQDN;

var $72317e2a3ab96a86$var$_assertString = $72317e2a3ab96a86$var$_interopRequireDefault((parcelRequire("1khYO")));

var $72317e2a3ab96a86$var$_merge = $72317e2a3ab96a86$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $72317e2a3ab96a86$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $72317e2a3ab96a86$var$default_fqdn_options = {
    require_tld: true,
    allow_underscores: false,
    allow_trailing_dot: false,
    allow_numeric_tld: false,
    allow_wildcard: false
};
function $72317e2a3ab96a86$var$isFQDN(str, options) {
    (0, $72317e2a3ab96a86$var$_assertString.default)(str);
    options = (0, $72317e2a3ab96a86$var$_merge.default)(options, $72317e2a3ab96a86$var$default_fqdn_options);
    /* Remove the optional trailing dot before checking validity */ if (options.allow_trailing_dot && str[str.length - 1] === ".") str = str.substring(0, str.length - 1);
    /* Remove the optional wildcard before checking validity */ if (options.allow_wildcard === true && str.indexOf("*.") === 0) str = str.substring(2);
    var parts = str.split(".");
    var tld = parts[parts.length - 1];
    if (options.require_tld) {
        // disallow fqdns without tld
        if (parts.length < 2) return false;
        if (!/^([a-z\u00A1-\u00A8\u00AA-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]{2,}|xn[a-z0-9-]{2,})$/i.test(tld)) return false;
         // disallow spaces
        if (/\s/.test(tld)) return false;
    } // reject numeric TLDs
    if (!options.allow_numeric_tld && /^\d+$/.test(tld)) return false;
    return parts.every(function(part) {
        if (part.length > 63) return false;
        if (!/^[a-z_\u00a1-\uffff0-9-]+$/i.test(part)) return false;
         // disallow full-width chars
        if (/[\uff01-\uff5e]/.test(part)) return false;
         // disallow parts starting or ending with hyphen
        if (/^-|-$/.test(part)) return false;
        if (!options.allow_underscores && /_/.test(part)) return false;
        return true;
    });
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("gIxCs", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $c2bad57aa7fb8020$var$isIP;

var $c2bad57aa7fb8020$var$_assertString = $c2bad57aa7fb8020$var$_interopRequireDefault((parcelRequire("1khYO")));
function $c2bad57aa7fb8020$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/**
11.3.  Examples

   The following addresses

             fe80::1234 (on the 1st link of the node)
             ff02::5678 (on the 5th link of the node)
             ff08::9abc (on the 10th organization of the node)

   would be represented as follows:

             fe80::1234%1
             ff02::5678%5
             ff08::9abc%10

   (Here we assume a natural translation from a zone index to the
   <zone_id> part, where the Nth zone of any scope is translated into
   "N".)

   If we use interface names as <zone_id>, those addresses could also be
   represented as follows:

            fe80::1234%ne0
            ff02::5678%pvc1.3
            ff08::9abc%interface10

   where the interface "ne0" belongs to the 1st link, "pvc1.3" belongs
   to the 5th link, and "interface10" belongs to the 10th organization.
 * * */ var $c2bad57aa7fb8020$var$IPv4SegmentFormat = "(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])";
var $c2bad57aa7fb8020$var$IPv4AddressFormat = "(".concat($c2bad57aa7fb8020$var$IPv4SegmentFormat, "[.]){3}").concat($c2bad57aa7fb8020$var$IPv4SegmentFormat);
var $c2bad57aa7fb8020$var$IPv4AddressRegExp = new RegExp("^".concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "$"));
var $c2bad57aa7fb8020$var$IPv6SegmentFormat = "(?:[0-9a-fA-F]{1,4})";
var $c2bad57aa7fb8020$var$IPv6AddressRegExp = new RegExp("^(" + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){7}(?:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){6}(?:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){5}(?::").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,2}|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){4}(?:(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){0,1}:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,3}|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){3}(?:(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){0,2}:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,4}|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){2}(?:(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){0,3}:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,5}|:)|") + "(?:".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, ":){1}(?:(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){0,4}:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(:").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,6}|:)|") + "(?::((?::".concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){0,5}:").concat($c2bad57aa7fb8020$var$IPv4AddressFormat, "|(?::").concat($c2bad57aa7fb8020$var$IPv6SegmentFormat, "){1,7}|:))") + ")(%[0-9a-zA-Z-.:]{1,})?$");
function $c2bad57aa7fb8020$var$isIP(str) {
    var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    (0, $c2bad57aa7fb8020$var$_assertString.default)(str);
    version = String(version);
    if (!version) return $c2bad57aa7fb8020$var$isIP(str, 4) || $c2bad57aa7fb8020$var$isIP(str, 6);
    if (version === "4") {
        if (!$c2bad57aa7fb8020$var$IPv4AddressRegExp.test(str)) return false;
        var parts = str.split(".").sort(function(a, b) {
            return a - b;
        });
        return parts[3] <= 255;
    }
    if (version === "6") return !!$c2bad57aa7fb8020$var$IPv6AddressRegExp.test(str);
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("joKBl", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $e1f468424ce43277$var$isURL;

var $e1f468424ce43277$var$_assertString = $e1f468424ce43277$var$_interopRequireDefault((parcelRequire("1khYO")));

var $e1f468424ce43277$var$_isFQDN = $e1f468424ce43277$var$_interopRequireDefault((parcelRequire("9NQBl")));

var $e1f468424ce43277$var$_isIP = $e1f468424ce43277$var$_interopRequireDefault((parcelRequire("gIxCs")));

var $e1f468424ce43277$var$_merge = $e1f468424ce43277$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $e1f468424ce43277$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $e1f468424ce43277$var$_slicedToArray(arr, i) {
    return $e1f468424ce43277$var$_arrayWithHoles(arr) || $e1f468424ce43277$var$_iterableToArrayLimit(arr, i) || $e1f468424ce43277$var$_unsupportedIterableToArray(arr, i) || $e1f468424ce43277$var$_nonIterableRest();
}
function $e1f468424ce43277$var$_nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function $e1f468424ce43277$var$_unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return $e1f468424ce43277$var$_arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return $e1f468424ce43277$var$_arrayLikeToArray(o, minLen);
}
function $e1f468424ce43277$var$_arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
function $e1f468424ce43277$var$_iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;
    try {
        for(var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function $e1f468424ce43277$var$_arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
/*
options for isURL method

require_protocol - if set as true isURL will return false if protocol is not present in the URL
require_valid_protocol - isURL will check if the URL's protocol is present in the protocols option
protocols - valid protocols can be modified with this option
require_host - if set as false isURL will not check if host is present in the URL
require_port - if set as true isURL will check if port is present in the URL
allow_protocol_relative_urls - if set as true protocol relative URLs will be allowed
validate_length - if set as false isURL will skip string length validation (IE maximum is 2083)

*/ var $e1f468424ce43277$var$default_url_options = {
    protocols: [
        "http",
        "https",
        "ftp"
    ],
    require_tld: true,
    require_protocol: false,
    require_host: true,
    require_port: false,
    require_valid_protocol: true,
    allow_underscores: false,
    allow_trailing_dot: false,
    allow_protocol_relative_urls: false,
    allow_fragments: true,
    allow_query_components: true,
    validate_length: true
};
var $e1f468424ce43277$var$wrapped_ipv6 = /^\[([^\]]+)\](?::([0-9]+))?$/;
function $e1f468424ce43277$var$isRegExp(obj) {
    return Object.prototype.toString.call(obj) === "[object RegExp]";
}
function $e1f468424ce43277$var$checkHost(host, matches) {
    for(var i = 0; i < matches.length; i++){
        var match = matches[i];
        if (host === match || $e1f468424ce43277$var$isRegExp(match) && match.test(host)) return true;
    }
    return false;
}
function $e1f468424ce43277$var$isURL(url, options) {
    (0, $e1f468424ce43277$var$_assertString.default)(url);
    if (!url || /[\s<>]/.test(url)) return false;
    if (url.indexOf("mailto:") === 0) return false;
    options = (0, $e1f468424ce43277$var$_merge.default)(options, $e1f468424ce43277$var$default_url_options);
    if (options.validate_length && url.length >= 2083) return false;
    if (!options.allow_fragments && url.includes("#")) return false;
    if (!options.allow_query_components && (url.includes("?") || url.includes("&"))) return false;
    var protocol, auth, host, hostname, port, port_str, split, ipv6;
    split = url.split("#");
    url = split.shift();
    split = url.split("?");
    url = split.shift();
    split = url.split("://");
    if (split.length > 1) {
        protocol = split.shift().toLowerCase();
        if (options.require_valid_protocol && options.protocols.indexOf(protocol) === -1) return false;
    } else if (options.require_protocol) return false;
    else if (url.substr(0, 2) === "//") {
        if (!options.allow_protocol_relative_urls) return false;
        split[0] = url.substr(2);
    }
    url = split.join("://");
    if (url === "") return false;
    split = url.split("/");
    url = split.shift();
    if (url === "" && !options.require_host) return true;
    split = url.split("@");
    if (split.length > 1) {
        if (options.disallow_auth) return false;
        if (split[0] === "") return false;
        auth = split.shift();
        if (auth.indexOf(":") >= 0 && auth.split(":").length > 2) return false;
        var _auth$split = auth.split(":"), _auth$split2 = $e1f468424ce43277$var$_slicedToArray(_auth$split, 2), user = _auth$split2[0], password = _auth$split2[1];
        if (user === "" && password === "") return false;
    }
    hostname = split.join("@");
    port_str = null;
    ipv6 = null;
    var ipv6_match = hostname.match($e1f468424ce43277$var$wrapped_ipv6);
    if (ipv6_match) {
        host = "";
        ipv6 = ipv6_match[1];
        port_str = ipv6_match[2] || null;
    } else {
        split = hostname.split(":");
        host = split.shift();
        if (split.length) port_str = split.join(":");
    }
    if (port_str !== null && port_str.length > 0) {
        port = parseInt(port_str, 10);
        if (!/^[0-9]+$/.test(port_str) || port <= 0 || port > 65535) return false;
    } else if (options.require_port) return false;
    if (options.host_whitelist) return $e1f468424ce43277$var$checkHost(host, options.host_whitelist);
    if (!(0, $e1f468424ce43277$var$_isIP.default)(host) && !(0, $e1f468424ce43277$var$_isFQDN.default)(host, options) && (!ipv6 || !(0, $e1f468424ce43277$var$_isIP.default)(ipv6, 6))) return false;
    host = host || ipv6;
    if (options.host_blacklist && $e1f468424ce43277$var$checkHost(host, options.host_blacklist)) return false;
    return true;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("eYCsE", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $ae7480df0be1d8ef$var$isMACAddress;

var $ae7480df0be1d8ef$var$_assertString = $ae7480df0be1d8ef$var$_interopRequireDefault((parcelRequire("1khYO")));
function $ae7480df0be1d8ef$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $ae7480df0be1d8ef$var$macAddress = /^(?:[0-9a-fA-F]{2}([-:\s]))([0-9a-fA-F]{2}\1){4}([0-9a-fA-F]{2})$/;
var $ae7480df0be1d8ef$var$macAddressNoSeparators = /^([0-9a-fA-F]){12}$/;
var $ae7480df0be1d8ef$var$macAddressWithDots = /^([0-9a-fA-F]{4}\.){2}([0-9a-fA-F]{4})$/;
function $ae7480df0be1d8ef$var$isMACAddress(str, options) {
    (0, $ae7480df0be1d8ef$var$_assertString.default)(str);
    /**
   * @deprecated `no_colons` TODO: remove it in the next major
  */ if (options && (options.no_colons || options.no_separators)) return $ae7480df0be1d8ef$var$macAddressNoSeparators.test(str);
    return $ae7480df0be1d8ef$var$macAddress.test(str) || $ae7480df0be1d8ef$var$macAddressWithDots.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2rsAP", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $1c74485ed51071d7$var$isIPRange;

var $1c74485ed51071d7$var$_assertString = $1c74485ed51071d7$var$_interopRequireDefault((parcelRequire("1khYO")));

var $1c74485ed51071d7$var$_isIP = $1c74485ed51071d7$var$_interopRequireDefault((parcelRequire("gIxCs")));
function $1c74485ed51071d7$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $1c74485ed51071d7$var$subnetMaybe = /^\d{1,3}$/;
var $1c74485ed51071d7$var$v4Subnet = 32;
var $1c74485ed51071d7$var$v6Subnet = 128;
function $1c74485ed51071d7$var$isIPRange(str) {
    var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    (0, $1c74485ed51071d7$var$_assertString.default)(str);
    var parts = str.split("/"); // parts[0] -> ip, parts[1] -> subnet
    if (parts.length !== 2) return false;
    if (!$1c74485ed51071d7$var$subnetMaybe.test(parts[1])) return false;
     // Disallow preceding 0 i.e. 01, 02, ...
    if (parts[1].length > 1 && parts[1].startsWith("0")) return false;
    var isValidIP = (0, $1c74485ed51071d7$var$_isIP.default)(parts[0], version);
    if (!isValidIP) return false;
     // Define valid subnet according to IP's version
    var expectedSubnet = null;
    switch(String(version)){
        case "4":
            expectedSubnet = $1c74485ed51071d7$var$v4Subnet;
            break;
        case "6":
            expectedSubnet = $1c74485ed51071d7$var$v6Subnet;
            break;
        default:
            expectedSubnet = (0, $1c74485ed51071d7$var$_isIP.default)(parts[0], "6") ? $1c74485ed51071d7$var$v6Subnet : $1c74485ed51071d7$var$v4Subnet;
    }
    return parts[1] <= expectedSubnet && parts[1] >= 0;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2KIcT", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $20122b38d3d2714a$var$isDate;

var $20122b38d3d2714a$var$_merge = $20122b38d3d2714a$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $20122b38d3d2714a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $20122b38d3d2714a$var$_slicedToArray(arr, i) {
    return $20122b38d3d2714a$var$_arrayWithHoles(arr) || $20122b38d3d2714a$var$_iterableToArrayLimit(arr, i) || $20122b38d3d2714a$var$_unsupportedIterableToArray(arr, i) || $20122b38d3d2714a$var$_nonIterableRest();
}
function $20122b38d3d2714a$var$_nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function $20122b38d3d2714a$var$_iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;
    try {
        for(var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function $20122b38d3d2714a$var$_arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
function $20122b38d3d2714a$var$_createForOfIteratorHelper(o, allowArrayLike) {
    var it;
    if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
        if (Array.isArray(o) || (it = $20122b38d3d2714a$var$_unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
            if (it) o = it;
            var i = 0;
            var F = function F() {};
            return {
                s: F,
                n: function n() {
                    if (i >= o.length) return {
                        done: true
                    };
                    return {
                        done: false,
                        value: o[i++]
                    };
                },
                e: function e(_e2) {
                    throw _e2;
                },
                f: F
            };
        }
        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    var normalCompletion = true, didErr = false, err;
    return {
        s: function s() {
            it = o[Symbol.iterator]();
        },
        n: function n() {
            var step = it.next();
            normalCompletion = step.done;
            return step;
        },
        e: function e(_e3) {
            didErr = true;
            err = _e3;
        },
        f: function f() {
            try {
                if (!normalCompletion && it.return != null) it.return();
            } finally{
                if (didErr) throw err;
            }
        }
    };
}
function $20122b38d3d2714a$var$_unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return $20122b38d3d2714a$var$_arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return $20122b38d3d2714a$var$_arrayLikeToArray(o, minLen);
}
function $20122b38d3d2714a$var$_arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
var $20122b38d3d2714a$var$default_date_options = {
    format: "YYYY/MM/DD",
    delimiters: [
        "/",
        "-"
    ],
    strictMode: false
};
function $20122b38d3d2714a$var$isValidFormat(format) {
    return /(^(y{4}|y{2})[.\/-](m{1,2})[.\/-](d{1,2})$)|(^(m{1,2})[.\/-](d{1,2})[.\/-]((y{4}|y{2})$))|(^(d{1,2})[.\/-](m{1,2})[.\/-]((y{4}|y{2})$))/gi.test(format);
}
function $20122b38d3d2714a$var$zip(date, format) {
    var zippedArr = [], len = Math.min(date.length, format.length);
    for(var i = 0; i < len; i++)zippedArr.push([
        date[i],
        format[i]
    ]);
    return zippedArr;
}
function $20122b38d3d2714a$var$isDate(input, options) {
    if (typeof options === "string") // Allow backward compatbility for old format isDate(input [, format])
    options = (0, $20122b38d3d2714a$var$_merge.default)({
        format: options
    }, $20122b38d3d2714a$var$default_date_options);
    else options = (0, $20122b38d3d2714a$var$_merge.default)(options, $20122b38d3d2714a$var$default_date_options);
    if (typeof input === "string" && $20122b38d3d2714a$var$isValidFormat(options.format)) {
        var formatDelimiter = options.delimiters.find(function(delimiter) {
            return options.format.indexOf(delimiter) !== -1;
        });
        var dateDelimiter = options.strictMode ? formatDelimiter : options.delimiters.find(function(delimiter) {
            return input.indexOf(delimiter) !== -1;
        });
        var dateAndFormat = $20122b38d3d2714a$var$zip(input.split(dateDelimiter), options.format.toLowerCase().split(formatDelimiter));
        var dateObj = {};
        var _iterator = $20122b38d3d2714a$var$_createForOfIteratorHelper(dateAndFormat), _step;
        try {
            for(_iterator.s(); !(_step = _iterator.n()).done;){
                var _step$value = $20122b38d3d2714a$var$_slicedToArray(_step.value, 2), dateWord = _step$value[0], formatWord = _step$value[1];
                if (dateWord.length !== formatWord.length) return false;
                dateObj[formatWord.charAt(0)] = dateWord;
            }
        } catch (err) {
            _iterator.e(err);
        } finally{
            _iterator.f();
        }
        return new Date("".concat(dateObj.m, "/").concat(dateObj.d, "/").concat(dateObj.y)).getDate() === +dateObj.d;
    }
    if (!options.strictMode) return Object.prototype.toString.call(input) === "[object Date]" && isFinite(input);
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("cNmHm", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $950bab130f7b9443$var$isBoolean;

var $950bab130f7b9443$var$_assertString = $950bab130f7b9443$var$_interopRequireDefault((parcelRequire("1khYO")));
function $950bab130f7b9443$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $950bab130f7b9443$var$defaultOptions = {
    loose: false
};
var $950bab130f7b9443$var$strictBooleans = [
    "true",
    "false",
    "1",
    "0"
];
var $950bab130f7b9443$var$looseBooleans = [].concat($950bab130f7b9443$var$strictBooleans, [
    "yes",
    "no"
]);
function $950bab130f7b9443$var$isBoolean(str) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : $950bab130f7b9443$var$defaultOptions;
    (0, $950bab130f7b9443$var$_assertString.default)(str);
    if (options.loose) return $950bab130f7b9443$var$looseBooleans.includes(str.toLowerCase());
    return $950bab130f7b9443$var$strictBooleans.includes(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("a1AWM", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $74c6a8455338bc68$var$isLocale;

var $74c6a8455338bc68$var$_assertString = $74c6a8455338bc68$var$_interopRequireDefault((parcelRequire("1khYO")));
function $74c6a8455338bc68$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $74c6a8455338bc68$var$localeReg = /^[A-Za-z]{2,4}([_-]([A-Za-z]{4}|[\d]{3}))?([_-]([A-Za-z]{2}|[\d]{3}))?$/;
function $74c6a8455338bc68$var$isLocale(str) {
    (0, $74c6a8455338bc68$var$_assertString.default)(str);
    if (str === "en_US_POSIX" || str === "ca_ES_VALENCIA") return true;
    return $74c6a8455338bc68$var$localeReg.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("gzCZU", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $c10e2a56a03d233a$var$isAlpha;
module.exports.locales = void 0;

var $c10e2a56a03d233a$var$_assertString = $c10e2a56a03d233a$var$_interopRequireDefault((parcelRequire("1khYO")));

var $hfuvR = parcelRequire("hfuvR");
function $c10e2a56a03d233a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $c10e2a56a03d233a$var$isAlpha(_str) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "en-US";
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    (0, $c10e2a56a03d233a$var$_assertString.default)(_str);
    var str = _str;
    var ignore = options.ignore;
    if (ignore) {
        if (ignore instanceof RegExp) str = str.replace(ignore, "");
        else if (typeof ignore === "string") str = str.replace(new RegExp("[".concat(ignore.replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, "\\$&"), "]"), "g"), ""); // escape regex for ignore
        else throw new Error("ignore should be instance of a String or RegExp");
    }
    if (locale in $hfuvR.alpha) return $hfuvR.alpha[locale].test(str);
    throw new Error("Invalid locale '".concat(locale, "'"));
}
var $c10e2a56a03d233a$var$locales = Object.keys($hfuvR.alpha);
module.exports.locales = $c10e2a56a03d233a$var$locales;

});

parcelRequire.register("7PUYk", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5b497d42c1a9593a$var$isAlphanumeric;
module.exports.locales = void 0;

var $5b497d42c1a9593a$var$_assertString = $5b497d42c1a9593a$var$_interopRequireDefault((parcelRequire("1khYO")));

var $hfuvR = parcelRequire("hfuvR");
function $5b497d42c1a9593a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $5b497d42c1a9593a$var$isAlphanumeric(_str) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "en-US";
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    (0, $5b497d42c1a9593a$var$_assertString.default)(_str);
    var str = _str;
    var ignore = options.ignore;
    if (ignore) {
        if (ignore instanceof RegExp) str = str.replace(ignore, "");
        else if (typeof ignore === "string") str = str.replace(new RegExp("[".concat(ignore.replace(/[-[\]{}()*+?.,\\^$|#\\s]/g, "\\$&"), "]"), "g"), ""); // escape regex for ignore
        else throw new Error("ignore should be instance of a String or RegExp");
    }
    if (locale in $hfuvR.alphanumeric) return $hfuvR.alphanumeric[locale].test(str);
    throw new Error("Invalid locale '".concat(locale, "'"));
}
var $5b497d42c1a9593a$var$locales = Object.keys($hfuvR.alphanumeric);
module.exports.locales = $5b497d42c1a9593a$var$locales;

});

parcelRequire.register("8RlnA", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $01aa207178f91344$var$isNumeric;

var $01aa207178f91344$var$_assertString = $01aa207178f91344$var$_interopRequireDefault((parcelRequire("1khYO")));

var $hfuvR = parcelRequire("hfuvR");
function $01aa207178f91344$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $01aa207178f91344$var$numericNoSymbols = /^[0-9]+$/;
function $01aa207178f91344$var$isNumeric(str, options) {
    (0, $01aa207178f91344$var$_assertString.default)(str);
    if (options && options.no_symbols) return $01aa207178f91344$var$numericNoSymbols.test(str);
    return new RegExp("^[+-]?([0-9]*[".concat((options || {}).locale ? $hfuvR.decimal[options.locale] : ".", "])?[0-9]+$")).test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("1c80o", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $0ded1f1d0fc5d14b$var$isPassportNumber;

var $0ded1f1d0fc5d14b$var$_assertString = $0ded1f1d0fc5d14b$var$_interopRequireDefault((parcelRequire("1khYO")));
function $0ded1f1d0fc5d14b$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/**
 * Reference:
 * https://en.wikipedia.org/ -- Wikipedia
 * https://docs.microsoft.com/en-us/microsoft-365/compliance/eu-passport-number -- EU Passport Number
 * https://countrycode.org/ -- Country Codes
 */ var $0ded1f1d0fc5d14b$var$passportRegexByCountryCode = {
    AM: /^[A-Z]{2}\d{7}$/,
    // ARMENIA
    AR: /^[A-Z]{3}\d{6}$/,
    // ARGENTINA
    AT: /^[A-Z]\d{7}$/,
    // AUSTRIA
    AU: /^[A-Z]\d{7}$/,
    // AUSTRALIA
    BE: /^[A-Z]{2}\d{6}$/,
    // BELGIUM
    BG: /^\d{9}$/,
    // BULGARIA
    BR: /^[A-Z]{2}\d{6}$/,
    // BRAZIL
    BY: /^[A-Z]{2}\d{7}$/,
    // BELARUS
    CA: /^[A-Z]{2}\d{6}$/,
    // CANADA
    CH: /^[A-Z]\d{7}$/,
    // SWITZERLAND
    CN: /^G\d{8}$|^E(?![IO])[A-Z0-9]\d{7}$/,
    // CHINA [G=Ordinary, E=Electronic] followed by 8-digits, or E followed by any UPPERCASE letter (except I and O) followed by 7 digits
    CY: /^[A-Z](\d{6}|\d{8})$/,
    // CYPRUS
    CZ: /^\d{8}$/,
    // CZECH REPUBLIC
    DE: /^[CFGHJKLMNPRTVWXYZ0-9]{9}$/,
    // GERMANY
    DK: /^\d{9}$/,
    // DENMARK
    DZ: /^\d{9}$/,
    // ALGERIA
    EE: /^([A-Z]\d{7}|[A-Z]{2}\d{7})$/,
    // ESTONIA (K followed by 7-digits), e-passports have 2 UPPERCASE followed by 7 digits
    ES: /^[A-Z0-9]{2}([A-Z0-9]?)\d{6}$/,
    // SPAIN
    FI: /^[A-Z]{2}\d{7}$/,
    // FINLAND
    FR: /^\d{2}[A-Z]{2}\d{5}$/,
    // FRANCE
    GB: /^\d{9}$/,
    // UNITED KINGDOM
    GR: /^[A-Z]{2}\d{7}$/,
    // GREECE
    HR: /^\d{9}$/,
    // CROATIA
    HU: /^[A-Z]{2}(\d{6}|\d{7})$/,
    // HUNGARY
    IE: /^[A-Z0-9]{2}\d{7}$/,
    // IRELAND
    IN: /^[A-Z]{1}-?\d{7}$/,
    // INDIA
    ID: /^[A-C]\d{7}$/,
    // INDONESIA
    IR: /^[A-Z]\d{8}$/,
    // IRAN
    IS: /^(A)\d{7}$/,
    // ICELAND
    IT: /^[A-Z0-9]{2}\d{7}$/,
    // ITALY
    JP: /^[A-Z]{2}\d{7}$/,
    // JAPAN
    KR: /^[MS]\d{8}$/,
    // SOUTH KOREA, REPUBLIC OF KOREA, [S=PS Passports, M=PM Passports]
    LT: /^[A-Z0-9]{8}$/,
    // LITHUANIA
    LU: /^[A-Z0-9]{8}$/,
    // LUXEMBURG
    LV: /^[A-Z0-9]{2}\d{7}$/,
    // LATVIA
    LY: /^[A-Z0-9]{8}$/,
    // LIBYA
    MT: /^\d{7}$/,
    // MALTA
    MZ: /^([A-Z]{2}\d{7})|(\d{2}[A-Z]{2}\d{5})$/,
    // MOZAMBIQUE
    MY: /^[AHK]\d{8}$/,
    // MALAYSIA
    NL: /^[A-Z]{2}[A-Z0-9]{6}\d$/,
    // NETHERLANDS
    PL: /^[A-Z]{2}\d{7}$/,
    // POLAND
    PT: /^[A-Z]\d{6}$/,
    // PORTUGAL
    RO: /^\d{8,9}$/,
    // ROMANIA
    RU: /^\d{9}$/,
    // RUSSIAN FEDERATION
    SE: /^\d{8}$/,
    // SWEDEN
    SL: /^(P)[A-Z]\d{7}$/,
    // SLOVANIA
    SK: /^[0-9A-Z]\d{7}$/,
    // SLOVAKIA
    TR: /^[A-Z]\d{8}$/,
    // TURKEY
    UA: /^[A-Z]{2}\d{6}$/,
    // UKRAINE
    US: /^\d{9}$/ // UNITED STATES
};
/**
 * Check if str is a valid passport number
 * relative to provided ISO Country Code.
 *
 * @param {string} str
 * @param {string} countryCode
 * @return {boolean}
 */ function $0ded1f1d0fc5d14b$var$isPassportNumber(str, countryCode) {
    (0, $0ded1f1d0fc5d14b$var$_assertString.default)(str);
    /** Remove All Whitespaces, Convert to UPPERCASE */ var normalizedStr = str.replace(/\s/g, "").toUpperCase();
    return countryCode.toUpperCase() in $0ded1f1d0fc5d14b$var$passportRegexByCountryCode && $0ded1f1d0fc5d14b$var$passportRegexByCountryCode[countryCode].test(normalizedStr);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("e2Kqq", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a39543b2f855b966$var$isPort;

var $a39543b2f855b966$var$_isInt = $a39543b2f855b966$var$_interopRequireDefault((parcelRequire("7j5R1")));
function $a39543b2f855b966$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $a39543b2f855b966$var$isPort(str) {
    return (0, $a39543b2f855b966$var$_isInt.default)(str, {
        min: 0,
        max: 65535
    });
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("7j5R1", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $551ed93f9a41c372$var$isInt;

var $551ed93f9a41c372$var$_assertString = $551ed93f9a41c372$var$_interopRequireDefault((parcelRequire("1khYO")));
function $551ed93f9a41c372$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $551ed93f9a41c372$var$int = /^(?:[-+]?(?:0|[1-9][0-9]*))$/;
var $551ed93f9a41c372$var$intLeadingZeroes = /^[-+]?[0-9]+$/;
function $551ed93f9a41c372$var$isInt(str, options) {
    (0, $551ed93f9a41c372$var$_assertString.default)(str);
    options = options || {}; // Get the regex to use for testing, based on whether
    // leading zeroes are allowed or not.
    var regex = options.hasOwnProperty("allow_leading_zeroes") && !options.allow_leading_zeroes ? $551ed93f9a41c372$var$int : $551ed93f9a41c372$var$intLeadingZeroes; // Check min/max/lt/gt
    var minCheckPassed = !options.hasOwnProperty("min") || str >= options.min;
    var maxCheckPassed = !options.hasOwnProperty("max") || str <= options.max;
    var ltCheckPassed = !options.hasOwnProperty("lt") || str < options.lt;
    var gtCheckPassed = !options.hasOwnProperty("gt") || str > options.gt;
    return regex.test(str) && minCheckPassed && maxCheckPassed && ltCheckPassed && gtCheckPassed;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("aeF61", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $773b170a472eaf2b$var$isLowercase;

var $773b170a472eaf2b$var$_assertString = $773b170a472eaf2b$var$_interopRequireDefault((parcelRequire("1khYO")));
function $773b170a472eaf2b$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $773b170a472eaf2b$var$isLowercase(str) {
    (0, $773b170a472eaf2b$var$_assertString.default)(str);
    return str === str.toLowerCase();
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("3DvQa", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $2a5db673b3ce7355$var$isUppercase;

var $2a5db673b3ce7355$var$_assertString = $2a5db673b3ce7355$var$_interopRequireDefault((parcelRequire("1khYO")));
function $2a5db673b3ce7355$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $2a5db673b3ce7355$var$isUppercase(str) {
    (0, $2a5db673b3ce7355$var$_assertString.default)(str);
    return str === str.toUpperCase();
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("flo0E", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $b2bb70f19a784c42$var$isIMEI;

var $b2bb70f19a784c42$var$_assertString = $b2bb70f19a784c42$var$_interopRequireDefault((parcelRequire("1khYO")));
function $b2bb70f19a784c42$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $b2bb70f19a784c42$var$imeiRegexWithoutHypens = /^[0-9]{15}$/;
var $b2bb70f19a784c42$var$imeiRegexWithHypens = /^\d{2}-\d{6}-\d{6}-\d{1}$/;
function $b2bb70f19a784c42$var$isIMEI(str, options) {
    (0, $b2bb70f19a784c42$var$_assertString.default)(str);
    options = options || {}; // default regex for checking imei is the one without hyphens
    var imeiRegex = $b2bb70f19a784c42$var$imeiRegexWithoutHypens;
    if (options.allow_hyphens) imeiRegex = $b2bb70f19a784c42$var$imeiRegexWithHypens;
    if (!imeiRegex.test(str)) return false;
    str = str.replace(/-/g, "");
    var sum = 0, mul = 2, l = 14;
    for(var i = 0; i < l; i++){
        var digit = str.substring(l - i - 1, l - i);
        var tp = parseInt(digit, 10) * mul;
        if (tp >= 10) sum += tp % 10 + 1;
        else sum += tp;
        if (mul === 1) mul += 1;
        else mul -= 1;
    }
    var chk = (10 - sum % 10) % 10;
    if (chk !== parseInt(str.substring(14, 15), 10)) return false;
    return true;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("1mWYz", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $0ff597efaf5acd1c$var$isAscii;

var $0ff597efaf5acd1c$var$_assertString = $0ff597efaf5acd1c$var$_interopRequireDefault((parcelRequire("1khYO")));
function $0ff597efaf5acd1c$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* eslint-disable no-control-regex */ var $0ff597efaf5acd1c$var$ascii = /^[\x00-\x7F]+$/;
/* eslint-enable no-control-regex */ function $0ff597efaf5acd1c$var$isAscii(str) {
    (0, $0ff597efaf5acd1c$var$_assertString.default)(str);
    return $0ff597efaf5acd1c$var$ascii.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7x82U", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $57c1db3e4b8a5286$var$isFullWidth;
module.exports.fullWidth = void 0;

var $57c1db3e4b8a5286$var$_assertString = $57c1db3e4b8a5286$var$_interopRequireDefault((parcelRequire("1khYO")));
function $57c1db3e4b8a5286$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $57c1db3e4b8a5286$var$fullWidth = /[^\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/;
module.exports.fullWidth = $57c1db3e4b8a5286$var$fullWidth;
function $57c1db3e4b8a5286$var$isFullWidth(str) {
    (0, $57c1db3e4b8a5286$var$_assertString.default)(str);
    return $57c1db3e4b8a5286$var$fullWidth.test(str);
}

});

parcelRequire.register("jh7hv", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $e08540c1637119af$var$isHalfWidth;
module.exports.halfWidth = void 0;

var $e08540c1637119af$var$_assertString = $e08540c1637119af$var$_interopRequireDefault((parcelRequire("1khYO")));
function $e08540c1637119af$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $e08540c1637119af$var$halfWidth = /[\u0020-\u007E\uFF61-\uFF9F\uFFA0-\uFFDC\uFFE8-\uFFEE0-9a-zA-Z]/;
module.exports.halfWidth = $e08540c1637119af$var$halfWidth;
function $e08540c1637119af$var$isHalfWidth(str) {
    (0, $e08540c1637119af$var$_assertString.default)(str);
    return $e08540c1637119af$var$halfWidth.test(str);
}

});

parcelRequire.register("1Y32c", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $16edbfa2db0c3c91$var$isVariableWidth;

var $16edbfa2db0c3c91$var$_assertString = $16edbfa2db0c3c91$var$_interopRequireDefault((parcelRequire("1khYO")));

var $7x82U = parcelRequire("7x82U");

var $jh7hv = parcelRequire("jh7hv");
function $16edbfa2db0c3c91$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $16edbfa2db0c3c91$var$isVariableWidth(str) {
    (0, $16edbfa2db0c3c91$var$_assertString.default)(str);
    return $7x82U.fullWidth.test(str) && $jh7hv.halfWidth.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("jCxMT", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $e48bc6728eb61041$var$isMultibyte;

var $e48bc6728eb61041$var$_assertString = $e48bc6728eb61041$var$_interopRequireDefault((parcelRequire("1khYO")));
function $e48bc6728eb61041$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* eslint-disable no-control-regex */ var $e48bc6728eb61041$var$multibyte = /[^\x00-\x7F]/;
/* eslint-enable no-control-regex */ function $e48bc6728eb61041$var$isMultibyte(str) {
    (0, $e48bc6728eb61041$var$_assertString.default)(str);
    return $e48bc6728eb61041$var$multibyte.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2iu3W", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $1ac495097d315d89$var$isSemVer;

var $1ac495097d315d89$var$_assertString = $1ac495097d315d89$var$_interopRequireDefault((parcelRequire("1khYO")));

var $1ac495097d315d89$var$_multilineRegex = $1ac495097d315d89$var$_interopRequireDefault((parcelRequire("ec286")));
function $1ac495097d315d89$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/**
 * Regular Expression to match
 * semantic versioning (SemVer)
 * built from multi-line, multi-parts regexp
 * Reference: https://semver.org/
 */ var $1ac495097d315d89$var$semanticVersioningRegex = (0, $1ac495097d315d89$var$_multilineRegex.default)([
    "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)",
    "(?:-((?:0|[1-9]\\d*|\\d*[a-z-][0-9a-z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-z-][0-9a-z-]*))*))",
    "?(?:\\+([0-9a-z-]+(?:\\.[0-9a-z-]+)*))?$"
], "i");
function $1ac495097d315d89$var$isSemVer(str) {
    (0, $1ac495097d315d89$var$_assertString.default)(str);
    return $1ac495097d315d89$var$semanticVersioningRegex.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("ec286", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a553d69250b1fb1f$var$multilineRegexp;
/**
 * Build RegExp object from an array
 * of multiple/multi-line regexp parts
 *
 * @param {string[]} parts
 * @param {string} flags
 * @return {object} - RegExp object
 */ function $a553d69250b1fb1f$var$multilineRegexp(parts, flags) {
    var regexpAsStringLiteral = parts.join("");
    return new RegExp(regexpAsStringLiteral, flags);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("hcmv6", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $c85502d19c8ee5bf$var$isSurrogatePair;

var $c85502d19c8ee5bf$var$_assertString = $c85502d19c8ee5bf$var$_interopRequireDefault((parcelRequire("1khYO")));
function $c85502d19c8ee5bf$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $c85502d19c8ee5bf$var$surrogatePair = /[\uD800-\uDBFF][\uDC00-\uDFFF]/;
function $c85502d19c8ee5bf$var$isSurrogatePair(str) {
    (0, $c85502d19c8ee5bf$var$_assertString.default)(str);
    return $c85502d19c8ee5bf$var$surrogatePair.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("bpbov", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $84dade26e460ebd7$var$isDecimal;

var $84dade26e460ebd7$var$_merge = $84dade26e460ebd7$var$_interopRequireDefault((parcelRequire("lIjC8")));

var $84dade26e460ebd7$var$_assertString = $84dade26e460ebd7$var$_interopRequireDefault((parcelRequire("1khYO")));

var $84dade26e460ebd7$var$_includes = $84dade26e460ebd7$var$_interopRequireDefault((parcelRequire("4itpJ")));

var $hfuvR = parcelRequire("hfuvR");
function $84dade26e460ebd7$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $84dade26e460ebd7$var$decimalRegExp(options) {
    var regExp = new RegExp("^[-+]?([0-9]+)?(\\".concat($hfuvR.decimal[options.locale], "[0-9]{").concat(options.decimal_digits, "})").concat(options.force_decimal ? "" : "?", "$"));
    return regExp;
}
var $84dade26e460ebd7$var$default_decimal_options = {
    force_decimal: false,
    decimal_digits: "1,",
    locale: "en-US"
};
var $84dade26e460ebd7$var$blacklist = [
    "",
    "-",
    "+"
];
function $84dade26e460ebd7$var$isDecimal(str, options) {
    (0, $84dade26e460ebd7$var$_assertString.default)(str);
    options = (0, $84dade26e460ebd7$var$_merge.default)(options, $84dade26e460ebd7$var$default_decimal_options);
    if (options.locale in $hfuvR.decimal) return !(0, $84dade26e460ebd7$var$_includes.default)($84dade26e460ebd7$var$blacklist, str.replace(/ /g, "")) && $84dade26e460ebd7$var$decimalRegExp(options).test(str);
    throw new Error("Invalid locale '".concat(options.locale, "'"));
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("4itpJ", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = void 0;
var $320fa8e2bfce2836$var$includes = function includes(arr, val) {
    return arr.some(function(arrVal) {
        return val === arrVal;
    });
};
var $320fa8e2bfce2836$var$_default = $320fa8e2bfce2836$var$includes;
module.exports.default = $320fa8e2bfce2836$var$_default;
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("heG7h", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $c8c469ef416bf897$var$isHexadecimal;

var $c8c469ef416bf897$var$_assertString = $c8c469ef416bf897$var$_interopRequireDefault((parcelRequire("1khYO")));
function $c8c469ef416bf897$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $c8c469ef416bf897$var$hexadecimal = /^(0x|0h)?[0-9A-F]+$/i;
function $c8c469ef416bf897$var$isHexadecimal(str) {
    (0, $c8c469ef416bf897$var$_assertString.default)(str);
    return $c8c469ef416bf897$var$hexadecimal.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("6vCo5", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $4bd36a442b89539a$var$isOctal;

var $4bd36a442b89539a$var$_assertString = $4bd36a442b89539a$var$_interopRequireDefault((parcelRequire("1khYO")));
function $4bd36a442b89539a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $4bd36a442b89539a$var$octal = /^(0o)?[0-7]+$/i;
function $4bd36a442b89539a$var$isOctal(str) {
    (0, $4bd36a442b89539a$var$_assertString.default)(str);
    return $4bd36a442b89539a$var$octal.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("8eteF", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5fe64f1b4778a231$var$isDivisibleBy;

var $5fe64f1b4778a231$var$_assertString = $5fe64f1b4778a231$var$_interopRequireDefault((parcelRequire("1khYO")));

var $5fe64f1b4778a231$var$_toFloat = $5fe64f1b4778a231$var$_interopRequireDefault((parcelRequire("b9wlP")));
function $5fe64f1b4778a231$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $5fe64f1b4778a231$var$isDivisibleBy(str, num) {
    (0, $5fe64f1b4778a231$var$_assertString.default)(str);
    return (0, $5fe64f1b4778a231$var$_toFloat.default)(str) % parseInt(num, 10) === 0;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2eDDw", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $1a0ba28ec3a2eb6d$var$isHexColor;

var $1a0ba28ec3a2eb6d$var$_assertString = $1a0ba28ec3a2eb6d$var$_interopRequireDefault((parcelRequire("1khYO")));
function $1a0ba28ec3a2eb6d$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $1a0ba28ec3a2eb6d$var$hexcolor = /^#?([0-9A-F]{3}|[0-9A-F]{4}|[0-9A-F]{6}|[0-9A-F]{8})$/i;
function $1a0ba28ec3a2eb6d$var$isHexColor(str) {
    (0, $1a0ba28ec3a2eb6d$var$_assertString.default)(str);
    return $1a0ba28ec3a2eb6d$var$hexcolor.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("lYDV0", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $fffe2a8ee258b6db$var$isRgbColor;

var $fffe2a8ee258b6db$var$_assertString = $fffe2a8ee258b6db$var$_interopRequireDefault((parcelRequire("1khYO")));
function $fffe2a8ee258b6db$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $fffe2a8ee258b6db$var$rgbColor = /^rgb\((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),){2}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\)$/;
var $fffe2a8ee258b6db$var$rgbaColor = /^rgba\((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),){3}(0?\.\d|1(\.0)?|0(\.0)?)\)$/;
var $fffe2a8ee258b6db$var$rgbColorPercent = /^rgb\((([0-9]%|[1-9][0-9]%|100%),){2}([0-9]%|[1-9][0-9]%|100%)\)/;
var $fffe2a8ee258b6db$var$rgbaColorPercent = /^rgba\((([0-9]%|[1-9][0-9]%|100%),){3}(0?\.\d|1(\.0)?|0(\.0)?)\)/;
function $fffe2a8ee258b6db$var$isRgbColor(str) {
    var includePercentValues = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    (0, $fffe2a8ee258b6db$var$_assertString.default)(str);
    if (!includePercentValues) return $fffe2a8ee258b6db$var$rgbColor.test(str) || $fffe2a8ee258b6db$var$rgbaColor.test(str);
    return $fffe2a8ee258b6db$var$rgbColor.test(str) || $fffe2a8ee258b6db$var$rgbaColor.test(str) || $fffe2a8ee258b6db$var$rgbColorPercent.test(str) || $fffe2a8ee258b6db$var$rgbaColorPercent.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("hYcXt", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $d15217845cf47cb0$var$isHSL;

var $d15217845cf47cb0$var$_assertString = $d15217845cf47cb0$var$_interopRequireDefault((parcelRequire("1khYO")));
function $d15217845cf47cb0$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $d15217845cf47cb0$var$hslComma = /^hsla?\(((\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?))(deg|grad|rad|turn)?(,(\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?)%){2}(,((\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?)%?))?\)$/i;
var $d15217845cf47cb0$var$hslSpace = /^hsla?\(((\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?))(deg|grad|rad|turn)?(\s(\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?)%){2}\s?(\/\s((\+|\-)?([0-9]+(\.[0-9]+)?(e(\+|\-)?[0-9]+)?|\.[0-9]+(e(\+|\-)?[0-9]+)?)%?)\s?)?\)$/i;
function $d15217845cf47cb0$var$isHSL(str) {
    (0, $d15217845cf47cb0$var$_assertString.default)(str); // Strip duplicate spaces before calling the validation regex (See  #1598 for more info)
    var strippedStr = str.replace(/\s+/g, " ").replace(/\s?(hsla?\(|\)|,)\s?/ig, "$1");
    if (strippedStr.indexOf(",") !== -1) return $d15217845cf47cb0$var$hslComma.test(strippedStr);
    return $d15217845cf47cb0$var$hslSpace.test(strippedStr);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2Kqyz", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $20047a41b016c6c9$var$isISRC;

var $20047a41b016c6c9$var$_assertString = $20047a41b016c6c9$var$_interopRequireDefault((parcelRequire("1khYO")));
function $20047a41b016c6c9$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// see http://isrc.ifpi.org/en/isrc-standard/code-syntax
var $20047a41b016c6c9$var$isrc = /^[A-Z]{2}[0-9A-Z]{3}\d{2}\d{5}$/;
function $20047a41b016c6c9$var$isISRC(str) {
    (0, $20047a41b016c6c9$var$_assertString.default)(str);
    return $20047a41b016c6c9$var$isrc.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("4xd2a", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $34d45abdedea579b$var$isIBAN;
module.exports.locales = void 0;

var $34d45abdedea579b$var$_assertString = $34d45abdedea579b$var$_interopRequireDefault((parcelRequire("1khYO")));
function $34d45abdedea579b$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/**
 * List of country codes with
 * corresponding IBAN regular expression
 * Reference: https://en.wikipedia.org/wiki/International_Bank_Account_Number
 */ var $34d45abdedea579b$var$ibanRegexThroughCountryCode = {
    AD: /^(AD[0-9]{2})\d{8}[A-Z0-9]{12}$/,
    AE: /^(AE[0-9]{2})\d{3}\d{16}$/,
    AL: /^(AL[0-9]{2})\d{8}[A-Z0-9]{16}$/,
    AT: /^(AT[0-9]{2})\d{16}$/,
    AZ: /^(AZ[0-9]{2})[A-Z0-9]{4}\d{20}$/,
    BA: /^(BA[0-9]{2})\d{16}$/,
    BE: /^(BE[0-9]{2})\d{12}$/,
    BG: /^(BG[0-9]{2})[A-Z]{4}\d{6}[A-Z0-9]{8}$/,
    BH: /^(BH[0-9]{2})[A-Z]{4}[A-Z0-9]{14}$/,
    BR: /^(BR[0-9]{2})\d{23}[A-Z]{1}[A-Z0-9]{1}$/,
    BY: /^(BY[0-9]{2})[A-Z0-9]{4}\d{20}$/,
    CH: /^(CH[0-9]{2})\d{5}[A-Z0-9]{12}$/,
    CR: /^(CR[0-9]{2})\d{18}$/,
    CY: /^(CY[0-9]{2})\d{8}[A-Z0-9]{16}$/,
    CZ: /^(CZ[0-9]{2})\d{20}$/,
    DE: /^(DE[0-9]{2})\d{18}$/,
    DK: /^(DK[0-9]{2})\d{14}$/,
    DO: /^(DO[0-9]{2})[A-Z]{4}\d{20}$/,
    EE: /^(EE[0-9]{2})\d{16}$/,
    EG: /^(EG[0-9]{2})\d{25}$/,
    ES: /^(ES[0-9]{2})\d{20}$/,
    FI: /^(FI[0-9]{2})\d{14}$/,
    FO: /^(FO[0-9]{2})\d{14}$/,
    FR: /^(FR[0-9]{2})\d{10}[A-Z0-9]{11}\d{2}$/,
    GB: /^(GB[0-9]{2})[A-Z]{4}\d{14}$/,
    GE: /^(GE[0-9]{2})[A-Z0-9]{2}\d{16}$/,
    GI: /^(GI[0-9]{2})[A-Z]{4}[A-Z0-9]{15}$/,
    GL: /^(GL[0-9]{2})\d{14}$/,
    GR: /^(GR[0-9]{2})\d{7}[A-Z0-9]{16}$/,
    GT: /^(GT[0-9]{2})[A-Z0-9]{4}[A-Z0-9]{20}$/,
    HR: /^(HR[0-9]{2})\d{17}$/,
    HU: /^(HU[0-9]{2})\d{24}$/,
    IE: /^(IE[0-9]{2})[A-Z0-9]{4}\d{14}$/,
    IL: /^(IL[0-9]{2})\d{19}$/,
    IQ: /^(IQ[0-9]{2})[A-Z]{4}\d{15}$/,
    IR: /^(IR[0-9]{2})0\d{2}0\d{18}$/,
    IS: /^(IS[0-9]{2})\d{22}$/,
    IT: /^(IT[0-9]{2})[A-Z]{1}\d{10}[A-Z0-9]{12}$/,
    JO: /^(JO[0-9]{2})[A-Z]{4}\d{22}$/,
    KW: /^(KW[0-9]{2})[A-Z]{4}[A-Z0-9]{22}$/,
    KZ: /^(KZ[0-9]{2})\d{3}[A-Z0-9]{13}$/,
    LB: /^(LB[0-9]{2})\d{4}[A-Z0-9]{20}$/,
    LC: /^(LC[0-9]{2})[A-Z]{4}[A-Z0-9]{24}$/,
    LI: /^(LI[0-9]{2})\d{5}[A-Z0-9]{12}$/,
    LT: /^(LT[0-9]{2})\d{16}$/,
    LU: /^(LU[0-9]{2})\d{3}[A-Z0-9]{13}$/,
    LV: /^(LV[0-9]{2})[A-Z]{4}[A-Z0-9]{13}$/,
    MC: /^(MC[0-9]{2})\d{10}[A-Z0-9]{11}\d{2}$/,
    MD: /^(MD[0-9]{2})[A-Z0-9]{20}$/,
    ME: /^(ME[0-9]{2})\d{18}$/,
    MK: /^(MK[0-9]{2})\d{3}[A-Z0-9]{10}\d{2}$/,
    MR: /^(MR[0-9]{2})\d{23}$/,
    MT: /^(MT[0-9]{2})[A-Z]{4}\d{5}[A-Z0-9]{18}$/,
    MU: /^(MU[0-9]{2})[A-Z]{4}\d{19}[A-Z]{3}$/,
    MZ: /^(MZ[0-9]{2})\d{21}$/,
    NL: /^(NL[0-9]{2})[A-Z]{4}\d{10}$/,
    NO: /^(NO[0-9]{2})\d{11}$/,
    PK: /^(PK[0-9]{2})[A-Z0-9]{4}\d{16}$/,
    PL: /^(PL[0-9]{2})\d{24}$/,
    PS: /^(PS[0-9]{2})[A-Z0-9]{4}\d{21}$/,
    PT: /^(PT[0-9]{2})\d{21}$/,
    QA: /^(QA[0-9]{2})[A-Z]{4}[A-Z0-9]{21}$/,
    RO: /^(RO[0-9]{2})[A-Z]{4}[A-Z0-9]{16}$/,
    RS: /^(RS[0-9]{2})\d{18}$/,
    SA: /^(SA[0-9]{2})\d{2}[A-Z0-9]{18}$/,
    SC: /^(SC[0-9]{2})[A-Z]{4}\d{20}[A-Z]{3}$/,
    SE: /^(SE[0-9]{2})\d{20}$/,
    SI: /^(SI[0-9]{2})\d{15}$/,
    SK: /^(SK[0-9]{2})\d{20}$/,
    SM: /^(SM[0-9]{2})[A-Z]{1}\d{10}[A-Z0-9]{12}$/,
    SV: /^(SV[0-9]{2})[A-Z0-9]{4}\d{20}$/,
    TL: /^(TL[0-9]{2})\d{19}$/,
    TN: /^(TN[0-9]{2})\d{20}$/,
    TR: /^(TR[0-9]{2})\d{5}[A-Z0-9]{17}$/,
    UA: /^(UA[0-9]{2})\d{6}[A-Z0-9]{19}$/,
    VA: /^(VA[0-9]{2})\d{18}$/,
    VG: /^(VG[0-9]{2})[A-Z0-9]{4}\d{16}$/,
    XK: /^(XK[0-9]{2})\d{16}$/
};
/**
 * Check whether string has correct universal IBAN format
 * The IBAN consists of up to 34 alphanumeric characters, as follows:
 * Country Code using ISO 3166-1 alpha-2, two letters
 * check digits, two digits and
 * Basic Bank Account Number (BBAN), up to 30 alphanumeric characters.
 * NOTE: Permitted IBAN characters are: digits [0-9] and the 26 latin alphabetic [A-Z]
 *
 * @param {string} str - string under validation
 * @return {boolean}
 */ function $34d45abdedea579b$var$hasValidIbanFormat(str) {
    // Strip white spaces and hyphens
    var strippedStr = str.replace(/[\s\-]+/gi, "").toUpperCase();
    var isoCountryCode = strippedStr.slice(0, 2).toUpperCase();
    return isoCountryCode in $34d45abdedea579b$var$ibanRegexThroughCountryCode && $34d45abdedea579b$var$ibanRegexThroughCountryCode[isoCountryCode].test(strippedStr);
}
/**
   * Check whether string has valid IBAN Checksum
   * by performing basic mod-97 operation and
   * the remainder should equal 1
   * -- Start by rearranging the IBAN by moving the four initial characters to the end of the string
   * -- Replace each letter in the string with two digits, A -> 10, B = 11, Z = 35
   * -- Interpret the string as a decimal integer and
   * -- compute the remainder on division by 97 (mod 97)
   * Reference: https://en.wikipedia.org/wiki/International_Bank_Account_Number
   *
   * @param {string} str
   * @return {boolean}
   */ function $34d45abdedea579b$var$hasValidIbanChecksum(str) {
    var strippedStr = str.replace(/[^A-Z0-9]+/gi, "").toUpperCase(); // Keep only digits and A-Z latin alphabetic
    var rearranged = strippedStr.slice(4) + strippedStr.slice(0, 4);
    var alphaCapsReplacedWithDigits = rearranged.replace(/[A-Z]/g, function(char) {
        return char.charCodeAt(0) - 55;
    });
    var remainder = alphaCapsReplacedWithDigits.match(/\d{1,7}/g).reduce(function(acc, value) {
        return Number(acc + value) % 97;
    }, "");
    return remainder === 1;
}
function $34d45abdedea579b$var$isIBAN(str) {
    (0, $34d45abdedea579b$var$_assertString.default)(str);
    return $34d45abdedea579b$var$hasValidIbanFormat(str) && $34d45abdedea579b$var$hasValidIbanChecksum(str);
}
var $34d45abdedea579b$var$locales = Object.keys($34d45abdedea579b$var$ibanRegexThroughCountryCode);
module.exports.locales = $34d45abdedea579b$var$locales;

});

parcelRequire.register("5OY2Z", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $43d033601c7c7fa7$var$isBIC;

var $43d033601c7c7fa7$var$_assertString = $43d033601c7c7fa7$var$_interopRequireDefault((parcelRequire("1khYO")));

var $estEF = parcelRequire("estEF");
function $43d033601c7c7fa7$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// https://en.wikipedia.org/wiki/ISO_9362
var $43d033601c7c7fa7$var$isBICReg = /^[A-Za-z]{6}[A-Za-z0-9]{2}([A-Za-z0-9]{3})?$/;
function $43d033601c7c7fa7$var$isBIC(str) {
    (0, $43d033601c7c7fa7$var$_assertString.default)(str); // toUpperCase() should be removed when a new major version goes out that changes
    // the regex to [A-Z] (per the spec).
    if (!$estEF.CountryCodes.has(str.slice(4, 6).toUpperCase())) return false;
    return $43d033601c7c7fa7$var$isBICReg.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("estEF", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a86aaf15d3e590e3$var$isISO31661Alpha2;
module.exports.CountryCodes = void 0;

var $a86aaf15d3e590e3$var$_assertString = $a86aaf15d3e590e3$var$_interopRequireDefault((parcelRequire("1khYO")));
function $a86aaf15d3e590e3$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// from https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
var $a86aaf15d3e590e3$var$validISO31661Alpha2CountriesCodes = new Set([
    "AD",
    "AE",
    "AF",
    "AG",
    "AI",
    "AL",
    "AM",
    "AO",
    "AQ",
    "AR",
    "AS",
    "AT",
    "AU",
    "AW",
    "AX",
    "AZ",
    "BA",
    "BB",
    "BD",
    "BE",
    "BF",
    "BG",
    "BH",
    "BI",
    "BJ",
    "BL",
    "BM",
    "BN",
    "BO",
    "BQ",
    "BR",
    "BS",
    "BT",
    "BV",
    "BW",
    "BY",
    "BZ",
    "CA",
    "CC",
    "CD",
    "CF",
    "CG",
    "CH",
    "CI",
    "CK",
    "CL",
    "CM",
    "CN",
    "CO",
    "CR",
    "CU",
    "CV",
    "CW",
    "CX",
    "CY",
    "CZ",
    "DE",
    "DJ",
    "DK",
    "DM",
    "DO",
    "DZ",
    "EC",
    "EE",
    "EG",
    "EH",
    "ER",
    "ES",
    "ET",
    "FI",
    "FJ",
    "FK",
    "FM",
    "FO",
    "FR",
    "GA",
    "GB",
    "GD",
    "GE",
    "GF",
    "GG",
    "GH",
    "GI",
    "GL",
    "GM",
    "GN",
    "GP",
    "GQ",
    "GR",
    "GS",
    "GT",
    "GU",
    "GW",
    "GY",
    "HK",
    "HM",
    "HN",
    "HR",
    "HT",
    "HU",
    "ID",
    "IE",
    "IL",
    "IM",
    "IN",
    "IO",
    "IQ",
    "IR",
    "IS",
    "IT",
    "JE",
    "JM",
    "JO",
    "JP",
    "KE",
    "KG",
    "KH",
    "KI",
    "KM",
    "KN",
    "KP",
    "KR",
    "KW",
    "KY",
    "KZ",
    "LA",
    "LB",
    "LC",
    "LI",
    "LK",
    "LR",
    "LS",
    "LT",
    "LU",
    "LV",
    "LY",
    "MA",
    "MC",
    "MD",
    "ME",
    "MF",
    "MG",
    "MH",
    "MK",
    "ML",
    "MM",
    "MN",
    "MO",
    "MP",
    "MQ",
    "MR",
    "MS",
    "MT",
    "MU",
    "MV",
    "MW",
    "MX",
    "MY",
    "MZ",
    "NA",
    "NC",
    "NE",
    "NF",
    "NG",
    "NI",
    "NL",
    "NO",
    "NP",
    "NR",
    "NU",
    "NZ",
    "OM",
    "PA",
    "PE",
    "PF",
    "PG",
    "PH",
    "PK",
    "PL",
    "PM",
    "PN",
    "PR",
    "PS",
    "PT",
    "PW",
    "PY",
    "QA",
    "RE",
    "RO",
    "RS",
    "RU",
    "RW",
    "SA",
    "SB",
    "SC",
    "SD",
    "SE",
    "SG",
    "SH",
    "SI",
    "SJ",
    "SK",
    "SL",
    "SM",
    "SN",
    "SO",
    "SR",
    "SS",
    "ST",
    "SV",
    "SX",
    "SY",
    "SZ",
    "TC",
    "TD",
    "TF",
    "TG",
    "TH",
    "TJ",
    "TK",
    "TL",
    "TM",
    "TN",
    "TO",
    "TR",
    "TT",
    "TV",
    "TW",
    "TZ",
    "UA",
    "UG",
    "UM",
    "US",
    "UY",
    "UZ",
    "VA",
    "VC",
    "VE",
    "VG",
    "VI",
    "VN",
    "VU",
    "WF",
    "WS",
    "YE",
    "YT",
    "ZA",
    "ZM",
    "ZW"
]);
function $a86aaf15d3e590e3$var$isISO31661Alpha2(str) {
    (0, $a86aaf15d3e590e3$var$_assertString.default)(str);
    return $a86aaf15d3e590e3$var$validISO31661Alpha2CountriesCodes.has(str.toUpperCase());
}
var $a86aaf15d3e590e3$var$CountryCodes = $a86aaf15d3e590e3$var$validISO31661Alpha2CountriesCodes;
module.exports.CountryCodes = $a86aaf15d3e590e3$var$CountryCodes;

});


parcelRequire.register("bEev4", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $87aead894734a4e3$var$isMD5;

var $87aead894734a4e3$var$_assertString = $87aead894734a4e3$var$_interopRequireDefault((parcelRequire("1khYO")));
function $87aead894734a4e3$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $87aead894734a4e3$var$md5 = /^[a-f0-9]{32}$/;
function $87aead894734a4e3$var$isMD5(str) {
    (0, $87aead894734a4e3$var$_assertString.default)(str);
    return $87aead894734a4e3$var$md5.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("lekj3", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $f74aa82f38dfadcd$var$isHash;

var $f74aa82f38dfadcd$var$_assertString = $f74aa82f38dfadcd$var$_interopRequireDefault((parcelRequire("1khYO")));
function $f74aa82f38dfadcd$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $f74aa82f38dfadcd$var$lengths = {
    md5: 32,
    md4: 32,
    sha1: 40,
    sha256: 64,
    sha384: 96,
    sha512: 128,
    ripemd128: 32,
    ripemd160: 40,
    tiger128: 32,
    tiger160: 40,
    tiger192: 48,
    crc32: 8,
    crc32b: 8
};
function $f74aa82f38dfadcd$var$isHash(str, algorithm) {
    (0, $f74aa82f38dfadcd$var$_assertString.default)(str);
    var hash = new RegExp("^[a-fA-F0-9]{".concat($f74aa82f38dfadcd$var$lengths[algorithm], "}$"));
    return hash.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2JO4r", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $1fe69fb4f2326582$var$isJWT;

var $1fe69fb4f2326582$var$_assertString = $1fe69fb4f2326582$var$_interopRequireDefault((parcelRequire("1khYO")));

var $1fe69fb4f2326582$var$_isBase = $1fe69fb4f2326582$var$_interopRequireDefault((parcelRequire("cVIvk")));
function $1fe69fb4f2326582$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $1fe69fb4f2326582$var$isJWT(str) {
    (0, $1fe69fb4f2326582$var$_assertString.default)(str);
    var dotSplit = str.split(".");
    var len = dotSplit.length;
    if (len > 3 || len < 2) return false;
    return dotSplit.reduce(function(acc, currElem) {
        return acc && (0, $1fe69fb4f2326582$var$_isBase.default)(currElem, {
            urlSafe: true
        });
    }, true);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("cVIvk", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $969d54883a78151c$var$isBase64;

var $969d54883a78151c$var$_assertString = $969d54883a78151c$var$_interopRequireDefault((parcelRequire("1khYO")));

var $969d54883a78151c$var$_merge = $969d54883a78151c$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $969d54883a78151c$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $969d54883a78151c$var$notBase64 = /[^A-Z0-9+\/=]/i;
var $969d54883a78151c$var$urlSafeBase64 = /^[A-Z0-9_\-]*$/i;
var $969d54883a78151c$var$defaultBase64Options = {
    urlSafe: false
};
function $969d54883a78151c$var$isBase64(str, options) {
    (0, $969d54883a78151c$var$_assertString.default)(str);
    options = (0, $969d54883a78151c$var$_merge.default)(options, $969d54883a78151c$var$defaultBase64Options);
    var len = str.length;
    if (options.urlSafe) return $969d54883a78151c$var$urlSafeBase64.test(str);
    if (len % 4 !== 0 || $969d54883a78151c$var$notBase64.test(str)) return false;
    var firstPaddingChar = str.indexOf("=");
    return firstPaddingChar === -1 || firstPaddingChar === len - 1 || firstPaddingChar === len - 2 && str[len - 1] === "=";
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("hEi2B", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $cd942aab514db81a$var$isJSON;

var $cd942aab514db81a$var$_assertString = $cd942aab514db81a$var$_interopRequireDefault((parcelRequire("1khYO")));

var $cd942aab514db81a$var$_merge = $cd942aab514db81a$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $cd942aab514db81a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $cd942aab514db81a$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $cd942aab514db81a$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $cd942aab514db81a$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $cd942aab514db81a$var$_typeof(obj);
}
var $cd942aab514db81a$var$default_json_options = {
    allow_primitives: false
};
function $cd942aab514db81a$var$isJSON(str, options) {
    (0, $cd942aab514db81a$var$_assertString.default)(str);
    try {
        options = (0, $cd942aab514db81a$var$_merge.default)(options, $cd942aab514db81a$var$default_json_options);
        var primitives = [];
        if (options.allow_primitives) primitives = [
            null,
            false,
            true
        ];
        var obj = JSON.parse(str);
        return primitives.includes(obj) || !!obj && $cd942aab514db81a$var$_typeof(obj) === "object";
    } catch (e) {
    /* ignore */ }
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("8PGgv", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $66e3df218ed1205e$var$isEmpty;

var $66e3df218ed1205e$var$_assertString = $66e3df218ed1205e$var$_interopRequireDefault((parcelRequire("1khYO")));

var $66e3df218ed1205e$var$_merge = $66e3df218ed1205e$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $66e3df218ed1205e$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $66e3df218ed1205e$var$default_is_empty_options = {
    ignore_whitespace: false
};
function $66e3df218ed1205e$var$isEmpty(str, options) {
    (0, $66e3df218ed1205e$var$_assertString.default)(str);
    options = (0, $66e3df218ed1205e$var$_merge.default)(options, $66e3df218ed1205e$var$default_is_empty_options);
    return (options.ignore_whitespace ? str.trim().length : str.length) === 0;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("ennxN", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a7757a09fb10c204$var$isLength;

var $a7757a09fb10c204$var$_assertString = $a7757a09fb10c204$var$_interopRequireDefault((parcelRequire("1khYO")));
function $a7757a09fb10c204$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $a7757a09fb10c204$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $a7757a09fb10c204$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $a7757a09fb10c204$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $a7757a09fb10c204$var$_typeof(obj);
}
/* eslint-disable prefer-rest-params */ function $a7757a09fb10c204$var$isLength(str, options) {
    (0, $a7757a09fb10c204$var$_assertString.default)(str);
    var min;
    var max;
    if ($a7757a09fb10c204$var$_typeof(options) === "object") {
        min = options.min || 0;
        max = options.max;
    } else {
        // backwards compatibility: isLength(str, min [, max])
        min = arguments[1] || 0;
        max = arguments[2];
    }
    var surrogatePairs = str.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [];
    var len = str.length - surrogatePairs.length;
    return len >= min && (typeof max === "undefined" || len <= max);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("5UlZM", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $44d33e91697bfeb5$var$isUUID;

var $44d33e91697bfeb5$var$_assertString = $44d33e91697bfeb5$var$_interopRequireDefault((parcelRequire("1khYO")));
function $44d33e91697bfeb5$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $44d33e91697bfeb5$var$uuid = {
    1: /^[0-9A-F]{8}-[0-9A-F]{4}-1[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
    2: /^[0-9A-F]{8}-[0-9A-F]{4}-2[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
    3: /^[0-9A-F]{8}-[0-9A-F]{4}-3[0-9A-F]{3}-[0-9A-F]{4}-[0-9A-F]{12}$/i,
    4: /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
    5: /^[0-9A-F]{8}-[0-9A-F]{4}-5[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i,
    all: /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i
};
function $44d33e91697bfeb5$var$isUUID(str, version) {
    (0, $44d33e91697bfeb5$var$_assertString.default)(str);
    var pattern = $44d33e91697bfeb5$var$uuid[![
        undefined,
        null
    ].includes(version) ? version : "all"];
    return !!pattern && pattern.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("feSlP", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $b18252d1ca7b7814$var$isMongoId;

var $b18252d1ca7b7814$var$_assertString = $b18252d1ca7b7814$var$_interopRequireDefault((parcelRequire("1khYO")));

var $b18252d1ca7b7814$var$_isHexadecimal = $b18252d1ca7b7814$var$_interopRequireDefault((parcelRequire("heG7h")));
function $b18252d1ca7b7814$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $b18252d1ca7b7814$var$isMongoId(str) {
    (0, $b18252d1ca7b7814$var$_assertString.default)(str);
    return (0, $b18252d1ca7b7814$var$_isHexadecimal.default)(str) && str.length === 24;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("9WiHS", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $73c80a9299192680$var$isAfter;

var $73c80a9299192680$var$_assertString = $73c80a9299192680$var$_interopRequireDefault((parcelRequire("1khYO")));

var $73c80a9299192680$var$_toDate = $73c80a9299192680$var$_interopRequireDefault((parcelRequire("5AZVd")));
function $73c80a9299192680$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $73c80a9299192680$var$isAfter(str) {
    var date = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : String(new Date());
    (0, $73c80a9299192680$var$_assertString.default)(str);
    var comparison = (0, $73c80a9299192680$var$_toDate.default)(date);
    var original = (0, $73c80a9299192680$var$_toDate.default)(str);
    return !!(original && comparison && original > comparison);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("3B2a9", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $29e6814ae2a1ef9b$var$isBefore;

var $29e6814ae2a1ef9b$var$_assertString = $29e6814ae2a1ef9b$var$_interopRequireDefault((parcelRequire("1khYO")));

var $29e6814ae2a1ef9b$var$_toDate = $29e6814ae2a1ef9b$var$_interopRequireDefault((parcelRequire("5AZVd")));
function $29e6814ae2a1ef9b$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $29e6814ae2a1ef9b$var$isBefore(str) {
    var date = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : String(new Date());
    (0, $29e6814ae2a1ef9b$var$_assertString.default)(str);
    var comparison = (0, $29e6814ae2a1ef9b$var$_toDate.default)(date);
    var original = (0, $29e6814ae2a1ef9b$var$_toDate.default)(str);
    return !!(original && comparison && original < comparison);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("6lIlX", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $49f71bde90f0dd49$var$isIn;

var $49f71bde90f0dd49$var$_assertString = $49f71bde90f0dd49$var$_interopRequireDefault((parcelRequire("1khYO")));

var $49f71bde90f0dd49$var$_toString = $49f71bde90f0dd49$var$_interopRequireDefault((parcelRequire("liR8H")));
function $49f71bde90f0dd49$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $49f71bde90f0dd49$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $49f71bde90f0dd49$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $49f71bde90f0dd49$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $49f71bde90f0dd49$var$_typeof(obj);
}
function $49f71bde90f0dd49$var$isIn(str, options) {
    (0, $49f71bde90f0dd49$var$_assertString.default)(str);
    var i;
    if (Object.prototype.toString.call(options) === "[object Array]") {
        var array = [];
        for(i in options)// https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md#ignoring-code-for-coverage-purposes
        // istanbul ignore else
        if (({}).hasOwnProperty.call(options, i)) array[i] = (0, $49f71bde90f0dd49$var$_toString.default)(options[i]);
        return array.indexOf(str) >= 0;
    } else if ($49f71bde90f0dd49$var$_typeof(options) === "object") return options.hasOwnProperty(str);
    else if (options && typeof options.indexOf === "function") return options.indexOf(str) >= 0;
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7hMzn", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $54dfcbda9fe024de$var$isCreditCard;

var $54dfcbda9fe024de$var$_assertString = $54dfcbda9fe024de$var$_interopRequireDefault((parcelRequire("1khYO")));
function $54dfcbda9fe024de$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* eslint-disable max-len */ var $54dfcbda9fe024de$var$creditCard = /^(?:4[0-9]{12}(?:[0-9]{3,6})?|5[1-5][0-9]{14}|(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|6(?:011|5[0-9][0-9])[0-9]{12,15}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11}|6[27][0-9]{14}|^(81[0-9]{14,17}))$/;
/* eslint-enable max-len */ function $54dfcbda9fe024de$var$isCreditCard(str) {
    (0, $54dfcbda9fe024de$var$_assertString.default)(str);
    var sanitized = str.replace(/[- ]+/g, "");
    if (!$54dfcbda9fe024de$var$creditCard.test(sanitized)) return false;
    var sum = 0;
    var digit;
    var tmpNum;
    var shouldDouble;
    for(var i = sanitized.length - 1; i >= 0; i--){
        digit = sanitized.substring(i, i + 1);
        tmpNum = parseInt(digit, 10);
        if (shouldDouble) {
            tmpNum *= 2;
            if (tmpNum >= 10) sum += tmpNum % 10 + 1;
            else sum += tmpNum;
        } else sum += tmpNum;
        shouldDouble = !shouldDouble;
    }
    return !!(sum % 10 === 0 ? sanitized : false);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("1YYIG", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $171a7ebc053e6a04$var$isIdentityCard;

var $171a7ebc053e6a04$var$_assertString = $171a7ebc053e6a04$var$_interopRequireDefault((parcelRequire("1khYO")));

var $171a7ebc053e6a04$var$_isInt = $171a7ebc053e6a04$var$_interopRequireDefault((parcelRequire("7j5R1")));
function $171a7ebc053e6a04$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $171a7ebc053e6a04$var$validators = {
    PL: function PL(str) {
        (0, $171a7ebc053e6a04$var$_assertString.default)(str);
        var weightOfDigits = {
            1: 1,
            2: 3,
            3: 7,
            4: 9,
            5: 1,
            6: 3,
            7: 7,
            8: 9,
            9: 1,
            10: 3,
            11: 0
        };
        if (str != null && str.length === 11 && (0, $171a7ebc053e6a04$var$_isInt.default)(str, {
            allow_leading_zeroes: true
        })) {
            var digits = str.split("").slice(0, -1);
            var sum = digits.reduce(function(acc, digit, index) {
                return acc + Number(digit) * weightOfDigits[index + 1];
            }, 0);
            var modulo = sum % 10;
            var lastDigit = Number(str.charAt(str.length - 1));
            if (modulo === 0 && lastDigit === 0 || lastDigit === 10 - modulo) return true;
        }
        return false;
    },
    ES: function ES(str) {
        (0, $171a7ebc053e6a04$var$_assertString.default)(str);
        var DNI = /^[0-9X-Z][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/;
        var charsValue = {
            X: 0,
            Y: 1,
            Z: 2
        };
        var controlDigits = [
            "T",
            "R",
            "W",
            "A",
            "G",
            "M",
            "Y",
            "F",
            "P",
            "D",
            "X",
            "B",
            "N",
            "J",
            "Z",
            "S",
            "Q",
            "V",
            "H",
            "L",
            "C",
            "K",
            "E"
        ]; // sanitize user input
        var sanitized = str.trim().toUpperCase(); // validate the data structure
        if (!DNI.test(sanitized)) return false;
         // validate the control digit
        var number = sanitized.slice(0, -1).replace(/[X,Y,Z]/g, function(char) {
            return charsValue[char];
        });
        return sanitized.endsWith(controlDigits[number % 23]);
    },
    FI: function FI(str) {
        // https://dvv.fi/en/personal-identity-code#:~:text=control%20character%20for%20a-,personal,-identity%20code%20calculated
        (0, $171a7ebc053e6a04$var$_assertString.default)(str);
        if (str.length !== 11) return false;
        if (!str.match(/^\d{6}[\-A\+]\d{3}[0-9ABCDEFHJKLMNPRSTUVWXY]{1}$/)) return false;
        var checkDigits = "0123456789ABCDEFHJKLMNPRSTUVWXY";
        var idAsNumber = parseInt(str.slice(0, 6), 10) * 1000 + parseInt(str.slice(7, 10), 10);
        var remainder = idAsNumber % 31;
        var checkDigit = checkDigits[remainder];
        return checkDigit === str.slice(10, 11);
    },
    IN: function IN(str) {
        var DNI = /^[1-9]\d{3}\s?\d{4}\s?\d{4}$/; // multiplication table
        var d = [
            [
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9
            ],
            [
                1,
                2,
                3,
                4,
                0,
                6,
                7,
                8,
                9,
                5
            ],
            [
                2,
                3,
                4,
                0,
                1,
                7,
                8,
                9,
                5,
                6
            ],
            [
                3,
                4,
                0,
                1,
                2,
                8,
                9,
                5,
                6,
                7
            ],
            [
                4,
                0,
                1,
                2,
                3,
                9,
                5,
                6,
                7,
                8
            ],
            [
                5,
                9,
                8,
                7,
                6,
                0,
                4,
                3,
                2,
                1
            ],
            [
                6,
                5,
                9,
                8,
                7,
                1,
                0,
                4,
                3,
                2
            ],
            [
                7,
                6,
                5,
                9,
                8,
                2,
                1,
                0,
                4,
                3
            ],
            [
                8,
                7,
                6,
                5,
                9,
                3,
                2,
                1,
                0,
                4
            ],
            [
                9,
                8,
                7,
                6,
                5,
                4,
                3,
                2,
                1,
                0
            ]
        ]; // permutation table
        var p = [
            [
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9
            ],
            [
                1,
                5,
                7,
                6,
                2,
                8,
                3,
                0,
                9,
                4
            ],
            [
                5,
                8,
                0,
                3,
                7,
                9,
                6,
                1,
                4,
                2
            ],
            [
                8,
                9,
                1,
                6,
                0,
                4,
                3,
                5,
                2,
                7
            ],
            [
                9,
                4,
                5,
                3,
                1,
                2,
                6,
                8,
                7,
                0
            ],
            [
                4,
                2,
                8,
                6,
                5,
                7,
                3,
                9,
                0,
                1
            ],
            [
                2,
                7,
                9,
                3,
                8,
                0,
                6,
                4,
                1,
                5
            ],
            [
                7,
                0,
                4,
                6,
                9,
                1,
                3,
                2,
                5,
                8
            ]
        ]; // sanitize user input
        var sanitized = str.trim(); // validate the data structure
        if (!DNI.test(sanitized)) return false;
        var c = 0;
        var invertedArray = sanitized.replace(/\s/g, "").split("").map(Number).reverse();
        invertedArray.forEach(function(val, i) {
            c = d[c][p[i % 8][val]];
        });
        return c === 0;
    },
    IR: function IR(str) {
        if (!str.match(/^\d{10}$/)) return false;
        str = "0000".concat(str).substr(str.length - 6);
        if (parseInt(str.substr(3, 6), 10) === 0) return false;
        var lastNumber = parseInt(str.substr(9, 1), 10);
        var sum = 0;
        for(var i = 0; i < 9; i++)sum += parseInt(str.substr(i, 1), 10) * (10 - i);
        sum %= 11;
        return sum < 2 && lastNumber === sum || sum >= 2 && lastNumber === 11 - sum;
    },
    IT: function IT(str) {
        if (str.length !== 9) return false;
        if (str === "CA00000AA") return false; // https://it.wikipedia.org/wiki/Carta_d%27identit%C3%A0_elettronica_italiana
        return str.search(/C[A-Z][0-9]{5}[A-Z]{2}/i) > -1;
    },
    NO: function NO(str) {
        var sanitized = str.trim();
        if (isNaN(Number(sanitized))) return false;
        if (sanitized.length !== 11) return false;
        if (sanitized === "00000000000") return false; // https://no.wikipedia.org/wiki/F%C3%B8dselsnummer
        var f = sanitized.split("").map(Number);
        var k1 = (11 - (3 * f[0] + 7 * f[1] + 6 * f[2] + 1 * f[3] + 8 * f[4] + 9 * f[5] + 4 * f[6] + 5 * f[7] + 2 * f[8]) % 11) % 11;
        var k2 = (11 - (5 * f[0] + 4 * f[1] + 3 * f[2] + 2 * f[3] + 7 * f[4] + 6 * f[5] + 5 * f[6] + 4 * f[7] + 3 * f[8] + 2 * k1) % 11) % 11;
        if (k1 !== f[9] || k2 !== f[10]) return false;
        return true;
    },
    TH: function TH(str) {
        if (!str.match(/^[1-8]\d{12}$/)) return false; // validate check digit
        var sum = 0;
        for(var i = 0; i < 12; i++)sum += parseInt(str[i], 10) * (13 - i);
        return str[12] === ((11 - sum % 11) % 10).toString();
    },
    LK: function LK(str) {
        var old_nic = /^[1-9]\d{8}[vx]$/i;
        var new_nic = /^[1-9]\d{11}$/i;
        if (str.length === 10 && old_nic.test(str)) return true;
        else if (str.length === 12 && new_nic.test(str)) return true;
        return false;
    },
    "he-IL": function heIL(str) {
        var DNI = /^\d{9}$/; // sanitize user input
        var sanitized = str.trim(); // validate the data structure
        if (!DNI.test(sanitized)) return false;
        var id = sanitized;
        var sum = 0, incNum;
        for(var i = 0; i < id.length; i++){
            incNum = Number(id[i]) * (i % 2 + 1); // Multiply number by 1 or 2
            sum += incNum > 9 ? incNum - 9 : incNum; // Sum the digits up and add to total
        }
        return sum % 10 === 0;
    },
    "ar-LY": function arLY(str) {
        // Libya National Identity Number NIN is 12 digits, the first digit is either 1 or 2
        var NIN = /^(1|2)\d{11}$/; // sanitize user input
        var sanitized = str.trim(); // validate the data structure
        if (!NIN.test(sanitized)) return false;
        return true;
    },
    "ar-TN": function arTN(str) {
        var DNI = /^\d{8}$/; // sanitize user input
        var sanitized = str.trim(); // validate the data structure
        if (!DNI.test(sanitized)) return false;
        return true;
    },
    "zh-CN": function zhCN(str) {
        var provincesAndCities = [
            "11",
            "12",
            "13",
            "14",
            "15",
            "21",
            "22",
            "23",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "50",
            "51",
            "52",
            "53",
            "54",
            "61",
            "62",
            "63",
            "64",
            "65",
            "71",
            "81",
            "82",
            "91" // 国外
        ];
        var powers = [
            "7",
            "9",
            "10",
            "5",
            "8",
            "4",
            "2",
            "1",
            "6",
            "3",
            "7",
            "9",
            "10",
            "5",
            "8",
            "4",
            "2"
        ];
        var parityBit = [
            "1",
            "0",
            "X",
            "9",
            "8",
            "7",
            "6",
            "5",
            "4",
            "3",
            "2"
        ];
        var checkAddressCode = function checkAddressCode(addressCode) {
            return provincesAndCities.includes(addressCode);
        };
        var checkBirthDayCode = function checkBirthDayCode(birDayCode) {
            var yyyy = parseInt(birDayCode.substring(0, 4), 10);
            var mm = parseInt(birDayCode.substring(4, 6), 10);
            var dd = parseInt(birDayCode.substring(6), 10);
            var xdata = new Date(yyyy, mm - 1, dd);
            if (xdata > new Date()) return false; // eslint-disable-next-line max-len
            else if (xdata.getFullYear() === yyyy && xdata.getMonth() === mm - 1 && xdata.getDate() === dd) return true;
            return false;
        };
        var getParityBit = function getParityBit(idCardNo) {
            var id17 = idCardNo.substring(0, 17);
            var power = 0;
            for(var i = 0; i < 17; i++)power += parseInt(id17.charAt(i), 10) * parseInt(powers[i], 10);
            var mod = power % 11;
            return parityBit[mod];
        };
        var checkParityBit = function checkParityBit(idCardNo) {
            return getParityBit(idCardNo) === idCardNo.charAt(17).toUpperCase();
        };
        var check15IdCardNo = function check15IdCardNo(idCardNo) {
            var check = /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/.test(idCardNo);
            if (!check) return false;
            var addressCode = idCardNo.substring(0, 2);
            check = checkAddressCode(addressCode);
            if (!check) return false;
            var birDayCode = "19".concat(idCardNo.substring(6, 12));
            check = checkBirthDayCode(birDayCode);
            if (!check) return false;
            return true;
        };
        var check18IdCardNo = function check18IdCardNo(idCardNo) {
            var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/.test(idCardNo);
            if (!check) return false;
            var addressCode = idCardNo.substring(0, 2);
            check = checkAddressCode(addressCode);
            if (!check) return false;
            var birDayCode = idCardNo.substring(6, 14);
            check = checkBirthDayCode(birDayCode);
            if (!check) return false;
            return checkParityBit(idCardNo);
        };
        var checkIdCardNo = function checkIdCardNo(idCardNo) {
            var check = /^\d{15}|(\d{17}(\d|x|X))$/.test(idCardNo);
            if (!check) return false;
            if (idCardNo.length === 15) return check15IdCardNo(idCardNo);
            return check18IdCardNo(idCardNo);
        };
        return checkIdCardNo(str);
    },
    "zh-TW": function zhTW(str) {
        var ALPHABET_CODES = {
            A: 10,
            B: 11,
            C: 12,
            D: 13,
            E: 14,
            F: 15,
            G: 16,
            H: 17,
            I: 34,
            J: 18,
            K: 19,
            L: 20,
            M: 21,
            N: 22,
            O: 35,
            P: 23,
            Q: 24,
            R: 25,
            S: 26,
            T: 27,
            U: 28,
            V: 29,
            W: 32,
            X: 30,
            Y: 31,
            Z: 33
        };
        var sanitized = str.trim().toUpperCase();
        if (!/^[A-Z][0-9]{9}$/.test(sanitized)) return false;
        return Array.from(sanitized).reduce(function(sum, number, index) {
            if (index === 0) {
                var code = ALPHABET_CODES[number];
                return code % 10 * 9 + Math.floor(code / 10);
            }
            if (index === 9) return (10 - sum % 10 - Number(number)) % 10 === 0;
            return sum + Number(number) * (9 - index);
        }, 0);
    }
};
function $171a7ebc053e6a04$var$isIdentityCard(str, locale) {
    (0, $171a7ebc053e6a04$var$_assertString.default)(str);
    if (locale in $171a7ebc053e6a04$var$validators) return $171a7ebc053e6a04$var$validators[locale](str);
    else if (locale === "any") {
        for(var key in $171a7ebc053e6a04$var$validators)// https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md#ignoring-code-for-coverage-purposes
        // istanbul ignore else
        if ($171a7ebc053e6a04$var$validators.hasOwnProperty(key)) {
            var validator = $171a7ebc053e6a04$var$validators[key];
            if (validator(str)) return true;
        }
        return false;
    }
    throw new Error("Invalid locale '".concat(locale, "'"));
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7q6Jj", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $56702e89d5b2d143$var$isEAN;

var $56702e89d5b2d143$var$_assertString = $56702e89d5b2d143$var$_interopRequireDefault((parcelRequire("1khYO")));
function $56702e89d5b2d143$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/**
 * The most commonly used EAN standard is
 * the thirteen-digit EAN-13, while the
 * less commonly used 8-digit EAN-8 barcode was
 * introduced for use on small packages.
 * Also EAN/UCC-14 is used for Grouping of individual
 * trade items above unit level(Intermediate, Carton or Pallet).
 * For more info about EAN-14 checkout: https://www.gtin.info/itf-14-barcodes/
 * EAN consists of:
 * GS1 prefix, manufacturer code, product code and check digit
 * Reference: https://en.wikipedia.org/wiki/International_Article_Number
 * Reference: https://www.gtin.info/
 */ /**
 * Define EAN Lenghts; 8 for EAN-8; 13 for EAN-13; 14 for EAN-14
 * and Regular Expression for valid EANs (EAN-8, EAN-13, EAN-14),
 * with exact numberic matching of 8 or 13 or 14 digits [0-9]
 */ var $56702e89d5b2d143$var$LENGTH_EAN_8 = 8;
var $56702e89d5b2d143$var$LENGTH_EAN_14 = 14;
var $56702e89d5b2d143$var$validEanRegex = /^(\d{8}|\d{13}|\d{14})$/;
/**
 * Get position weight given:
 * EAN length and digit index/position
 *
 * @param {number} length
 * @param {number} index
 * @return {number}
 */ function $56702e89d5b2d143$var$getPositionWeightThroughLengthAndIndex(length, index) {
    if (length === $56702e89d5b2d143$var$LENGTH_EAN_8 || length === $56702e89d5b2d143$var$LENGTH_EAN_14) return index % 2 === 0 ? 3 : 1;
    return index % 2 === 0 ? 1 : 3;
}
/**
 * Calculate EAN Check Digit
 * Reference: https://en.wikipedia.org/wiki/International_Article_Number#Calculation_of_checksum_digit
 *
 * @param {string} ean
 * @return {number}
 */ function $56702e89d5b2d143$var$calculateCheckDigit(ean) {
    var checksum = ean.slice(0, -1).split("").map(function(char, index) {
        return Number(char) * $56702e89d5b2d143$var$getPositionWeightThroughLengthAndIndex(ean.length, index);
    }).reduce(function(acc, partialSum) {
        return acc + partialSum;
    }, 0);
    var remainder = 10 - checksum % 10;
    return remainder < 10 ? remainder : 0;
}
/**
 * Check if string is valid EAN:
 * Matches EAN-8/EAN-13/EAN-14 regex
 * Has valid check digit.
 *
 * @param {string} str
 * @return {boolean}
 */ function $56702e89d5b2d143$var$isEAN(str) {
    (0, $56702e89d5b2d143$var$_assertString.default)(str);
    var actualCheckDigit = Number(str.slice(-1));
    return $56702e89d5b2d143$var$validEanRegex.test(str) && actualCheckDigit === $56702e89d5b2d143$var$calculateCheckDigit(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("2CQ0D", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $1e97795a24a8cbcb$var$isISIN;

var $1e97795a24a8cbcb$var$_assertString = $1e97795a24a8cbcb$var$_interopRequireDefault((parcelRequire("1khYO")));
function $1e97795a24a8cbcb$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $1e97795a24a8cbcb$var$isin = /^[A-Z]{2}[0-9A-Z]{9}[0-9]$/; // this link details how the check digit is calculated:
// https://www.isin.org/isin-format/. it is a little bit
// odd in that it works with digits, not numbers. in order
// to make only one pass through the ISIN characters, the
// each alpha character is handled as 2 characters within
// the loop.
function $1e97795a24a8cbcb$var$isISIN(str) {
    (0, $1e97795a24a8cbcb$var$_assertString.default)(str);
    if (!$1e97795a24a8cbcb$var$isin.test(str)) return false;
    var double = true;
    var sum = 0; // convert values
    for(var i = str.length - 2; i >= 0; i--)if (str[i] >= "A" && str[i] <= "Z") {
        var value = str[i].charCodeAt(0) - 55;
        var lo = value % 10;
        var hi = Math.trunc(value / 10); // letters have two digits, so handle the low order
        // and high order digits separately.
        for(var _i = 0, _arr = [
            lo,
            hi
        ]; _i < _arr.length; _i++){
            var digit = _arr[_i];
            if (double) {
                if (digit >= 5) sum += 1 + (digit - 5) * 2;
                else sum += digit * 2;
            } else sum += digit;
            double = !double;
        }
    } else {
        var _digit = str[i].charCodeAt(0) - "0".charCodeAt(0);
        if (double) {
            if (_digit >= 5) sum += 1 + (_digit - 5) * 2;
            else sum += _digit * 2;
        } else sum += _digit;
        double = !double;
    }
    var check = Math.trunc((sum + 9) / 10) * 10 - sum;
    return +str[str.length - 1] === check;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("jwNFV", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $e37789ebb42c6403$var$isISBN;

var $e37789ebb42c6403$var$_assertString = $e37789ebb42c6403$var$_interopRequireDefault((parcelRequire("1khYO")));
function $e37789ebb42c6403$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $e37789ebb42c6403$var$isbn10Maybe = /^(?:[0-9]{9}X|[0-9]{10})$/;
var $e37789ebb42c6403$var$isbn13Maybe = /^(?:[0-9]{13})$/;
var $e37789ebb42c6403$var$factor = [
    1,
    3
];
function $e37789ebb42c6403$var$isISBN(str) {
    var version = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    (0, $e37789ebb42c6403$var$_assertString.default)(str);
    version = String(version);
    if (!version) return $e37789ebb42c6403$var$isISBN(str, 10) || $e37789ebb42c6403$var$isISBN(str, 13);
    var sanitized = str.replace(/[\s-]+/g, "");
    var checksum = 0;
    var i;
    if (version === "10") {
        if (!$e37789ebb42c6403$var$isbn10Maybe.test(sanitized)) return false;
        for(i = 0; i < 9; i++)checksum += (i + 1) * sanitized.charAt(i);
        if (sanitized.charAt(9) === "X") checksum += 100;
        else checksum += 10 * sanitized.charAt(9);
        if (checksum % 11 === 0) return !!sanitized;
    } else if (version === "13") {
        if (!$e37789ebb42c6403$var$isbn13Maybe.test(sanitized)) return false;
        for(i = 0; i < 12; i++)checksum += $e37789ebb42c6403$var$factor[i % 2] * sanitized.charAt(i);
        if (sanitized.charAt(12) - (10 - checksum % 10) % 10 === 0) return !!sanitized;
    }
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("dnaPy", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $9bc5d310fe46a801$var$isISSN;

var $9bc5d310fe46a801$var$_assertString = $9bc5d310fe46a801$var$_interopRequireDefault((parcelRequire("1khYO")));
function $9bc5d310fe46a801$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $9bc5d310fe46a801$var$issn = "^\\d{4}-?\\d{3}[\\dX]$";
function $9bc5d310fe46a801$var$isISSN(str) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, $9bc5d310fe46a801$var$_assertString.default)(str);
    var testIssn = $9bc5d310fe46a801$var$issn;
    testIssn = options.require_hyphen ? testIssn.replace("?", "") : testIssn;
    testIssn = options.case_sensitive ? new RegExp(testIssn) : new RegExp(testIssn, "i");
    if (!testIssn.test(str)) return false;
    var digits = str.replace("-", "").toUpperCase();
    var checksum = 0;
    for(var i = 0; i < digits.length; i++){
        var digit = digits[i];
        checksum += (digit === "X" ? 10 : +digit) * (8 - i);
    }
    return checksum % 11 === 0;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7jB1i", function(module, exports) {
"use strict";
function $5537061e7515c261$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $5537061e7515c261$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $5537061e7515c261$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $5537061e7515c261$var$_typeof(obj);
}
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5537061e7515c261$var$isTaxID;

var $5537061e7515c261$var$_assertString = $5537061e7515c261$var$_interopRequireDefault((parcelRequire("1khYO")));

var $5537061e7515c261$var$algorithms = $5537061e7515c261$var$_interopRequireWildcard((parcelRequire("eQpCf")));

var $5537061e7515c261$var$_isDate = $5537061e7515c261$var$_interopRequireDefault((parcelRequire("2KIcT")));
function $5537061e7515c261$var$_getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();
    $5537061e7515c261$var$_getRequireWildcardCache = function _getRequireWildcardCache() {
        return cache;
    };
    return cache;
}
function $5537061e7515c261$var$_interopRequireWildcard(obj) {
    if (obj && obj.__esModule) return obj;
    if (obj === null || $5537061e7515c261$var$_typeof(obj) !== "object" && typeof obj !== "function") return {
        default: obj
    };
    var cache = $5537061e7515c261$var$_getRequireWildcardCache();
    if (cache && cache.has(obj)) return cache.get(obj);
    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;
    for(var key in obj)if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;
        if (desc && (desc.get || desc.set)) Object.defineProperty(newObj, key, desc);
        else newObj[key] = obj[key];
    }
    newObj.default = obj;
    if (cache) cache.set(obj, newObj);
    return newObj;
}
function $5537061e7515c261$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $5537061e7515c261$var$_toConsumableArray(arr) {
    return $5537061e7515c261$var$_arrayWithoutHoles(arr) || $5537061e7515c261$var$_iterableToArray(arr) || $5537061e7515c261$var$_unsupportedIterableToArray(arr) || $5537061e7515c261$var$_nonIterableSpread();
}
function $5537061e7515c261$var$_nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function $5537061e7515c261$var$_unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return $5537061e7515c261$var$_arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return $5537061e7515c261$var$_arrayLikeToArray(o, minLen);
}
function $5537061e7515c261$var$_iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
}
function $5537061e7515c261$var$_arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return $5537061e7515c261$var$_arrayLikeToArray(arr);
}
function $5537061e7515c261$var$_arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
/**
 * TIN Validation
 * Validates Tax Identification Numbers (TINs) from the US, EU member states and the United Kingdom.
 *
 * EU-UK:
 * National TIN validity is calculated using public algorithms as made available by DG TAXUD.
 *
 * See `https://ec.europa.eu/taxation_customs/tin/specs/FS-TIN%20Algorithms-Public.docx` for more information.
 *
 * US:
 * An Employer Identification Number (EIN), also known as a Federal Tax Identification Number,
 *  is used to identify a business entity.
 *
 * NOTES:
 *  - Prefix 47 is being reserved for future use
 *  - Prefixes 26, 27, 45, 46 and 47 were previously assigned by the Philadelphia campus.
 *
 * See `http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/How-EINs-are-Assigned-and-Valid-EIN-Prefixes`
 * for more information.
 */ // Locale functions
/*
 * bg-BG validation function
 * (Edinen graždanski nomer (EGN/ЕГН), persons only)
 * Checks if birth date (first six digits) is valid and calculates check (last) digit
 */ function $5537061e7515c261$var$bgBgCheck(tin) {
    // Extract full year, normalize month and check birth date validity
    var century_year = tin.slice(0, 2);
    var month = parseInt(tin.slice(2, 4), 10);
    if (month > 40) {
        month -= 40;
        century_year = "20".concat(century_year);
    } else if (month > 20) {
        month -= 20;
        century_year = "18".concat(century_year);
    } else century_year = "19".concat(century_year);
    if (month < 10) month = "0".concat(month);
    var date = "".concat(century_year, "/").concat(month, "/").concat(tin.slice(4, 6));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    }); // Calculate checksum by multiplying digits with fixed values
    var multip_lookup = [
        2,
        4,
        8,
        5,
        10,
        9,
        7,
        3,
        6
    ];
    var checksum = 0;
    for(var i = 0; i < multip_lookup.length; i++)checksum += digits[i] * multip_lookup[i];
    checksum = checksum % 11 === 10 ? 0 : checksum % 11;
    return checksum === digits[9];
}
/*
 * cs-CZ validation function
 * (Rodné číslo (RČ), persons only)
 * Checks if birth date (first six digits) is valid and divisibility by 11
 * Material not in DG TAXUD document sourced from:
 * -`https://lorenc.info/3MA381/overeni-spravnosti-rodneho-cisla.htm`
 * -`https://www.mvcr.cz/clanek/rady-a-sluzby-dokumenty-rodne-cislo.aspx`
 */ function $5537061e7515c261$var$csCzCheck(tin) {
    tin = tin.replace(/\W/, ""); // Extract full year from TIN length
    var full_year = parseInt(tin.slice(0, 2), 10);
    if (tin.length === 10) {
        if (full_year < 54) full_year = "20".concat(full_year);
        else full_year = "19".concat(full_year);
    } else {
        if (tin.slice(6) === "000") return false;
         // Three-zero serial not assigned before 1954
        if (full_year < 54) full_year = "19".concat(full_year);
        else return false; // No 18XX years seen in any of the resources
    } // Add missing zero if needed
    if (full_year.length === 3) full_year = [
        full_year.slice(0, 2),
        "0",
        full_year.slice(2)
    ].join("");
     // Extract month from TIN and normalize
    var month = parseInt(tin.slice(2, 4), 10);
    if (month > 50) month -= 50;
    if (month > 20) {
        // Month-plus-twenty was only introduced in 2004
        if (parseInt(full_year, 10) < 2004) return false;
        month -= 20;
    }
    if (month < 10) month = "0".concat(month);
     // Check date validity
    var date = "".concat(full_year, "/").concat(month, "/").concat(tin.slice(4, 6));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Verify divisibility by 11
    if (tin.length === 10) {
        if (parseInt(tin, 10) % 11 !== 0) {
            // Some numbers up to and including 1985 are still valid if
            // check (last) digit equals 0 and modulo of first 9 digits equals 10
            var checkdigit = parseInt(tin.slice(0, 9), 10) % 11;
            if (parseInt(full_year, 10) < 1986 && checkdigit === 10) {
                if (parseInt(tin.slice(9), 10) !== 0) return false;
            } else return false;
        }
    }
    return true;
}
/*
 * de-AT validation function
 * (Abgabenkontonummer, persons/entities)
 * Verify TIN validity by calling luhnCheck()
 */ function $5537061e7515c261$var$deAtCheck(tin) {
    return $5537061e7515c261$var$algorithms.luhnCheck(tin);
}
/*
 * de-DE validation function
 * (Steueridentifikationsnummer (Steuer-IdNr.), persons only)
 * Tests for single duplicate/triplicate value, then calculates ISO 7064 check (last) digit
 * Partial implementation of spec (same result with both algorithms always)
 */ function $5537061e7515c261$var$deDeCheck(tin) {
    // Split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    }); // Fill array with strings of number positions
    var occurences = [];
    for(var i = 0; i < digits.length - 1; i++){
        occurences.push("");
        for(var j = 0; j < digits.length - 1; j++)if (digits[i] === digits[j]) occurences[i] += j;
    } // Remove digits with one occurence and test for only one duplicate/triplicate
    occurences = occurences.filter(function(a) {
        return a.length > 1;
    });
    if (occurences.length !== 2 && occurences.length !== 3) return false;
     // In case of triplicate value only two digits are allowed next to each other
    if (occurences[0].length === 3) {
        var trip_locations = occurences[0].split("").map(function(a) {
            return parseInt(a, 10);
        });
        var recurrent = 0; // Amount of neighbour occurences
        for(var _i = 0; _i < trip_locations.length - 1; _i++)if (trip_locations[_i] + 1 === trip_locations[_i + 1]) recurrent += 1;
        if (recurrent === 2) return false;
    }
    return $5537061e7515c261$var$algorithms.iso7064Check(tin);
}
/*
 * dk-DK validation function
 * (CPR-nummer (personnummer), persons only)
 * Checks if birth date (first six digits) is valid and assigned to century (seventh) digit,
 * and calculates check (last) digit
 */ function $5537061e7515c261$var$dkDkCheck(tin) {
    tin = tin.replace(/\W/, ""); // Extract year, check if valid for given century digit and add century
    var year = parseInt(tin.slice(4, 6), 10);
    var century_digit = tin.slice(6, 7);
    switch(century_digit){
        case "0":
        case "1":
        case "2":
        case "3":
            year = "19".concat(year);
            break;
        case "4":
        case "9":
            if (year < 37) year = "20".concat(year);
            else year = "19".concat(year);
            break;
        default:
            if (year < 37) year = "20".concat(year);
            else if (year > 58) year = "18".concat(year);
            else return false;
            break;
    } // Add missing zero if needed
    if (year.length === 3) year = [
        year.slice(0, 2),
        "0",
        year.slice(2)
    ].join("");
     // Check date validity
    var date = "".concat(year, "/").concat(tin.slice(2, 4), "/").concat(tin.slice(0, 2));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    });
    var checksum = 0;
    var weight = 4; // Multiply by weight and add to checksum
    for(var i = 0; i < 9; i++){
        checksum += digits[i] * weight;
        weight -= 1;
        if (weight === 1) weight = 7;
    }
    checksum %= 11;
    if (checksum === 1) return false;
    return checksum === 0 ? digits[9] === 0 : digits[9] === 11 - checksum;
}
/*
 * el-CY validation function
 * (Arithmos Forologikou Mitroou (AFM/ΑΦΜ), persons only)
 * Verify TIN validity by calculating ASCII value of check (last) character
 */ function $5537061e7515c261$var$elCyCheck(tin) {
    // split digits into an array for further processing
    var digits = tin.slice(0, 8).split("").map(function(a) {
        return parseInt(a, 10);
    });
    var checksum = 0; // add digits in even places
    for(var i = 1; i < digits.length; i += 2)checksum += digits[i];
     // add digits in odd places
    for(var _i2 = 0; _i2 < digits.length; _i2 += 2)if (digits[_i2] < 2) checksum += 1 - digits[_i2];
    else {
        checksum += 2 * (digits[_i2] - 2) + 5;
        if (digits[_i2] > 4) checksum += 2;
    }
    return String.fromCharCode(checksum % 26 + 65) === tin.charAt(8);
}
/*
 * el-GR validation function
 * (Arithmos Forologikou Mitroou (AFM/ΑΦΜ), persons/entities)
 * Verify TIN validity by calculating check (last) digit
 * Algorithm not in DG TAXUD document- sourced from:
 * - `http://epixeirisi.gr/%CE%9A%CE%A1%CE%99%CE%A3%CE%99%CE%9C%CE%91-%CE%98%CE%95%CE%9C%CE%91%CE%A4%CE%91-%CE%A6%CE%9F%CE%A1%CE%9F%CE%9B%CE%9F%CE%93%CE%99%CE%91%CE%A3-%CE%9A%CE%91%CE%99-%CE%9B%CE%9F%CE%93%CE%99%CE%A3%CE%A4%CE%99%CE%9A%CE%97%CE%A3/23791/%CE%91%CF%81%CE%B9%CE%B8%CE%BC%CF%8C%CF%82-%CE%A6%CE%BF%CF%81%CE%BF%CE%BB%CE%BF%CE%B3%CE%B9%CE%BA%CE%BF%CF%8D-%CE%9C%CE%B7%CF%84%CF%81%CF%8E%CE%BF%CF%85`
 */ function $5537061e7515c261$var$elGrCheck(tin) {
    // split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    });
    var checksum = 0;
    for(var i = 0; i < 8; i++)checksum += digits[i] * Math.pow(2, 8 - i);
    return checksum % 11 % 10 === digits[8];
}
/*
 * en-GB validation function (should go here if needed)
 * (National Insurance Number (NINO) or Unique Taxpayer Reference (UTR),
 * persons/entities respectively)
 */ /*
 * en-IE validation function
 * (Personal Public Service Number (PPS No), persons only)
 * Verify TIN validity by calculating check (second to last) character
 */ function $5537061e7515c261$var$enIeCheck(tin) {
    var checksum = $5537061e7515c261$var$algorithms.reverseMultiplyAndSum(tin.split("").slice(0, 7).map(function(a) {
        return parseInt(a, 10);
    }), 8);
    if (tin.length === 9 && tin[8] !== "W") checksum += (tin[8].charCodeAt(0) - 64) * 9;
    checksum %= 23;
    if (checksum === 0) return tin[7].toUpperCase() === "W";
    return tin[7].toUpperCase() === String.fromCharCode(64 + checksum);
} // Valid US IRS campus prefixes
var $5537061e7515c261$var$enUsCampusPrefix = {
    andover: [
        "10",
        "12"
    ],
    atlanta: [
        "60",
        "67"
    ],
    austin: [
        "50",
        "53"
    ],
    brookhaven: [
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "11",
        "13",
        "14",
        "16",
        "21",
        "22",
        "23",
        "25",
        "34",
        "51",
        "52",
        "54",
        "55",
        "56",
        "57",
        "58",
        "59",
        "65"
    ],
    cincinnati: [
        "30",
        "32",
        "35",
        "36",
        "37",
        "38",
        "61"
    ],
    fresno: [
        "15",
        "24"
    ],
    internet: [
        "20",
        "26",
        "27",
        "45",
        "46",
        "47"
    ],
    kansas: [
        "40",
        "44"
    ],
    memphis: [
        "94",
        "95"
    ],
    ogden: [
        "80",
        "90"
    ],
    philadelphia: [
        "33",
        "39",
        "41",
        "42",
        "43",
        "46",
        "48",
        "62",
        "63",
        "64",
        "66",
        "68",
        "71",
        "72",
        "73",
        "74",
        "75",
        "76",
        "77",
        "81",
        "82",
        "83",
        "84",
        "85",
        "86",
        "87",
        "88",
        "91",
        "92",
        "93",
        "98",
        "99"
    ],
    sba: [
        "31"
    ]
}; // Return an array of all US IRS campus prefixes
function $5537061e7515c261$var$enUsGetPrefixes() {
    var prefixes = [];
    for(var location in $5537061e7515c261$var$enUsCampusPrefix)// https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md#ignoring-code-for-coverage-purposes
    // istanbul ignore else
    if ($5537061e7515c261$var$enUsCampusPrefix.hasOwnProperty(location)) prefixes.push.apply(prefixes, $5537061e7515c261$var$_toConsumableArray($5537061e7515c261$var$enUsCampusPrefix[location]));
    return prefixes;
}
/*
 * en-US validation function
 * Verify that the TIN starts with a valid IRS campus prefix
 */ function $5537061e7515c261$var$enUsCheck(tin) {
    return $5537061e7515c261$var$enUsGetPrefixes().indexOf(tin.substr(0, 2)) !== -1;
}
/*
 * es-ES validation function
 * (Documento Nacional de Identidad (DNI)
 * or Número de Identificación de Extranjero (NIE), persons only)
 * Verify TIN validity by calculating check (last) character
 */ function $5537061e7515c261$var$esEsCheck(tin) {
    // Split characters into an array for further processing
    var chars = tin.toUpperCase().split(""); // Replace initial letter if needed
    if (isNaN(parseInt(chars[0], 10)) && chars.length > 1) {
        var lead_replace = 0;
        switch(chars[0]){
            case "Y":
                lead_replace = 1;
                break;
            case "Z":
                lead_replace = 2;
                break;
            default:
        }
        chars.splice(0, 1, lead_replace); // Fill with zeros if smaller than proper
    } else while(chars.length < 9)chars.unshift(0);
     // Calculate checksum and check according to lookup
    var lookup = [
        "T",
        "R",
        "W",
        "A",
        "G",
        "M",
        "Y",
        "F",
        "P",
        "D",
        "X",
        "B",
        "N",
        "J",
        "Z",
        "S",
        "Q",
        "V",
        "H",
        "L",
        "C",
        "K",
        "E"
    ];
    chars = chars.join("");
    var checksum = parseInt(chars.slice(0, 8), 10) % 23;
    return chars[8] === lookup[checksum];
}
/*
 * et-EE validation function
 * (Isikukood (IK), persons only)
 * Checks if birth date (century digit and six following) is valid and calculates check (last) digit
 * Material not in DG TAXUD document sourced from:
 * - `https://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-identification-numbers/Estonia-TIN.pdf`
 */ function $5537061e7515c261$var$etEeCheck(tin) {
    // Extract year and add century
    var full_year = tin.slice(1, 3);
    var century_digit = tin.slice(0, 1);
    switch(century_digit){
        case "1":
        case "2":
            full_year = "18".concat(full_year);
            break;
        case "3":
        case "4":
            full_year = "19".concat(full_year);
            break;
        default:
            full_year = "20".concat(full_year);
            break;
    } // Check date validity
    var date = "".concat(full_year, "/").concat(tin.slice(3, 5), "/").concat(tin.slice(5, 7));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    });
    var checksum = 0;
    var weight = 1; // Multiply by weight and add to checksum
    for(var i = 0; i < 10; i++){
        checksum += digits[i] * weight;
        weight += 1;
        if (weight === 10) weight = 1;
    } // Do again if modulo 11 of checksum is 10
    if (checksum % 11 === 10) {
        checksum = 0;
        weight = 3;
        for(var _i3 = 0; _i3 < 10; _i3++){
            checksum += digits[_i3] * weight;
            weight += 1;
            if (weight === 10) weight = 1;
        }
        if (checksum % 11 === 10) return digits[10] === 0;
    }
    return checksum % 11 === digits[10];
}
/*
 * fi-FI validation function
 * (Henkilötunnus (HETU), persons only)
 * Checks if birth date (first six digits plus century symbol) is valid
 * and calculates check (last) digit
 */ function $5537061e7515c261$var$fiFiCheck(tin) {
    // Extract year and add century
    var full_year = tin.slice(4, 6);
    var century_symbol = tin.slice(6, 7);
    switch(century_symbol){
        case "+":
            full_year = "18".concat(full_year);
            break;
        case "-":
            full_year = "19".concat(full_year);
            break;
        default:
            full_year = "20".concat(full_year);
            break;
    } // Check date validity
    var date = "".concat(full_year, "/").concat(tin.slice(2, 4), "/").concat(tin.slice(0, 2));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Calculate check character
    var checksum = parseInt(tin.slice(0, 6) + tin.slice(7, 10), 10) % 31;
    if (checksum < 10) return checksum === parseInt(tin.slice(10), 10);
    checksum -= 10;
    var letters_lookup = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "H",
        "J",
        "K",
        "L",
        "M",
        "N",
        "P",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y"
    ];
    return letters_lookup[checksum] === tin.slice(10);
}
/*
 * fr/nl-BE validation function
 * (Numéro national (N.N.), persons only)
 * Checks if birth date (first six digits) is valid and calculates check (last two) digits
 */ function $5537061e7515c261$var$frBeCheck(tin) {
    // Zero month/day value is acceptable
    if (tin.slice(2, 4) !== "00" || tin.slice(4, 6) !== "00") {
        // Extract date from first six digits of TIN
        var date = "".concat(tin.slice(0, 2), "/").concat(tin.slice(2, 4), "/").concat(tin.slice(4, 6));
        if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YY/MM/DD")) return false;
    }
    var checksum = 97 - parseInt(tin.slice(0, 9), 10) % 97;
    var checkdigits = parseInt(tin.slice(9, 11), 10);
    if (checksum !== checkdigits) {
        checksum = 97 - parseInt("2".concat(tin.slice(0, 9)), 10) % 97;
        if (checksum !== checkdigits) return false;
    }
    return true;
}
/*
 * fr-FR validation function
 * (Numéro fiscal de référence (numéro SPI), persons only)
 * Verify TIN validity by calculating check (last three) digits
 */ function $5537061e7515c261$var$frFrCheck(tin) {
    tin = tin.replace(/\s/g, "");
    var checksum = parseInt(tin.slice(0, 10), 10) % 511;
    var checkdigits = parseInt(tin.slice(10, 13), 10);
    return checksum === checkdigits;
}
/*
 * fr/lb-LU validation function
 * (numéro d’identification personnelle, persons only)
 * Verify birth date validity and run Luhn and Verhoeff checks
 */ function $5537061e7515c261$var$frLuCheck(tin) {
    // Extract date and check validity
    var date = "".concat(tin.slice(0, 4), "/").concat(tin.slice(4, 6), "/").concat(tin.slice(6, 8));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Run Luhn check
    if (!$5537061e7515c261$var$algorithms.luhnCheck(tin.slice(0, 12))) return false;
     // Remove Luhn check digit and run Verhoeff check
    return $5537061e7515c261$var$algorithms.verhoeffCheck("".concat(tin.slice(0, 11)).concat(tin[12]));
}
/*
 * hr-HR validation function
 * (Osobni identifikacijski broj (OIB), persons/entities)
 * Verify TIN validity by calling iso7064Check(digits)
 */ function $5537061e7515c261$var$hrHrCheck(tin) {
    return $5537061e7515c261$var$algorithms.iso7064Check(tin);
}
/*
 * hu-HU validation function
 * (Adóazonosító jel, persons only)
 * Verify TIN validity by calculating check (last) digit
 */ function $5537061e7515c261$var$huHuCheck(tin) {
    // split digits into an array for further processing
    var digits = tin.split("").map(function(a) {
        return parseInt(a, 10);
    });
    var checksum = 8;
    for(var i = 1; i < 9; i++)checksum += digits[i] * (i + 1);
    return checksum % 11 === digits[9];
}
/*
 * lt-LT validation function (should go here if needed)
 * (Asmens kodas, persons/entities respectively)
 * Current validation check is alias of etEeCheck- same format applies
 */ /*
 * it-IT first/last name validity check
 * Accepts it-IT TIN-encoded names as a three-element character array and checks their validity
 * Due to lack of clarity between resources ("Are only Italian consonants used?
 * What happens if a person has X in their name?" etc.) only two test conditions
 * have been implemented:
 * Vowels may only be followed by other vowels or an X character
 * and X characters after vowels may only be followed by other X characters.
 */ function $5537061e7515c261$var$itItNameCheck(name) {
    // true at the first occurence of a vowel
    var vowelflag = false; // true at the first occurence of an X AFTER vowel
    // (to properly handle last names with X as consonant)
    var xflag = false;
    for(var i = 0; i < 3; i++){
        if (!vowelflag && /[AEIOU]/.test(name[i])) vowelflag = true;
        else if (!xflag && vowelflag && name[i] === "X") xflag = true;
        else if (i > 0) {
            if (vowelflag && !xflag) {
                if (!/[AEIOU]/.test(name[i])) return false;
            }
            if (xflag) {
                if (!/X/.test(name[i])) return false;
            }
        }
    }
    return true;
}
/*
 * it-IT validation function
 * (Codice fiscale (TIN-IT), persons only)
 * Verify name, birth date and codice catastale validity
 * and calculate check character.
 * Material not in DG-TAXUD document sourced from:
 * `https://en.wikipedia.org/wiki/Italian_fiscal_code`
 */ function $5537061e7515c261$var$itItCheck(tin) {
    // Capitalize and split characters into an array for further processing
    var chars = tin.toUpperCase().split(""); // Check first and last name validity calling itItNameCheck()
    if (!$5537061e7515c261$var$itItNameCheck(chars.slice(0, 3))) return false;
    if (!$5537061e7515c261$var$itItNameCheck(chars.slice(3, 6))) return false;
     // Convert letters in number spaces back to numbers if any
    var number_locations = [
        6,
        7,
        9,
        10,
        12,
        13,
        14
    ];
    var number_replace = {
        L: "0",
        M: "1",
        N: "2",
        P: "3",
        Q: "4",
        R: "5",
        S: "6",
        T: "7",
        U: "8",
        V: "9"
    };
    for(var _i4 = 0, _number_locations = number_locations; _i4 < _number_locations.length; _i4++){
        var i = _number_locations[_i4];
        if (chars[i] in number_replace) chars.splice(i, 1, number_replace[chars[i]]);
    } // Extract month and day, and check date validity
    var month_replace = {
        A: "01",
        B: "02",
        C: "03",
        D: "04",
        E: "05",
        H: "06",
        L: "07",
        M: "08",
        P: "09",
        R: "10",
        S: "11",
        T: "12"
    };
    var month = month_replace[chars[8]];
    var day = parseInt(chars[9] + chars[10], 10);
    if (day > 40) day -= 40;
    if (day < 10) day = "0".concat(day);
    var date = "".concat(chars[6]).concat(chars[7], "/").concat(month, "/").concat(day);
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YY/MM/DD")) return false;
     // Calculate check character by adding up even and odd characters as numbers
    var checksum = 0;
    for(var _i5 = 1; _i5 < chars.length - 1; _i5 += 2){
        var char_to_int = parseInt(chars[_i5], 10);
        if (isNaN(char_to_int)) char_to_int = chars[_i5].charCodeAt(0) - 65;
        checksum += char_to_int;
    }
    var odd_convert = {
        // Maps of characters at odd places
        A: 1,
        B: 0,
        C: 5,
        D: 7,
        E: 9,
        F: 13,
        G: 15,
        H: 17,
        I: 19,
        J: 21,
        K: 2,
        L: 4,
        M: 18,
        N: 20,
        O: 11,
        P: 3,
        Q: 6,
        R: 8,
        S: 12,
        T: 14,
        U: 16,
        V: 10,
        W: 22,
        X: 25,
        Y: 24,
        Z: 23,
        0: 1,
        1: 0
    };
    for(var _i6 = 0; _i6 < chars.length - 1; _i6 += 2){
        var _char_to_int = 0;
        if (chars[_i6] in odd_convert) _char_to_int = odd_convert[chars[_i6]];
        else {
            var multiplier = parseInt(chars[_i6], 10);
            _char_to_int = 2 * multiplier + 1;
            if (multiplier > 4) _char_to_int += 2;
        }
        checksum += _char_to_int;
    }
    if (String.fromCharCode(65 + checksum % 26) !== chars[15]) return false;
    return true;
}
/*
 * lv-LV validation function
 * (Personas kods (PK), persons only)
 * Check validity of birth date and calculate check (last) digit
 * Support only for old format numbers (not starting with '32', issued before 2017/07/01)
 * Material not in DG TAXUD document sourced from:
 * `https://boot.ritakafija.lv/forums/index.php?/topic/88314-personas-koda-algoritms-%C4%8Deksumma/`
 */ function $5537061e7515c261$var$lvLvCheck(tin) {
    tin = tin.replace(/\W/, ""); // Extract date from TIN
    var day = tin.slice(0, 2);
    if (day !== "32") {
        // No date/checksum check if new format
        var month = tin.slice(2, 4);
        if (month !== "00") {
            // No date check if unknown month
            var full_year = tin.slice(4, 6);
            switch(tin[6]){
                case "0":
                    full_year = "18".concat(full_year);
                    break;
                case "1":
                    full_year = "19".concat(full_year);
                    break;
                default:
                    full_year = "20".concat(full_year);
                    break;
            } // Check date validity
            var date = "".concat(full_year, "/").concat(tin.slice(2, 4), "/").concat(day);
            if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
        } // Calculate check digit
        var checksum = 1101;
        var multip_lookup = [
            1,
            6,
            3,
            7,
            9,
            10,
            5,
            8,
            4,
            2
        ];
        for(var i = 0; i < tin.length - 1; i++)checksum -= parseInt(tin[i], 10) * multip_lookup[i];
        return parseInt(tin[10], 10) === checksum % 11;
    }
    return true;
}
/*
 * mt-MT validation function
 * (Identity Card Number or Unique Taxpayer Reference, persons/entities)
 * Verify Identity Card Number structure (no other tests found)
 */ function $5537061e7515c261$var$mtMtCheck(tin) {
    if (tin.length !== 9) {
        // No tests for UTR
        var chars = tin.toUpperCase().split(""); // Fill with zeros if smaller than proper
        while(chars.length < 8)chars.unshift(0);
         // Validate format according to last character
        switch(tin[7]){
            case "A":
            case "P":
                if (parseInt(chars[6], 10) === 0) return false;
                break;
            default:
                var first_part = parseInt(chars.join("").slice(0, 5), 10);
                if (first_part > 32000) return false;
                var second_part = parseInt(chars.join("").slice(5, 7), 10);
                if (first_part === second_part) return false;
        }
    }
    return true;
}
/*
 * nl-NL validation function
 * (Burgerservicenummer (BSN) or Rechtspersonen Samenwerkingsverbanden Informatie Nummer (RSIN),
 * persons/entities respectively)
 * Verify TIN validity by calculating check (last) digit (variant of MOD 11)
 */ function $5537061e7515c261$var$nlNlCheck(tin) {
    return $5537061e7515c261$var$algorithms.reverseMultiplyAndSum(tin.split("").slice(0, 8).map(function(a) {
        return parseInt(a, 10);
    }), 9) % 11 === parseInt(tin[8], 10);
}
/*
 * pl-PL validation function
 * (Powszechny Elektroniczny System Ewidencji Ludności (PESEL)
 * or Numer identyfikacji podatkowej (NIP), persons/entities)
 * Verify TIN validity by validating birth date (PESEL) and calculating check (last) digit
 */ function $5537061e7515c261$var$plPlCheck(tin) {
    // NIP
    if (tin.length === 10) {
        // Calculate last digit by multiplying with lookup
        var lookup = [
            6,
            5,
            7,
            2,
            3,
            4,
            5,
            6,
            7
        ];
        var _checksum = 0;
        for(var i = 0; i < lookup.length; i++)_checksum += parseInt(tin[i], 10) * lookup[i];
        _checksum %= 11;
        if (_checksum === 10) return false;
        return _checksum === parseInt(tin[9], 10);
    } // PESEL
    // Extract full year using month
    var full_year = tin.slice(0, 2);
    var month = parseInt(tin.slice(2, 4), 10);
    if (month > 80) {
        full_year = "18".concat(full_year);
        month -= 80;
    } else if (month > 60) {
        full_year = "22".concat(full_year);
        month -= 60;
    } else if (month > 40) {
        full_year = "21".concat(full_year);
        month -= 40;
    } else if (month > 20) {
        full_year = "20".concat(full_year);
        month -= 20;
    } else full_year = "19".concat(full_year);
     // Add leading zero to month if needed
    if (month < 10) month = "0".concat(month);
     // Check date validity
    var date = "".concat(full_year, "/").concat(month, "/").concat(tin.slice(4, 6));
    if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
     // Calculate last digit by mulitplying with odd one-digit numbers except 5
    var checksum = 0;
    var multiplier = 1;
    for(var _i7 = 0; _i7 < tin.length - 1; _i7++){
        checksum += parseInt(tin[_i7], 10) * multiplier % 10;
        multiplier += 2;
        if (multiplier > 10) multiplier = 1;
        else if (multiplier === 5) multiplier += 2;
    }
    checksum = 10 - checksum % 10;
    return checksum === parseInt(tin[10], 10);
}
/*
* pt-BR validation function
* (Cadastro de Pessoas Físicas (CPF, persons)
* Cadastro Nacional de Pessoas Jurídicas (CNPJ, entities)
* Both inputs will be validated
*/ function $5537061e7515c261$var$ptBrCheck(tin) {
    if (tin.length === 11) {
        var _sum;
        var remainder;
        _sum = 0;
        if (tin === "11111111111" || tin === "22222222222" || tin === "33333333333" || tin === "44444444444" || tin === "55555555555" || tin === "66666666666" || tin === "77777777777" || tin === "88888888888" || tin === "99999999999" || tin === "00000000000") return false;
        for(var i = 1; i <= 9; i++)_sum += parseInt(tin.substring(i - 1, i), 10) * (11 - i);
        remainder = _sum * 10 % 11;
        if (remainder === 10) remainder = 0;
        if (remainder !== parseInt(tin.substring(9, 10), 10)) return false;
        _sum = 0;
        for(var _i8 = 1; _i8 <= 10; _i8++)_sum += parseInt(tin.substring(_i8 - 1, _i8), 10) * (12 - _i8);
        remainder = _sum * 10 % 11;
        if (remainder === 10) remainder = 0;
        if (remainder !== parseInt(tin.substring(10, 11), 10)) return false;
        return true;
    }
    if (tin === "00000000000000" || tin === "11111111111111" || tin === "22222222222222" || tin === "33333333333333" || tin === "44444444444444" || tin === "55555555555555" || tin === "66666666666666" || tin === "77777777777777" || tin === "88888888888888" || tin === "99999999999999") return false;
    var length = tin.length - 2;
    var identifiers = tin.substring(0, length);
    var verificators = tin.substring(length);
    var sum = 0;
    var pos = length - 7;
    for(var _i9 = length; _i9 >= 1; _i9--){
        sum += identifiers.charAt(length - _i9) * pos;
        pos -= 1;
        if (pos < 2) pos = 9;
    }
    var result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== parseInt(verificators.charAt(0), 10)) return false;
    length += 1;
    identifiers = tin.substring(0, length);
    sum = 0;
    pos = length - 7;
    for(var _i10 = length; _i10 >= 1; _i10--){
        sum += identifiers.charAt(length - _i10) * pos;
        pos -= 1;
        if (pos < 2) pos = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== parseInt(verificators.charAt(1), 10)) return false;
    return true;
}
/*
 * pt-PT validation function
 * (Número de identificação fiscal (NIF), persons/entities)
 * Verify TIN validity by calculating check (last) digit (variant of MOD 11)
 */ function $5537061e7515c261$var$ptPtCheck(tin) {
    var checksum = 11 - $5537061e7515c261$var$algorithms.reverseMultiplyAndSum(tin.split("").slice(0, 8).map(function(a) {
        return parseInt(a, 10);
    }), 9) % 11;
    if (checksum > 9) return parseInt(tin[8], 10) === 0;
    return checksum === parseInt(tin[8], 10);
}
/*
 * ro-RO validation function
 * (Cod Numeric Personal (CNP) or Cod de înregistrare fiscală (CIF),
 * persons only)
 * Verify CNP validity by calculating check (last) digit (test not found for CIF)
 * Material not in DG TAXUD document sourced from:
 * `https://en.wikipedia.org/wiki/National_identification_number#Romania`
 */ function $5537061e7515c261$var$roRoCheck(tin) {
    if (tin.slice(0, 4) !== "9000") {
        // No test found for this format
        // Extract full year using century digit if possible
        var full_year = tin.slice(1, 3);
        switch(tin[0]){
            case "1":
            case "2":
                full_year = "19".concat(full_year);
                break;
            case "3":
            case "4":
                full_year = "18".concat(full_year);
                break;
            case "5":
            case "6":
                full_year = "20".concat(full_year);
                break;
            default:
        } // Check date validity
        var date = "".concat(full_year, "/").concat(tin.slice(3, 5), "/").concat(tin.slice(5, 7));
        if (date.length === 8) {
            if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YY/MM/DD")) return false;
        } else if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
         // Calculate check digit
        var digits = tin.split("").map(function(a) {
            return parseInt(a, 10);
        });
        var multipliers = [
            2,
            7,
            9,
            1,
            4,
            6,
            3,
            5,
            8,
            2,
            7,
            9
        ];
        var checksum = 0;
        for(var i = 0; i < multipliers.length; i++)checksum += digits[i] * multipliers[i];
        if (checksum % 11 === 10) return digits[12] === 1;
        return digits[12] === checksum % 11;
    }
    return true;
}
/*
 * sk-SK validation function
 * (Rodné číslo (RČ) or bezvýznamové identifikačné číslo (BIČ), persons only)
 * Checks validity of pre-1954 birth numbers (rodné číslo) only
 * Due to the introduction of the pseudo-random BIČ it is not possible to test
 * post-1954 birth numbers without knowing whether they are BIČ or RČ beforehand
 */ function $5537061e7515c261$var$skSkCheck(tin) {
    if (tin.length === 9) {
        tin = tin.replace(/\W/, "");
        if (tin.slice(6) === "000") return false;
         // Three-zero serial not assigned before 1954
        // Extract full year from TIN length
        var full_year = parseInt(tin.slice(0, 2), 10);
        if (full_year > 53) return false;
        if (full_year < 10) full_year = "190".concat(full_year);
        else full_year = "19".concat(full_year);
         // Extract month from TIN and normalize
        var month = parseInt(tin.slice(2, 4), 10);
        if (month > 50) month -= 50;
        if (month < 10) month = "0".concat(month);
         // Check date validity
        var date = "".concat(full_year, "/").concat(month, "/").concat(tin.slice(4, 6));
        if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
    }
    return true;
}
/*
 * sl-SI validation function
 * (Davčna številka, persons/entities)
 * Verify TIN validity by calculating check (last) digit (variant of MOD 11)
 */ function $5537061e7515c261$var$slSiCheck(tin) {
    var checksum = 11 - $5537061e7515c261$var$algorithms.reverseMultiplyAndSum(tin.split("").slice(0, 7).map(function(a) {
        return parseInt(a, 10);
    }), 8) % 11;
    if (checksum === 10) return parseInt(tin[7], 10) === 0;
    return checksum === parseInt(tin[7], 10);
}
/*
 * sv-SE validation function
 * (Personnummer or samordningsnummer, persons only)
 * Checks validity of birth date and calls luhnCheck() to validate check (last) digit
 */ function $5537061e7515c261$var$svSeCheck(tin) {
    // Make copy of TIN and normalize to two-digit year form
    var tin_copy = tin.slice(0);
    if (tin.length > 11) tin_copy = tin_copy.slice(2);
     // Extract date of birth
    var full_year = "";
    var month = tin_copy.slice(2, 4);
    var day = parseInt(tin_copy.slice(4, 6), 10);
    if (tin.length > 11) full_year = tin.slice(0, 4);
    else {
        full_year = tin.slice(0, 2);
        if (tin.length === 11 && day < 60) {
            // Extract full year from centenarian symbol
            // Should work just fine until year 10000 or so
            var current_year = new Date().getFullYear().toString();
            var current_century = parseInt(current_year.slice(0, 2), 10);
            current_year = parseInt(current_year, 10);
            if (tin[6] === "-") {
                if (parseInt("".concat(current_century).concat(full_year), 10) > current_year) full_year = "".concat(current_century - 1).concat(full_year);
                else full_year = "".concat(current_century).concat(full_year);
            } else {
                full_year = "".concat(current_century - 1).concat(full_year);
                if (current_year - parseInt(full_year, 10) < 100) return false;
            }
        }
    } // Normalize day and check date validity
    if (day > 60) day -= 60;
    if (day < 10) day = "0".concat(day);
    var date = "".concat(full_year, "/").concat(month, "/").concat(day);
    if (date.length === 8) {
        if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YY/MM/DD")) return false;
    } else if (!(0, $5537061e7515c261$var$_isDate.default)(date, "YYYY/MM/DD")) return false;
    return $5537061e7515c261$var$algorithms.luhnCheck(tin.replace(/\W/, ""));
} // Locale lookup objects
/*
 * Tax id regex formats for various locales
 *
 * Where not explicitly specified in DG-TAXUD document both
 * uppercase and lowercase letters are acceptable.
 */ var $5537061e7515c261$var$taxIdFormat = {
    "bg-BG": /^\d{10}$/,
    "cs-CZ": /^\d{6}\/{0,1}\d{3,4}$/,
    "de-AT": /^\d{9}$/,
    "de-DE": /^[1-9]\d{10}$/,
    "dk-DK": /^\d{6}-{0,1}\d{4}$/,
    "el-CY": /^[09]\d{7}[A-Z]$/,
    "el-GR": /^([0-4]|[7-9])\d{8}$/,
    "en-GB": /^\d{10}$|^(?!GB|NK|TN|ZZ)(?![DFIQUV])[A-Z](?![DFIQUVO])[A-Z]\d{6}[ABCD ]$/i,
    "en-IE": /^\d{7}[A-W][A-IW]{0,1}$/i,
    "en-US": /^\d{2}[- ]{0,1}\d{7}$/,
    "es-ES": /^(\d{0,8}|[XYZKLM]\d{7})[A-HJ-NP-TV-Z]$/i,
    "et-EE": /^[1-6]\d{6}(00[1-9]|0[1-9][0-9]|[1-6][0-9]{2}|70[0-9]|710)\d$/,
    "fi-FI": /^\d{6}[-+A]\d{3}[0-9A-FHJ-NPR-Y]$/i,
    "fr-BE": /^\d{11}$/,
    "fr-FR": /^[0-3]\d{12}$|^[0-3]\d\s\d{2}(\s\d{3}){3}$/,
    // Conforms both to official spec and provided example
    "fr-LU": /^\d{13}$/,
    "hr-HR": /^\d{11}$/,
    "hu-HU": /^8\d{9}$/,
    "it-IT": /^[A-Z]{6}[L-NP-V0-9]{2}[A-EHLMPRST][L-NP-V0-9]{2}[A-ILMZ][L-NP-V0-9]{3}[A-Z]$/i,
    "lv-LV": /^\d{6}-{0,1}\d{5}$/,
    // Conforms both to DG TAXUD spec and original research
    "mt-MT": /^\d{3,7}[APMGLHBZ]$|^([1-8])\1\d{7}$/i,
    "nl-NL": /^\d{9}$/,
    "pl-PL": /^\d{10,11}$/,
    "pt-BR": /(?:^\d{11}$)|(?:^\d{14}$)/,
    "pt-PT": /^\d{9}$/,
    "ro-RO": /^\d{13}$/,
    "sk-SK": /^\d{6}\/{0,1}\d{3,4}$/,
    "sl-SI": /^[1-9]\d{7}$/,
    "sv-SE": /^(\d{6}[-+]{0,1}\d{4}|(18|19|20)\d{6}[-+]{0,1}\d{4})$/
}; // taxIdFormat locale aliases
$5537061e7515c261$var$taxIdFormat["lb-LU"] = $5537061e7515c261$var$taxIdFormat["fr-LU"];
$5537061e7515c261$var$taxIdFormat["lt-LT"] = $5537061e7515c261$var$taxIdFormat["et-EE"];
$5537061e7515c261$var$taxIdFormat["nl-BE"] = $5537061e7515c261$var$taxIdFormat["fr-BE"]; // Algorithmic tax id check functions for various locales
var $5537061e7515c261$var$taxIdCheck = {
    "bg-BG": $5537061e7515c261$var$bgBgCheck,
    "cs-CZ": $5537061e7515c261$var$csCzCheck,
    "de-AT": $5537061e7515c261$var$deAtCheck,
    "de-DE": $5537061e7515c261$var$deDeCheck,
    "dk-DK": $5537061e7515c261$var$dkDkCheck,
    "el-CY": $5537061e7515c261$var$elCyCheck,
    "el-GR": $5537061e7515c261$var$elGrCheck,
    "en-IE": $5537061e7515c261$var$enIeCheck,
    "en-US": $5537061e7515c261$var$enUsCheck,
    "es-ES": $5537061e7515c261$var$esEsCheck,
    "et-EE": $5537061e7515c261$var$etEeCheck,
    "fi-FI": $5537061e7515c261$var$fiFiCheck,
    "fr-BE": $5537061e7515c261$var$frBeCheck,
    "fr-FR": $5537061e7515c261$var$frFrCheck,
    "fr-LU": $5537061e7515c261$var$frLuCheck,
    "hr-HR": $5537061e7515c261$var$hrHrCheck,
    "hu-HU": $5537061e7515c261$var$huHuCheck,
    "it-IT": $5537061e7515c261$var$itItCheck,
    "lv-LV": $5537061e7515c261$var$lvLvCheck,
    "mt-MT": $5537061e7515c261$var$mtMtCheck,
    "nl-NL": $5537061e7515c261$var$nlNlCheck,
    "pl-PL": $5537061e7515c261$var$plPlCheck,
    "pt-BR": $5537061e7515c261$var$ptBrCheck,
    "pt-PT": $5537061e7515c261$var$ptPtCheck,
    "ro-RO": $5537061e7515c261$var$roRoCheck,
    "sk-SK": $5537061e7515c261$var$skSkCheck,
    "sl-SI": $5537061e7515c261$var$slSiCheck,
    "sv-SE": $5537061e7515c261$var$svSeCheck
}; // taxIdCheck locale aliases
$5537061e7515c261$var$taxIdCheck["lb-LU"] = $5537061e7515c261$var$taxIdCheck["fr-LU"];
$5537061e7515c261$var$taxIdCheck["lt-LT"] = $5537061e7515c261$var$taxIdCheck["et-EE"];
$5537061e7515c261$var$taxIdCheck["nl-BE"] = $5537061e7515c261$var$taxIdCheck["fr-BE"]; // Regexes for locales where characters should be omitted before checking format
var $5537061e7515c261$var$allsymbols = /[-\\\/!@#$%\^&\*\(\)\+\=\[\]]+/g;
var $5537061e7515c261$var$sanitizeRegexes = {
    "de-AT": $5537061e7515c261$var$allsymbols,
    "de-DE": /[\/\\]/g,
    "fr-BE": $5537061e7515c261$var$allsymbols
}; // sanitizeRegexes locale aliases
$5537061e7515c261$var$sanitizeRegexes["nl-BE"] = $5537061e7515c261$var$sanitizeRegexes["fr-BE"];
/*
 * Validator function
 * Return true if the passed string is a valid tax identification number
 * for the specified locale.
 * Throw an error exception if the locale is not supported.
 */ function $5537061e7515c261$var$isTaxID(str) {
    var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "en-US";
    (0, $5537061e7515c261$var$_assertString.default)(str); // Copy TIN to avoid replacement if sanitized
    var strcopy = str.slice(0);
    if (locale in $5537061e7515c261$var$taxIdFormat) {
        if (locale in $5537061e7515c261$var$sanitizeRegexes) strcopy = strcopy.replace($5537061e7515c261$var$sanitizeRegexes[locale], "");
        if (!$5537061e7515c261$var$taxIdFormat[locale].test(strcopy)) return false;
        if (locale in $5537061e7515c261$var$taxIdCheck) return $5537061e7515c261$var$taxIdCheck[locale](strcopy);
         // Fallthrough; not all locales have algorithmic checks
        return true;
    }
    throw new Error("Invalid locale '".concat(locale, "'"));
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("eQpCf", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.iso7064Check = $ace9caca45ced9e8$var$iso7064Check;
module.exports.luhnCheck = $ace9caca45ced9e8$var$luhnCheck;
module.exports.reverseMultiplyAndSum = $ace9caca45ced9e8$var$reverseMultiplyAndSum;
module.exports.verhoeffCheck = $ace9caca45ced9e8$var$verhoeffCheck;
/**
 * Algorithmic validation functions
 * May be used as is or implemented in the workflow of other validators.
 */ /*
 * ISO 7064 validation function
 * Called with a string of numbers (incl. check digit)
 * to validate according to ISO 7064 (MOD 11, 10).
 */ function $ace9caca45ced9e8$var$iso7064Check(str) {
    var checkvalue = 10;
    for(var i = 0; i < str.length - 1; i++)checkvalue = (parseInt(str[i], 10) + checkvalue) % 10 === 0 ? 9 : (parseInt(str[i], 10) + checkvalue) % 10 * 2 % 11;
    checkvalue = checkvalue === 1 ? 0 : 11 - checkvalue;
    return checkvalue === parseInt(str[10], 10);
}
/*
 * Luhn (mod 10) validation function
 * Called with a string of numbers (incl. check digit)
 * to validate according to the Luhn algorithm.
 */ function $ace9caca45ced9e8$var$luhnCheck(str) {
    var checksum = 0;
    var second = false;
    for(var i = str.length - 1; i >= 0; i--){
        if (second) {
            var product = parseInt(str[i], 10) * 2;
            if (product > 9) // sum digits of product and add to checksum
            checksum += product.toString().split("").map(function(a) {
                return parseInt(a, 10);
            }).reduce(function(a, b) {
                return a + b;
            }, 0);
            else checksum += product;
        } else checksum += parseInt(str[i], 10);
        second = !second;
    }
    return checksum % 10 === 0;
}
/*
 * Reverse TIN multiplication and summation helper function
 * Called with an array of single-digit integers and a base multiplier
 * to calculate the sum of the digits multiplied in reverse.
 * Normally used in variations of MOD 11 algorithmic checks.
 */ function $ace9caca45ced9e8$var$reverseMultiplyAndSum(digits, base) {
    var total = 0;
    for(var i = 0; i < digits.length; i++)total += digits[i] * (base - i);
    return total;
}
/*
 * Verhoeff validation helper function
 * Called with a string of numbers
 * to validate according to the Verhoeff algorithm.
 */ function $ace9caca45ced9e8$var$verhoeffCheck(str) {
    var d_table = [
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
        ],
        [
            1,
            2,
            3,
            4,
            0,
            6,
            7,
            8,
            9,
            5
        ],
        [
            2,
            3,
            4,
            0,
            1,
            7,
            8,
            9,
            5,
            6
        ],
        [
            3,
            4,
            0,
            1,
            2,
            8,
            9,
            5,
            6,
            7
        ],
        [
            4,
            0,
            1,
            2,
            3,
            9,
            5,
            6,
            7,
            8
        ],
        [
            5,
            9,
            8,
            7,
            6,
            0,
            4,
            3,
            2,
            1
        ],
        [
            6,
            5,
            9,
            8,
            7,
            1,
            0,
            4,
            3,
            2
        ],
        [
            7,
            6,
            5,
            9,
            8,
            2,
            1,
            0,
            4,
            3
        ],
        [
            8,
            7,
            6,
            5,
            9,
            3,
            2,
            1,
            0,
            4
        ],
        [
            9,
            8,
            7,
            6,
            5,
            4,
            3,
            2,
            1,
            0
        ]
    ];
    var p_table = [
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9
        ],
        [
            1,
            5,
            7,
            6,
            2,
            8,
            3,
            0,
            9,
            4
        ],
        [
            5,
            8,
            0,
            3,
            7,
            9,
            6,
            1,
            4,
            2
        ],
        [
            8,
            9,
            1,
            6,
            0,
            4,
            3,
            5,
            2,
            7
        ],
        [
            9,
            4,
            5,
            3,
            1,
            2,
            6,
            8,
            7,
            0
        ],
        [
            4,
            2,
            8,
            6,
            5,
            7,
            3,
            9,
            0,
            1
        ],
        [
            2,
            7,
            9,
            3,
            8,
            0,
            6,
            4,
            1,
            5
        ],
        [
            7,
            0,
            4,
            6,
            9,
            1,
            3,
            2,
            5,
            8
        ]
    ]; // Copy (to prevent replacement) and reverse
    var str_copy = str.split("").reverse().join("");
    var checksum = 0;
    for(var i = 0; i < str_copy.length; i++)checksum = d_table[checksum][p_table[i % 8][parseInt(str_copy[i], 10)]];
    return checksum === 0;
}

});


parcelRequire.register("foPrj", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $b360ffb54e673153$var$isMobilePhone;
module.exports.locales = void 0;

var $b360ffb54e673153$var$_assertString = $b360ffb54e673153$var$_interopRequireDefault((parcelRequire("1khYO")));
function $b360ffb54e673153$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* eslint-disable max-len */ var $b360ffb54e673153$var$phones = {
    "am-AM": /^(\+?374|0)((10|[9|7][0-9])\d{6}$|[2-4]\d{7}$)/,
    "ar-AE": /^((\+?971)|0)?5[024568]\d{7}$/,
    "ar-BH": /^(\+?973)?(3|6)\d{7}$/,
    "ar-DZ": /^(\+?213|0)(5|6|7)\d{8}$/,
    "ar-LB": /^(\+?961)?((3|81)\d{6}|7\d{7})$/,
    "ar-EG": /^((\+?20)|0)?1[0125]\d{8}$/,
    "ar-IQ": /^(\+?964|0)?7[0-9]\d{8}$/,
    "ar-JO": /^(\+?962|0)?7[789]\d{7}$/,
    "ar-KW": /^(\+?965)[569]\d{7}$/,
    "ar-LY": /^((\+?218)|0)?(9[1-6]\d{7}|[1-8]\d{7,9})$/,
    "ar-MA": /^(?:(?:\+|00)212|0)[5-7]\d{8}$/,
    "ar-OM": /^((\+|00)968)?(9[1-9])\d{6}$/,
    "ar-PS": /^(\+?970|0)5[6|9](\d{7})$/,
    "ar-SA": /^(!?(\+?966)|0)?5\d{8}$/,
    "ar-SY": /^(!?(\+?963)|0)?9\d{8}$/,
    "ar-TN": /^(\+?216)?[2459]\d{7}$/,
    "az-AZ": /^(\+994|0)(5[015]|7[07]|99)\d{7}$/,
    "bs-BA": /^((((\+|00)3876)|06))((([0-3]|[5-6])\d{6})|(4\d{7}))$/,
    "be-BY": /^(\+?375)?(24|25|29|33|44)\d{7}$/,
    "bg-BG": /^(\+?359|0)?8[789]\d{7}$/,
    "bn-BD": /^(\+?880|0)1[13456789][0-9]{8}$/,
    "ca-AD": /^(\+376)?[346]\d{5}$/,
    "cs-CZ": /^(\+?420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/,
    "da-DK": /^(\+?45)?\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2}$/,
    "de-DE": /^((\+49|0)[1|3])([0|5][0-45-9]\d|6([23]|0\d?)|7([0-57-9]|6\d))\d{7,9}$/,
    "de-AT": /^(\+43|0)\d{1,4}\d{3,12}$/,
    "de-CH": /^(\+41|0)([1-9])\d{1,9}$/,
    "de-LU": /^(\+352)?((6\d1)\d{6})$/,
    "dv-MV": /^(\+?960)?(7[2-9]|91|9[3-9])\d{7}$/,
    "el-GR": /^(\+?30|0)?(69\d{8})$/,
    "en-AU": /^(\+?61|0)4\d{8}$/,
    "en-BM": /^(\+?1)?441(((3|7)\d{6}$)|(5[0-3][0-9]\d{4}$)|(59\d{5}))/,
    "en-GB": /^(\+?44|0)7\d{9}$/,
    "en-GG": /^(\+?44|0)1481\d{6}$/,
    "en-GH": /^(\+233|0)(20|50|24|54|27|57|26|56|23|28|55|59)\d{7}$/,
    "en-GY": /^(\+592|0)6\d{6}$/,
    "en-HK": /^(\+?852[-\s]?)?[456789]\d{3}[-\s]?\d{4}$/,
    "en-MO": /^(\+?853[-\s]?)?[6]\d{3}[-\s]?\d{4}$/,
    "en-IE": /^(\+?353|0)8[356789]\d{7}$/,
    "en-IN": /^(\+?91|0)?[6789]\d{9}$/,
    "en-KE": /^(\+?254|0)(7|1)\d{8}$/,
    "en-KI": /^((\+686|686)?)?( )?((6|7)(2|3|8)[0-9]{6})$/,
    "en-MT": /^(\+?356|0)?(99|79|77|21|27|22|25)[0-9]{6}$/,
    "en-MU": /^(\+?230|0)?\d{8}$/,
    "en-NA": /^(\+?264|0)(6|8)\d{7}$/,
    "en-NG": /^(\+?234|0)?[789]\d{9}$/,
    "en-NZ": /^(\+?64|0)[28]\d{7,9}$/,
    "en-PK": /^((00|\+)?92|0)3[0-6]\d{8}$/,
    "en-PH": /^(09|\+639)\d{9}$/,
    "en-RW": /^(\+?250|0)?[7]\d{8}$/,
    "en-SG": /^(\+65)?[3689]\d{7}$/,
    "en-SL": /^(\+?232|0)\d{8}$/,
    "en-TZ": /^(\+?255|0)?[67]\d{8}$/,
    "en-UG": /^(\+?256|0)?[7]\d{8}$/,
    "en-US": /^((\+1|1)?( |-)?)?(\([2-9][0-9]{2}\)|[2-9][0-9]{2})( |-)?([2-9][0-9]{2}( |-)?[0-9]{4})$/,
    "en-ZA": /^(\+?27|0)\d{9}$/,
    "en-ZM": /^(\+?26)?09[567]\d{7}$/,
    "en-ZW": /^(\+263)[0-9]{9}$/,
    "en-BW": /^(\+?267)?(7[1-8]{1})\d{6}$/,
    "es-AR": /^\+?549(11|[2368]\d)\d{8}$/,
    "es-BO": /^(\+?591)?(6|7)\d{7}$/,
    "es-CO": /^(\+?57)?3(0(0|1|2|4|5)|1\d|2[0-4]|5(0|1))\d{7}$/,
    "es-CL": /^(\+?56|0)[2-9]\d{1}\d{7}$/,
    "es-CR": /^(\+506)?[2-8]\d{7}$/,
    "es-CU": /^(\+53|0053)?5\d{7}/,
    "es-DO": /^(\+?1)?8[024]9\d{7}$/,
    "es-HN": /^(\+?504)?[9|8]\d{7}$/,
    "es-EC": /^(\+?593|0)([2-7]|9[2-9])\d{7}$/,
    "es-ES": /^(\+?34)?[6|7]\d{8}$/,
    "es-PE": /^(\+?51)?9\d{8}$/,
    "es-MX": /^(\+?52)?(1|01)?\d{10,11}$/,
    "es-PA": /^(\+?507)\d{7,8}$/,
    "es-PY": /^(\+?595|0)9[9876]\d{7}$/,
    "es-SV": /^(\+?503)?[67]\d{7}$/,
    "es-UY": /^(\+598|0)9[1-9][\d]{6}$/,
    "es-VE": /^(\+?58)?(2|4)\d{9}$/,
    "et-EE": /^(\+?372)?\s?(5|8[1-4])\s?([0-9]\s?){6,7}$/,
    "fa-IR": /^(\+?98[\-\s]?|0)9[0-39]\d[\-\s]?\d{3}[\-\s]?\d{4}$/,
    "fi-FI": /^(\+?358|0)\s?(4(0|1|2|4|5|6)?|50)\s?(\d\s?){4,8}\d$/,
    "fj-FJ": /^(\+?679)?\s?\d{3}\s?\d{4}$/,
    "fo-FO": /^(\+?298)?\s?\d{2}\s?\d{2}\s?\d{2}$/,
    "fr-BF": /^(\+226|0)[67]\d{7}$/,
    "fr-CM": /^(\+?237)6[0-9]{8}$/,
    "fr-FR": /^(\+?33|0)[67]\d{8}$/,
    "fr-GF": /^(\+?594|0|00594)[67]\d{8}$/,
    "fr-GP": /^(\+?590|0|00590)[67]\d{8}$/,
    "fr-MQ": /^(\+?596|0|00596)[67]\d{8}$/,
    "fr-PF": /^(\+?689)?8[789]\d{6}$/,
    "fr-RE": /^(\+?262|0|00262)[67]\d{8}$/,
    "he-IL": /^(\+972|0)([23489]|5[012345689]|77)[1-9]\d{6}$/,
    "hu-HU": /^(\+?36|06)(20|30|31|50|70)\d{7}$/,
    "id-ID": /^(\+?62|0)8(1[123456789]|2[1238]|3[1238]|5[12356789]|7[78]|9[56789]|8[123456789])([\s?|\d]{5,11})$/,
    "it-IT": /^(\+?39)?\s?3\d{2} ?\d{6,7}$/,
    "it-SM": /^((\+378)|(0549)|(\+390549)|(\+3780549))?6\d{5,9}$/,
    "ja-JP": /^(\+81[ \-]?(\(0\))?|0)[6789]0[ \-]?\d{4}[ \-]?\d{4}$/,
    "ka-GE": /^(\+?995)?(5|79)\d{7}$/,
    "kk-KZ": /^(\+?7|8)?7\d{9}$/,
    "kl-GL": /^(\+?299)?\s?\d{2}\s?\d{2}\s?\d{2}$/,
    "ko-KR": /^((\+?82)[ \-]?)?0?1([0|1|6|7|8|9]{1})[ \-]?\d{3,4}[ \-]?\d{4}$/,
    "lt-LT": /^(\+370|8)\d{8}$/,
    "lv-LV": /^(\+?371)2\d{7}$/,
    "ms-MY": /^(\+?6?01){1}(([0145]{1}(\-|\s)?\d{7,8})|([236789]{1}(\s|\-)?\d{7}))$/,
    "mz-MZ": /^(\+?258)?8[234567]\d{7}$/,
    "nb-NO": /^(\+?47)?[49]\d{7}$/,
    "ne-NP": /^(\+?977)?9[78]\d{8}$/,
    "nl-BE": /^(\+?32|0)4\d{8}$/,
    "nl-NL": /^(((\+|00)?31\(0\))|((\+|00)?31)|0)6{1}\d{8}$/,
    "nn-NO": /^(\+?47)?[49]\d{7}$/,
    "pl-PL": /^(\+?48)? ?[5-8]\d ?\d{3} ?\d{2} ?\d{2}$/,
    "pt-BR": /^((\+?55\ ?[1-9]{2}\ ?)|(\+?55\ ?\([1-9]{2}\)\ ?)|(0[1-9]{2}\ ?)|(\([1-9]{2}\)\ ?)|([1-9]{2}\ ?))((\d{4}\-?\d{4})|(9[2-9]{1}\d{3}\-?\d{4}))$/,
    "pt-PT": /^(\+?351)?9[1236]\d{7}$/,
    "pt-AO": /^(\+244)\d{9}$/,
    "ro-RO": /^(\+?4?0)\s?7\d{2}(\/|\s|\.|\-)?\d{3}(\s|\.|\-)?\d{3}$/,
    "ru-RU": /^(\+?7|8)?9\d{9}$/,
    "si-LK": /^(?:0|94|\+94)?(7(0|1|2|4|5|6|7|8)( |-)?)\d{7}$/,
    "sl-SI": /^(\+386\s?|0)(\d{1}\s?\d{3}\s?\d{2}\s?\d{2}|\d{2}\s?\d{3}\s?\d{3})$/,
    "sk-SK": /^(\+?421)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/,
    "sq-AL": /^(\+355|0)6[789]\d{6}$/,
    "sr-RS": /^(\+3816|06)[- \d]{5,9}$/,
    "sv-SE": /^(\+?46|0)[\s\-]?7[\s\-]?[02369]([\s\-]?\d){7}$/,
    "tg-TJ": /^(\+?992)?[5][5]\d{7}$/,
    "th-TH": /^(\+66|66|0)\d{9}$/,
    "tr-TR": /^(\+?90|0)?5\d{9}$/,
    "tk-TM": /^(\+993|993|8)\d{8}$/,
    "uk-UA": /^(\+?38|8)?0\d{9}$/,
    "uz-UZ": /^(\+?998)?(6[125-79]|7[1-69]|88|9\d)\d{7}$/,
    "vi-VN": /^((\+?84)|0)((3([2-9]))|(5([25689]))|(7([0|6-9]))|(8([1-9]))|(9([0-9])))([0-9]{7})$/,
    "zh-CN": /^((\+|00)86)?(1[3-9]|9[28])\d{9}$/,
    "zh-TW": /^(\+?886\-?|0)?9\d{8}$/,
    "dz-BT": /^(\+?975|0)?(17|16|77|02)\d{6}$/
};
/* eslint-enable max-len */ // aliases
$b360ffb54e673153$var$phones["en-CA"] = $b360ffb54e673153$var$phones["en-US"];
$b360ffb54e673153$var$phones["fr-CA"] = $b360ffb54e673153$var$phones["en-CA"];
$b360ffb54e673153$var$phones["fr-BE"] = $b360ffb54e673153$var$phones["nl-BE"];
$b360ffb54e673153$var$phones["zh-HK"] = $b360ffb54e673153$var$phones["en-HK"];
$b360ffb54e673153$var$phones["zh-MO"] = $b360ffb54e673153$var$phones["en-MO"];
$b360ffb54e673153$var$phones["ga-IE"] = $b360ffb54e673153$var$phones["en-IE"];
$b360ffb54e673153$var$phones["fr-CH"] = $b360ffb54e673153$var$phones["de-CH"];
$b360ffb54e673153$var$phones["it-CH"] = $b360ffb54e673153$var$phones["fr-CH"];
function $b360ffb54e673153$var$isMobilePhone(str, locale, options) {
    (0, $b360ffb54e673153$var$_assertString.default)(str);
    if (options && options.strictMode && !str.startsWith("+")) return false;
    if (Array.isArray(locale)) return locale.some(function(key) {
        // https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md#ignoring-code-for-coverage-purposes
        // istanbul ignore else
        if ($b360ffb54e673153$var$phones.hasOwnProperty(key)) {
            var phone = $b360ffb54e673153$var$phones[key];
            if (phone.test(str)) return true;
        }
        return false;
    });
    else if (locale in $b360ffb54e673153$var$phones) return $b360ffb54e673153$var$phones[locale].test(str); // alias falsey locale as 'any'
    else if (!locale || locale === "any") {
        for(var key in $b360ffb54e673153$var$phones)// istanbul ignore else
        if ($b360ffb54e673153$var$phones.hasOwnProperty(key)) {
            var phone = $b360ffb54e673153$var$phones[key];
            if (phone.test(str)) return true;
        }
        return false;
    }
    throw new Error("Invalid locale '".concat(locale, "'"));
}
var $b360ffb54e673153$var$locales = Object.keys($b360ffb54e673153$var$phones);
module.exports.locales = $b360ffb54e673153$var$locales;

});

parcelRequire.register("8FnCl", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $64f47902e0b967d1$var$isEthereumAddress;

var $64f47902e0b967d1$var$_assertString = $64f47902e0b967d1$var$_interopRequireDefault((parcelRequire("1khYO")));
function $64f47902e0b967d1$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $64f47902e0b967d1$var$eth = /^(0x)[0-9a-f]{40}$/i;
function $64f47902e0b967d1$var$isEthereumAddress(str) {
    (0, $64f47902e0b967d1$var$_assertString.default)(str);
    return $64f47902e0b967d1$var$eth.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("cQXsU", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $95b877efb9c6fe2a$var$isCurrency;

var $95b877efb9c6fe2a$var$_merge = $95b877efb9c6fe2a$var$_interopRequireDefault((parcelRequire("lIjC8")));

var $95b877efb9c6fe2a$var$_assertString = $95b877efb9c6fe2a$var$_interopRequireDefault((parcelRequire("1khYO")));
function $95b877efb9c6fe2a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $95b877efb9c6fe2a$var$currencyRegex(options) {
    var decimal_digits = "\\d{".concat(options.digits_after_decimal[0], "}");
    options.digits_after_decimal.forEach(function(digit, index) {
        if (index !== 0) decimal_digits = "".concat(decimal_digits, "|\\d{").concat(digit, "}");
    });
    var symbol = "(".concat(options.symbol.replace(/\W/, function(m) {
        return "\\".concat(m);
    }), ")").concat(options.require_symbol ? "" : "?"), negative = "-?", whole_dollar_amount_without_sep = "[1-9]\\d*", whole_dollar_amount_with_sep = "[1-9]\\d{0,2}(\\".concat(options.thousands_separator, "\\d{3})*"), valid_whole_dollar_amounts = [
        "0",
        whole_dollar_amount_without_sep,
        whole_dollar_amount_with_sep
    ], whole_dollar_amount = "(".concat(valid_whole_dollar_amounts.join("|"), ")?"), decimal_amount = "(\\".concat(options.decimal_separator, "(").concat(decimal_digits, "))").concat(options.require_decimal ? "" : "?");
    var pattern = whole_dollar_amount + (options.allow_decimal || options.require_decimal ? decimal_amount : ""); // default is negative sign before symbol, but there are two other options (besides parens)
    if (options.allow_negatives && !options.parens_for_negatives) {
        if (options.negative_sign_after_digits) pattern += negative;
        else if (options.negative_sign_before_digits) pattern = negative + pattern;
    } // South African Rand, for example, uses R 123 (space) and R-123 (no space)
    if (options.allow_negative_sign_placeholder) pattern = "( (?!\\-))?".concat(pattern);
    else if (options.allow_space_after_symbol) pattern = " ?".concat(pattern);
    else if (options.allow_space_after_digits) pattern += "( (?!$))?";
    if (options.symbol_after_digits) pattern += symbol;
    else pattern = symbol + pattern;
    if (options.allow_negatives) {
        if (options.parens_for_negatives) pattern = "(\\(".concat(pattern, "\\)|").concat(pattern, ")");
        else if (!(options.negative_sign_before_digits || options.negative_sign_after_digits)) pattern = negative + pattern;
    } // ensure there's a dollar and/or decimal amount, and that
    // it doesn't start with a space or a negative sign followed by a space
    return new RegExp("^(?!-? )(?=.*\\d)".concat(pattern, "$"));
}
var $95b877efb9c6fe2a$var$default_currency_options = {
    symbol: "$",
    require_symbol: false,
    allow_space_after_symbol: false,
    symbol_after_digits: false,
    allow_negatives: true,
    parens_for_negatives: false,
    negative_sign_before_digits: false,
    negative_sign_after_digits: false,
    allow_negative_sign_placeholder: false,
    thousands_separator: ",",
    decimal_separator: ".",
    allow_decimal: true,
    require_decimal: false,
    digits_after_decimal: [
        2
    ],
    allow_space_after_digits: false
};
function $95b877efb9c6fe2a$var$isCurrency(str, options) {
    (0, $95b877efb9c6fe2a$var$_assertString.default)(str);
    options = (0, $95b877efb9c6fe2a$var$_merge.default)(options, $95b877efb9c6fe2a$var$default_currency_options);
    return $95b877efb9c6fe2a$var$currencyRegex(options).test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("6Ej0A", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $4d753930b34c39e0$var$isBtcAddress;

var $4d753930b34c39e0$var$_assertString = $4d753930b34c39e0$var$_interopRequireDefault((parcelRequire("1khYO")));
function $4d753930b34c39e0$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// supports Bech32 addresses
var $4d753930b34c39e0$var$bech32 = /^(bc1)[a-z0-9]{25,39}$/;
var $4d753930b34c39e0$var$base58 = /^(1|3)[A-HJ-NP-Za-km-z1-9]{25,39}$/;
function $4d753930b34c39e0$var$isBtcAddress(str) {
    (0, $4d753930b34c39e0$var$_assertString.default)(str); // check for bech32
    if (str.startsWith("bc1")) return $4d753930b34c39e0$var$bech32.test(str);
    return $4d753930b34c39e0$var$base58.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("OU6T2", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $099031eb3c0a7248$var$isISO8601;

var $099031eb3c0a7248$var$_assertString = $099031eb3c0a7248$var$_interopRequireDefault((parcelRequire("1khYO")));
function $099031eb3c0a7248$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* eslint-disable max-len */ // from http://goo.gl/0ejHHW
var $099031eb3c0a7248$var$iso8601 = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-3])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/; // same as above, except with a strict 'T' separator between date and time
var $099031eb3c0a7248$var$iso8601StrictSeparator = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-3])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;
/* eslint-enable max-len */ var $099031eb3c0a7248$var$isValidDate = function isValidDate(str) {
    // str must have passed the ISO8601 check
    // this check is meant to catch invalid dates
    // like 2009-02-31
    // first check for ordinal dates
    var ordinalMatch = str.match(/^(\d{4})-?(\d{3})([ T]{1}\.*|$)/);
    if (ordinalMatch) {
        var oYear = Number(ordinalMatch[1]);
        var oDay = Number(ordinalMatch[2]); // if is leap year
        if (oYear % 4 === 0 && oYear % 100 !== 0 || oYear % 400 === 0) return oDay <= 366;
        return oDay <= 365;
    }
    var match = str.match(/(\d{4})-?(\d{0,2})-?(\d*)/).map(Number);
    var year = match[1];
    var month = match[2];
    var day = match[3];
    var monthString = month ? "0".concat(month).slice(-2) : month;
    var dayString = day ? "0".concat(day).slice(-2) : day; // create a date object and compare
    var d = new Date("".concat(year, "-").concat(monthString || "01", "-").concat(dayString || "01"));
    if (month && day) return d.getUTCFullYear() === year && d.getUTCMonth() + 1 === month && d.getUTCDate() === day;
    return true;
};
function $099031eb3c0a7248$var$isISO8601(str) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, $099031eb3c0a7248$var$_assertString.default)(str);
    var check = options.strictSeparator ? $099031eb3c0a7248$var$iso8601StrictSeparator.test(str) : $099031eb3c0a7248$var$iso8601.test(str);
    if (check && options.strict) return $099031eb3c0a7248$var$isValidDate(str);
    return check;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("8bbyw", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5f485091dc16b178$var$isRFC3339;

var $5f485091dc16b178$var$_assertString = $5f485091dc16b178$var$_interopRequireDefault((parcelRequire("1khYO")));
function $5f485091dc16b178$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/* Based on https://tools.ietf.org/html/rfc3339#section-5.6 */ var $5f485091dc16b178$var$dateFullYear = /[0-9]{4}/;
var $5f485091dc16b178$var$dateMonth = /(0[1-9]|1[0-2])/;
var $5f485091dc16b178$var$dateMDay = /([12]\d|0[1-9]|3[01])/;
var $5f485091dc16b178$var$timeHour = /([01][0-9]|2[0-3])/;
var $5f485091dc16b178$var$timeMinute = /[0-5][0-9]/;
var $5f485091dc16b178$var$timeSecond = /([0-5][0-9]|60)/;
var $5f485091dc16b178$var$timeSecFrac = /(\.[0-9]+)?/;
var $5f485091dc16b178$var$timeNumOffset = new RegExp("[-+]".concat($5f485091dc16b178$var$timeHour.source, ":").concat($5f485091dc16b178$var$timeMinute.source));
var $5f485091dc16b178$var$timeOffset = new RegExp("([zZ]|".concat($5f485091dc16b178$var$timeNumOffset.source, ")"));
var $5f485091dc16b178$var$partialTime = new RegExp("".concat($5f485091dc16b178$var$timeHour.source, ":").concat($5f485091dc16b178$var$timeMinute.source, ":").concat($5f485091dc16b178$var$timeSecond.source).concat($5f485091dc16b178$var$timeSecFrac.source));
var $5f485091dc16b178$var$fullDate = new RegExp("".concat($5f485091dc16b178$var$dateFullYear.source, "-").concat($5f485091dc16b178$var$dateMonth.source, "-").concat($5f485091dc16b178$var$dateMDay.source));
var $5f485091dc16b178$var$fullTime = new RegExp("".concat($5f485091dc16b178$var$partialTime.source).concat($5f485091dc16b178$var$timeOffset.source));
var $5f485091dc16b178$var$rfc3339 = new RegExp("^".concat($5f485091dc16b178$var$fullDate.source, "[ tT]").concat($5f485091dc16b178$var$fullTime.source, "$"));
function $5f485091dc16b178$var$isRFC3339(str) {
    (0, $5f485091dc16b178$var$_assertString.default)(str);
    return $5f485091dc16b178$var$rfc3339.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7Q6iF", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5b5246c866f63884$var$isISO31661Alpha3;

var $5b5246c866f63884$var$_assertString = $5b5246c866f63884$var$_interopRequireDefault((parcelRequire("1khYO")));
function $5b5246c866f63884$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// from https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
var $5b5246c866f63884$var$validISO31661Alpha3CountriesCodes = new Set([
    "AFG",
    "ALA",
    "ALB",
    "DZA",
    "ASM",
    "AND",
    "AGO",
    "AIA",
    "ATA",
    "ATG",
    "ARG",
    "ARM",
    "ABW",
    "AUS",
    "AUT",
    "AZE",
    "BHS",
    "BHR",
    "BGD",
    "BRB",
    "BLR",
    "BEL",
    "BLZ",
    "BEN",
    "BMU",
    "BTN",
    "BOL",
    "BES",
    "BIH",
    "BWA",
    "BVT",
    "BRA",
    "IOT",
    "BRN",
    "BGR",
    "BFA",
    "BDI",
    "KHM",
    "CMR",
    "CAN",
    "CPV",
    "CYM",
    "CAF",
    "TCD",
    "CHL",
    "CHN",
    "CXR",
    "CCK",
    "COL",
    "COM",
    "COG",
    "COD",
    "COK",
    "CRI",
    "CIV",
    "HRV",
    "CUB",
    "CUW",
    "CYP",
    "CZE",
    "DNK",
    "DJI",
    "DMA",
    "DOM",
    "ECU",
    "EGY",
    "SLV",
    "GNQ",
    "ERI",
    "EST",
    "ETH",
    "FLK",
    "FRO",
    "FJI",
    "FIN",
    "FRA",
    "GUF",
    "PYF",
    "ATF",
    "GAB",
    "GMB",
    "GEO",
    "DEU",
    "GHA",
    "GIB",
    "GRC",
    "GRL",
    "GRD",
    "GLP",
    "GUM",
    "GTM",
    "GGY",
    "GIN",
    "GNB",
    "GUY",
    "HTI",
    "HMD",
    "VAT",
    "HND",
    "HKG",
    "HUN",
    "ISL",
    "IND",
    "IDN",
    "IRN",
    "IRQ",
    "IRL",
    "IMN",
    "ISR",
    "ITA",
    "JAM",
    "JPN",
    "JEY",
    "JOR",
    "KAZ",
    "KEN",
    "KIR",
    "PRK",
    "KOR",
    "KWT",
    "KGZ",
    "LAO",
    "LVA",
    "LBN",
    "LSO",
    "LBR",
    "LBY",
    "LIE",
    "LTU",
    "LUX",
    "MAC",
    "MKD",
    "MDG",
    "MWI",
    "MYS",
    "MDV",
    "MLI",
    "MLT",
    "MHL",
    "MTQ",
    "MRT",
    "MUS",
    "MYT",
    "MEX",
    "FSM",
    "MDA",
    "MCO",
    "MNG",
    "MNE",
    "MSR",
    "MAR",
    "MOZ",
    "MMR",
    "NAM",
    "NRU",
    "NPL",
    "NLD",
    "NCL",
    "NZL",
    "NIC",
    "NER",
    "NGA",
    "NIU",
    "NFK",
    "MNP",
    "NOR",
    "OMN",
    "PAK",
    "PLW",
    "PSE",
    "PAN",
    "PNG",
    "PRY",
    "PER",
    "PHL",
    "PCN",
    "POL",
    "PRT",
    "PRI",
    "QAT",
    "REU",
    "ROU",
    "RUS",
    "RWA",
    "BLM",
    "SHN",
    "KNA",
    "LCA",
    "MAF",
    "SPM",
    "VCT",
    "WSM",
    "SMR",
    "STP",
    "SAU",
    "SEN",
    "SRB",
    "SYC",
    "SLE",
    "SGP",
    "SXM",
    "SVK",
    "SVN",
    "SLB",
    "SOM",
    "ZAF",
    "SGS",
    "SSD",
    "ESP",
    "LKA",
    "SDN",
    "SUR",
    "SJM",
    "SWZ",
    "SWE",
    "CHE",
    "SYR",
    "TWN",
    "TJK",
    "TZA",
    "THA",
    "TLS",
    "TGO",
    "TKL",
    "TON",
    "TTO",
    "TUN",
    "TUR",
    "TKM",
    "TCA",
    "TUV",
    "UGA",
    "UKR",
    "ARE",
    "GBR",
    "USA",
    "UMI",
    "URY",
    "UZB",
    "VUT",
    "VEN",
    "VNM",
    "VGB",
    "VIR",
    "WLF",
    "ESH",
    "YEM",
    "ZMB",
    "ZWE"
]);
function $5b5246c866f63884$var$isISO31661Alpha3(str) {
    (0, $5b5246c866f63884$var$_assertString.default)(str);
    return $5b5246c866f63884$var$validISO31661Alpha3CountriesCodes.has(str.toUpperCase());
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("9bsC2", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $6afb560269106353$var$isISO4217;
module.exports.CurrencyCodes = void 0;

var $6afb560269106353$var$_assertString = $6afb560269106353$var$_interopRequireDefault((parcelRequire("1khYO")));
function $6afb560269106353$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// from https://en.wikipedia.org/wiki/ISO_4217
var $6afb560269106353$var$validISO4217CurrencyCodes = new Set([
    "AED",
    "AFN",
    "ALL",
    "AMD",
    "ANG",
    "AOA",
    "ARS",
    "AUD",
    "AWG",
    "AZN",
    "BAM",
    "BBD",
    "BDT",
    "BGN",
    "BHD",
    "BIF",
    "BMD",
    "BND",
    "BOB",
    "BOV",
    "BRL",
    "BSD",
    "BTN",
    "BWP",
    "BYN",
    "BZD",
    "CAD",
    "CDF",
    "CHE",
    "CHF",
    "CHW",
    "CLF",
    "CLP",
    "CNY",
    "COP",
    "COU",
    "CRC",
    "CUC",
    "CUP",
    "CVE",
    "CZK",
    "DJF",
    "DKK",
    "DOP",
    "DZD",
    "EGP",
    "ERN",
    "ETB",
    "EUR",
    "FJD",
    "FKP",
    "GBP",
    "GEL",
    "GHS",
    "GIP",
    "GMD",
    "GNF",
    "GTQ",
    "GYD",
    "HKD",
    "HNL",
    "HRK",
    "HTG",
    "HUF",
    "IDR",
    "ILS",
    "INR",
    "IQD",
    "IRR",
    "ISK",
    "JMD",
    "JOD",
    "JPY",
    "KES",
    "KGS",
    "KHR",
    "KMF",
    "KPW",
    "KRW",
    "KWD",
    "KYD",
    "KZT",
    "LAK",
    "LBP",
    "LKR",
    "LRD",
    "LSL",
    "LYD",
    "MAD",
    "MDL",
    "MGA",
    "MKD",
    "MMK",
    "MNT",
    "MOP",
    "MRU",
    "MUR",
    "MVR",
    "MWK",
    "MXN",
    "MXV",
    "MYR",
    "MZN",
    "NAD",
    "NGN",
    "NIO",
    "NOK",
    "NPR",
    "NZD",
    "OMR",
    "PAB",
    "PEN",
    "PGK",
    "PHP",
    "PKR",
    "PLN",
    "PYG",
    "QAR",
    "RON",
    "RSD",
    "RUB",
    "RWF",
    "SAR",
    "SBD",
    "SCR",
    "SDG",
    "SEK",
    "SGD",
    "SHP",
    "SLL",
    "SOS",
    "SRD",
    "SSP",
    "STN",
    "SVC",
    "SYP",
    "SZL",
    "THB",
    "TJS",
    "TMT",
    "TND",
    "TOP",
    "TRY",
    "TTD",
    "TWD",
    "TZS",
    "UAH",
    "UGX",
    "USD",
    "USN",
    "UYI",
    "UYU",
    "UYW",
    "UZS",
    "VES",
    "VND",
    "VUV",
    "WST",
    "XAF",
    "XAG",
    "XAU",
    "XBA",
    "XBB",
    "XBC",
    "XBD",
    "XCD",
    "XDR",
    "XOF",
    "XPD",
    "XPF",
    "XPT",
    "XSU",
    "XTS",
    "XUA",
    "XXX",
    "YER",
    "ZAR",
    "ZMW",
    "ZWL"
]);
function $6afb560269106353$var$isISO4217(str) {
    (0, $6afb560269106353$var$_assertString.default)(str);
    return $6afb560269106353$var$validISO4217CurrencyCodes.has(str.toUpperCase());
}
var $6afb560269106353$var$CurrencyCodes = $6afb560269106353$var$validISO4217CurrencyCodes;
module.exports.CurrencyCodes = $6afb560269106353$var$CurrencyCodes;

});

parcelRequire.register("eXqHg", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $ae3b48cae37c24f7$var$isBase32;

var $ae3b48cae37c24f7$var$_assertString = $ae3b48cae37c24f7$var$_interopRequireDefault((parcelRequire("1khYO")));
function $ae3b48cae37c24f7$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $ae3b48cae37c24f7$var$base32 = /^[A-Z2-7]+=*$/;
function $ae3b48cae37c24f7$var$isBase32(str) {
    (0, $ae3b48cae37c24f7$var$_assertString.default)(str);
    var len = str.length;
    if (len % 8 === 0 && $ae3b48cae37c24f7$var$base32.test(str)) return true;
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("a64ZZ", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $759e5792c43b40bc$var$isBase58;

var $759e5792c43b40bc$var$_assertString = $759e5792c43b40bc$var$_interopRequireDefault((parcelRequire("1khYO")));
function $759e5792c43b40bc$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// Accepted chars - 123456789ABCDEFGH JKLMN PQRSTUVWXYZabcdefghijk mnopqrstuvwxyz
var $759e5792c43b40bc$var$base58Reg = /^[A-HJ-NP-Za-km-z1-9]*$/;
function $759e5792c43b40bc$var$isBase58(str) {
    (0, $759e5792c43b40bc$var$_assertString.default)(str);
    if ($759e5792c43b40bc$var$base58Reg.test(str)) return true;
    return false;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("hwQPp", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $cc2e67697b46a21e$var$isDataURI;

var $cc2e67697b46a21e$var$_assertString = $cc2e67697b46a21e$var$_interopRequireDefault((parcelRequire("1khYO")));
function $cc2e67697b46a21e$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $cc2e67697b46a21e$var$validMediaType = /^[a-z]+\/[a-z0-9\-\+]+$/i;
var $cc2e67697b46a21e$var$validAttribute = /^[a-z\-]+=[a-z0-9\-]+$/i;
var $cc2e67697b46a21e$var$validData = /^[a-z0-9!\$&'\(\)\*\+,;=\-\._~:@\/\?%\s]*$/i;
function $cc2e67697b46a21e$var$isDataURI(str) {
    (0, $cc2e67697b46a21e$var$_assertString.default)(str);
    var data = str.split(",");
    if (data.length < 2) return false;
    var attributes = data.shift().trim().split(";");
    var schemeAndMediaType = attributes.shift();
    if (schemeAndMediaType.substr(0, 5) !== "data:") return false;
    var mediaType = schemeAndMediaType.substr(5);
    if (mediaType !== "" && !$cc2e67697b46a21e$var$validMediaType.test(mediaType)) return false;
    for(var i = 0; i < attributes.length; i++){
        if (!(i === attributes.length - 1 && attributes[i].toLowerCase() === "base64") && !$cc2e67697b46a21e$var$validAttribute.test(attributes[i])) return false;
    }
    for(var _i = 0; _i < data.length; _i++){
        if (!$cc2e67697b46a21e$var$validData.test(data[_i])) return false;
    }
    return true;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("j5m7M", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $de4fa56e947133e7$var$isMagnetURI;

var $de4fa56e947133e7$var$_assertString = $de4fa56e947133e7$var$_interopRequireDefault((parcelRequire("1khYO")));
function $de4fa56e947133e7$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $de4fa56e947133e7$var$magnetURI = /^magnet:\?xt(?:\.1)?=urn:(?:aich|bitprint|btih|ed2k|ed2khash|kzhash|md5|sha1|tree:tiger):[a-z0-9]{32}(?:[a-z0-9]{8})?($|&)/i;
function $de4fa56e947133e7$var$isMagnetURI(url) {
    (0, $de4fa56e947133e7$var$_assertString.default)(url);
    return $de4fa56e947133e7$var$magnetURI.test(url.trim());
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("ejOce", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a6c9c6f524a5d37e$var$isMimeType;

var $a6c9c6f524a5d37e$var$_assertString = $a6c9c6f524a5d37e$var$_interopRequireDefault((parcelRequire("1khYO")));
function $a6c9c6f524a5d37e$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
/*
  Checks if the provided string matches to a correct Media type format (MIME type)

  This function only checks is the string format follows the
  etablished rules by the according RFC specifications.
  This function supports 'charset' in textual media types
  (https://tools.ietf.org/html/rfc6657).

  This function does not check against all the media types listed
  by the IANA (https://www.iana.org/assignments/media-types/media-types.xhtml)
  because of lightness purposes : it would require to include
  all these MIME types in this librairy, which would weigh it
  significantly. This kind of effort maybe is not worth for the use that
  this function has in this entire librairy.

  More informations in the RFC specifications :
  - https://tools.ietf.org/html/rfc2045
  - https://tools.ietf.org/html/rfc2046
  - https://tools.ietf.org/html/rfc7231#section-3.1.1.1
  - https://tools.ietf.org/html/rfc7231#section-3.1.1.5
*/ // Match simple MIME types
// NB :
//   Subtype length must not exceed 100 characters.
//   This rule does not comply to the RFC specs (what is the max length ?).
var $a6c9c6f524a5d37e$var$mimeTypeSimple = /^(application|audio|font|image|message|model|multipart|text|video)\/[a-zA-Z0-9\.\-\+]{1,100}$/i; // eslint-disable-line max-len
// Handle "charset" in "text/*"
var $a6c9c6f524a5d37e$var$mimeTypeText = /^text\/[a-zA-Z0-9\.\-\+]{1,100};\s?charset=("[a-zA-Z0-9\.\-\+\s]{0,70}"|[a-zA-Z0-9\.\-\+]{0,70})(\s?\([a-zA-Z0-9\.\-\+\s]{1,20}\))?$/i; // eslint-disable-line max-len
// Handle "boundary" in "multipart/*"
var $a6c9c6f524a5d37e$var$mimeTypeMultipart = /^multipart\/[a-zA-Z0-9\.\-\+]{1,100}(;\s?(boundary|charset)=("[a-zA-Z0-9\.\-\+\s]{0,70}"|[a-zA-Z0-9\.\-\+]{0,70})(\s?\([a-zA-Z0-9\.\-\+\s]{1,20}\))?){0,2}$/i; // eslint-disable-line max-len
function $a6c9c6f524a5d37e$var$isMimeType(str) {
    (0, $a6c9c6f524a5d37e$var$_assertString.default)(str);
    return $a6c9c6f524a5d37e$var$mimeTypeSimple.test(str) || $a6c9c6f524a5d37e$var$mimeTypeText.test(str) || $a6c9c6f524a5d37e$var$mimeTypeMultipart.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("iO9To", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $db1491f48200474d$var$isLatLong;

var $db1491f48200474d$var$_assertString = $db1491f48200474d$var$_interopRequireDefault((parcelRequire("1khYO")));

var $db1491f48200474d$var$_merge = $db1491f48200474d$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $db1491f48200474d$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $db1491f48200474d$var$lat = /^\(?[+-]?(90(\.0+)?|[1-8]?\d(\.\d+)?)$/;
var $db1491f48200474d$var$long = /^\s?[+-]?(180(\.0+)?|1[0-7]\d(\.\d+)?|\d{1,2}(\.\d+)?)\)?$/;
var $db1491f48200474d$var$latDMS = /^(([1-8]?\d)\D+([1-5]?\d|60)\D+([1-5]?\d|60)(\.\d+)?|90\D+0\D+0)\D+[NSns]?$/i;
var $db1491f48200474d$var$longDMS = /^\s*([1-7]?\d{1,2}\D+([1-5]?\d|60)\D+([1-5]?\d|60)(\.\d+)?|180\D+0\D+0)\D+[EWew]?$/i;
var $db1491f48200474d$var$defaultLatLongOptions = {
    checkDMS: false
};
function $db1491f48200474d$var$isLatLong(str, options) {
    (0, $db1491f48200474d$var$_assertString.default)(str);
    options = (0, $db1491f48200474d$var$_merge.default)(options, $db1491f48200474d$var$defaultLatLongOptions);
    if (!str.includes(",")) return false;
    var pair = str.split(",");
    if (pair[0].startsWith("(") && !pair[1].endsWith(")") || pair[1].endsWith(")") && !pair[0].startsWith("(")) return false;
    if (options.checkDMS) return $db1491f48200474d$var$latDMS.test(pair[0]) && $db1491f48200474d$var$longDMS.test(pair[1]);
    return $db1491f48200474d$var$lat.test(pair[0]) && $db1491f48200474d$var$long.test(pair[1]);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("6D7eY", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $4d3c006490a051c2$var$isPostalCode;
module.exports.locales = void 0;

var $4d3c006490a051c2$var$_assertString = $4d3c006490a051c2$var$_interopRequireDefault((parcelRequire("1khYO")));
function $4d3c006490a051c2$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
// common patterns
var $4d3c006490a051c2$var$threeDigit = /^\d{3}$/;
var $4d3c006490a051c2$var$fourDigit = /^\d{4}$/;
var $4d3c006490a051c2$var$fiveDigit = /^\d{5}$/;
var $4d3c006490a051c2$var$sixDigit = /^\d{6}$/;
var $4d3c006490a051c2$var$patterns = {
    AD: /^AD\d{3}$/,
    AT: $4d3c006490a051c2$var$fourDigit,
    AU: $4d3c006490a051c2$var$fourDigit,
    AZ: /^AZ\d{4}$/,
    BE: $4d3c006490a051c2$var$fourDigit,
    BG: $4d3c006490a051c2$var$fourDigit,
    BR: /^\d{5}-\d{3}$/,
    BY: /2[1-4]{1}\d{4}$/,
    CA: /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][\s\-]?\d[ABCEGHJ-NPRSTV-Z]\d$/i,
    CH: $4d3c006490a051c2$var$fourDigit,
    CN: /^(0[1-7]|1[012356]|2[0-7]|3[0-6]|4[0-7]|5[1-7]|6[1-7]|7[1-5]|8[1345]|9[09])\d{4}$/,
    CZ: /^\d{3}\s?\d{2}$/,
    DE: $4d3c006490a051c2$var$fiveDigit,
    DK: $4d3c006490a051c2$var$fourDigit,
    DO: $4d3c006490a051c2$var$fiveDigit,
    DZ: $4d3c006490a051c2$var$fiveDigit,
    EE: $4d3c006490a051c2$var$fiveDigit,
    ES: /^(5[0-2]{1}|[0-4]{1}\d{1})\d{3}$/,
    FI: $4d3c006490a051c2$var$fiveDigit,
    FR: /^\d{2}\s?\d{3}$/,
    GB: /^(gir\s?0aa|[a-z]{1,2}\d[\da-z]?\s?(\d[a-z]{2})?)$/i,
    GR: /^\d{3}\s?\d{2}$/,
    HR: /^([1-5]\d{4}$)/,
    HT: /^HT\d{4}$/,
    HU: $4d3c006490a051c2$var$fourDigit,
    ID: $4d3c006490a051c2$var$fiveDigit,
    IE: /^(?!.*(?:o))[A-Za-z]\d[\dw]\s\w{4}$/i,
    IL: /^(\d{5}|\d{7})$/,
    IN: /^((?!10|29|35|54|55|65|66|86|87|88|89)[1-9][0-9]{5})$/,
    IR: /\b(?!(\d)\1{3})[13-9]{4}[1346-9][013-9]{5}\b/,
    IS: $4d3c006490a051c2$var$threeDigit,
    IT: $4d3c006490a051c2$var$fiveDigit,
    JP: /^\d{3}\-\d{4}$/,
    KE: $4d3c006490a051c2$var$fiveDigit,
    KR: /^(\d{5}|\d{6})$/,
    LI: /^(948[5-9]|949[0-7])$/,
    LT: /^LT\-\d{5}$/,
    LU: $4d3c006490a051c2$var$fourDigit,
    LV: /^LV\-\d{4}$/,
    LK: $4d3c006490a051c2$var$fiveDigit,
    MX: $4d3c006490a051c2$var$fiveDigit,
    MT: /^[A-Za-z]{3}\s{0,1}\d{4}$/,
    MY: $4d3c006490a051c2$var$fiveDigit,
    NL: /^\d{4}\s?[a-z]{2}$/i,
    NO: $4d3c006490a051c2$var$fourDigit,
    NP: /^(10|21|22|32|33|34|44|45|56|57)\d{3}$|^(977)$/i,
    NZ: $4d3c006490a051c2$var$fourDigit,
    PL: /^\d{2}\-\d{3}$/,
    PR: /^00[679]\d{2}([ -]\d{4})?$/,
    PT: /^\d{4}\-\d{3}?$/,
    RO: $4d3c006490a051c2$var$sixDigit,
    RU: $4d3c006490a051c2$var$sixDigit,
    SA: $4d3c006490a051c2$var$fiveDigit,
    SE: /^[1-9]\d{2}\s?\d{2}$/,
    SG: $4d3c006490a051c2$var$sixDigit,
    SI: $4d3c006490a051c2$var$fourDigit,
    SK: /^\d{3}\s?\d{2}$/,
    TH: $4d3c006490a051c2$var$fiveDigit,
    TN: $4d3c006490a051c2$var$fourDigit,
    TW: /^\d{3}(\d{2})?$/,
    UA: $4d3c006490a051c2$var$fiveDigit,
    US: /^\d{5}(-\d{4})?$/,
    ZA: $4d3c006490a051c2$var$fourDigit,
    ZM: $4d3c006490a051c2$var$fiveDigit
};
var $4d3c006490a051c2$var$locales = Object.keys($4d3c006490a051c2$var$patterns);
module.exports.locales = $4d3c006490a051c2$var$locales;
function $4d3c006490a051c2$var$isPostalCode(str, locale) {
    (0, $4d3c006490a051c2$var$_assertString.default)(str);
    if (locale in $4d3c006490a051c2$var$patterns) return $4d3c006490a051c2$var$patterns[locale].test(str);
    else if (locale === "any") {
        for(var key in $4d3c006490a051c2$var$patterns)// https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md#ignoring-code-for-coverage-purposes
        // istanbul ignore else
        if ($4d3c006490a051c2$var$patterns.hasOwnProperty(key)) {
            var pattern = $4d3c006490a051c2$var$patterns[key];
            if (pattern.test(str)) return true;
        }
        return false;
    }
    throw new Error("Invalid locale '".concat(locale, "'"));
}

});

parcelRequire.register("eV26l", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $adc804d672a47534$var$ltrim;

var $adc804d672a47534$var$_assertString = $adc804d672a47534$var$_interopRequireDefault((parcelRequire("1khYO")));
function $adc804d672a47534$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $adc804d672a47534$var$ltrim(str, chars) {
    (0, $adc804d672a47534$var$_assertString.default)(str); // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
    var pattern = chars ? new RegExp("^[".concat(chars.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), "]+"), "g") : /^\s+/g;
    return str.replace(pattern, "");
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("cdqrX", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $8e4b3750d4ee024d$var$rtrim;

var $8e4b3750d4ee024d$var$_assertString = $8e4b3750d4ee024d$var$_interopRequireDefault((parcelRequire("1khYO")));
function $8e4b3750d4ee024d$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $8e4b3750d4ee024d$var$rtrim(str, chars) {
    (0, $8e4b3750d4ee024d$var$_assertString.default)(str);
    if (chars) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
        var pattern = new RegExp("[".concat(chars.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), "]+$"), "g");
        return str.replace(pattern, "");
    } // Use a faster and more safe than regex trim method https://blog.stevenlevithan.com/archives/faster-trim-javascript
    var strIndex = str.length - 1;
    while(/\s/.test(str.charAt(strIndex)))strIndex -= 1;
    return str.slice(0, strIndex + 1);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("fDF1p", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $b62a519e90505584$var$trim;

var $b62a519e90505584$var$_rtrim = $b62a519e90505584$var$_interopRequireDefault((parcelRequire("cdqrX")));

var $b62a519e90505584$var$_ltrim = $b62a519e90505584$var$_interopRequireDefault((parcelRequire("eV26l")));
function $b62a519e90505584$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $b62a519e90505584$var$trim(str, chars) {
    return (0, $b62a519e90505584$var$_rtrim.default)((0, $b62a519e90505584$var$_ltrim.default)(str, chars), chars);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("7VXXJ", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $5c6c5e95df68c3a5$var$escape;

var $5c6c5e95df68c3a5$var$_assertString = $5c6c5e95df68c3a5$var$_interopRequireDefault((parcelRequire("1khYO")));
function $5c6c5e95df68c3a5$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $5c6c5e95df68c3a5$var$escape(str) {
    (0, $5c6c5e95df68c3a5$var$_assertString.default)(str);
    return str.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\//g, "&#x2F;").replace(/\\/g, "&#x5C;").replace(/`/g, "&#96;");
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("aKIrY", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $7d40ae2414d10ff8$var$unescape;

var $7d40ae2414d10ff8$var$_assertString = $7d40ae2414d10ff8$var$_interopRequireDefault((parcelRequire("1khYO")));
function $7d40ae2414d10ff8$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $7d40ae2414d10ff8$var$unescape(str) {
    (0, $7d40ae2414d10ff8$var$_assertString.default)(str);
    return str.replace(/&quot;/g, '"').replace(/&#x27;/g, "'").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&#x2F;/g, "/").replace(/&#x5C;/g, "\\").replace(/&#96;/g, "`").replace(/&amp;/g, "&"); // &amp; replacement has to be the last one to prevent
// bugs with intermediate strings containing escape sequences
// See: https://github.com/validatorjs/validator.js/issues/1827
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("3I1Oz", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $2b36e3831b9661b1$var$stripLow;

var $2b36e3831b9661b1$var$_assertString = $2b36e3831b9661b1$var$_interopRequireDefault((parcelRequire("1khYO")));

var $2b36e3831b9661b1$var$_blacklist = $2b36e3831b9661b1$var$_interopRequireDefault((parcelRequire("cllFM")));
function $2b36e3831b9661b1$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $2b36e3831b9661b1$var$stripLow(str, keep_new_lines) {
    (0, $2b36e3831b9661b1$var$_assertString.default)(str);
    var chars = keep_new_lines ? "\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F" : "\\x00-\\x1F\\x7F";
    return (0, $2b36e3831b9661b1$var$_blacklist.default)(str, chars);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});
parcelRequire.register("cllFM", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $8fc841ffa727d6ef$var$blacklist;

var $8fc841ffa727d6ef$var$_assertString = $8fc841ffa727d6ef$var$_interopRequireDefault((parcelRequire("1khYO")));
function $8fc841ffa727d6ef$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $8fc841ffa727d6ef$var$blacklist(str, chars) {
    (0, $8fc841ffa727d6ef$var$_assertString.default)(str);
    return str.replace(new RegExp("[".concat(chars, "]+"), "g"), "");
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});


parcelRequire.register("6qSw4", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $4aef759633884e53$var$whitelist;

var $4aef759633884e53$var$_assertString = $4aef759633884e53$var$_interopRequireDefault((parcelRequire("1khYO")));
function $4aef759633884e53$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $4aef759633884e53$var$whitelist(str, chars) {
    (0, $4aef759633884e53$var$_assertString.default)(str);
    return str.replace(new RegExp("[^".concat(chars, "]+"), "g"), "");
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("gdekX", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $bcd8fb3f769f9641$var$isWhitelisted;

var $bcd8fb3f769f9641$var$_assertString = $bcd8fb3f769f9641$var$_interopRequireDefault((parcelRequire("1khYO")));
function $bcd8fb3f769f9641$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
function $bcd8fb3f769f9641$var$isWhitelisted(str, chars) {
    (0, $bcd8fb3f769f9641$var$_assertString.default)(str);
    for(var i = str.length - 1; i >= 0; i--){
        if (chars.indexOf(str[i]) === -1) return false;
    }
    return true;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("aSY8z", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $7ecd988403286e44$var$normalizeEmail;

var $7ecd988403286e44$var$_merge = $7ecd988403286e44$var$_interopRequireDefault((parcelRequire("lIjC8")));
function $7ecd988403286e44$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $7ecd988403286e44$var$default_normalize_email_options = {
    // The following options apply to all email addresses
    // Lowercases the local part of the email address.
    // Please note this may violate RFC 5321 as per http://stackoverflow.com/a/9808332/192024).
    // The domain is always lowercased, as per RFC 1035
    all_lowercase: true,
    // The following conversions are specific to GMail
    // Lowercases the local part of the GMail address (known to be case-insensitive)
    gmail_lowercase: true,
    // Removes dots from the local part of the email address, as that's ignored by GMail
    gmail_remove_dots: true,
    // Removes the subaddress (e.g. "+foo") from the email address
    gmail_remove_subaddress: true,
    // Conversts the googlemail.com domain to gmail.com
    gmail_convert_googlemaildotcom: true,
    // The following conversions are specific to Outlook.com / Windows Live / Hotmail
    // Lowercases the local part of the Outlook.com address (known to be case-insensitive)
    outlookdotcom_lowercase: true,
    // Removes the subaddress (e.g. "+foo") from the email address
    outlookdotcom_remove_subaddress: true,
    // The following conversions are specific to Yahoo
    // Lowercases the local part of the Yahoo address (known to be case-insensitive)
    yahoo_lowercase: true,
    // Removes the subaddress (e.g. "-foo") from the email address
    yahoo_remove_subaddress: true,
    // The following conversions are specific to Yandex
    // Lowercases the local part of the Yandex address (known to be case-insensitive)
    yandex_lowercase: true,
    // The following conversions are specific to iCloud
    // Lowercases the local part of the iCloud address (known to be case-insensitive)
    icloud_lowercase: true,
    // Removes the subaddress (e.g. "+foo") from the email address
    icloud_remove_subaddress: true
}; // List of domains used by iCloud
var $7ecd988403286e44$var$icloud_domains = [
    "icloud.com",
    "me.com"
]; // List of domains used by Outlook.com and its predecessors
// This list is likely incomplete.
// Partial reference:
// https://blogs.office.com/2013/04/17/outlook-com-gets-two-step-verification-sign-in-by-alias-and-new-international-domains/
var $7ecd988403286e44$var$outlookdotcom_domains = [
    "hotmail.at",
    "hotmail.be",
    "hotmail.ca",
    "hotmail.cl",
    "hotmail.co.il",
    "hotmail.co.nz",
    "hotmail.co.th",
    "hotmail.co.uk",
    "hotmail.com",
    "hotmail.com.ar",
    "hotmail.com.au",
    "hotmail.com.br",
    "hotmail.com.gr",
    "hotmail.com.mx",
    "hotmail.com.pe",
    "hotmail.com.tr",
    "hotmail.com.vn",
    "hotmail.cz",
    "hotmail.de",
    "hotmail.dk",
    "hotmail.es",
    "hotmail.fr",
    "hotmail.hu",
    "hotmail.id",
    "hotmail.ie",
    "hotmail.in",
    "hotmail.it",
    "hotmail.jp",
    "hotmail.kr",
    "hotmail.lv",
    "hotmail.my",
    "hotmail.ph",
    "hotmail.pt",
    "hotmail.sa",
    "hotmail.sg",
    "hotmail.sk",
    "live.be",
    "live.co.uk",
    "live.com",
    "live.com.ar",
    "live.com.mx",
    "live.de",
    "live.es",
    "live.eu",
    "live.fr",
    "live.it",
    "live.nl",
    "msn.com",
    "outlook.at",
    "outlook.be",
    "outlook.cl",
    "outlook.co.il",
    "outlook.co.nz",
    "outlook.co.th",
    "outlook.com",
    "outlook.com.ar",
    "outlook.com.au",
    "outlook.com.br",
    "outlook.com.gr",
    "outlook.com.pe",
    "outlook.com.tr",
    "outlook.com.vn",
    "outlook.cz",
    "outlook.de",
    "outlook.dk",
    "outlook.es",
    "outlook.fr",
    "outlook.hu",
    "outlook.id",
    "outlook.ie",
    "outlook.in",
    "outlook.it",
    "outlook.jp",
    "outlook.kr",
    "outlook.lv",
    "outlook.my",
    "outlook.ph",
    "outlook.pt",
    "outlook.sa",
    "outlook.sg",
    "outlook.sk",
    "passport.com"
]; // List of domains used by Yahoo Mail
// This list is likely incomplete
var $7ecd988403286e44$var$yahoo_domains = [
    "rocketmail.com",
    "yahoo.ca",
    "yahoo.co.uk",
    "yahoo.com",
    "yahoo.de",
    "yahoo.fr",
    "yahoo.in",
    "yahoo.it",
    "ymail.com"
]; // List of domains used by yandex.ru
var $7ecd988403286e44$var$yandex_domains = [
    "yandex.ru",
    "yandex.ua",
    "yandex.kz",
    "yandex.com",
    "yandex.by",
    "ya.ru"
]; // replace single dots, but not multiple consecutive dots
function $7ecd988403286e44$var$dotsReplacer(match) {
    if (match.length > 1) return match;
    return "";
}
function $7ecd988403286e44$var$normalizeEmail(email, options) {
    options = (0, $7ecd988403286e44$var$_merge.default)(options, $7ecd988403286e44$var$default_normalize_email_options);
    var raw_parts = email.split("@");
    var domain = raw_parts.pop();
    var user = raw_parts.join("@");
    var parts = [
        user,
        domain
    ]; // The domain is always lowercased, as it's case-insensitive per RFC 1035
    parts[1] = parts[1].toLowerCase();
    if (parts[1] === "gmail.com" || parts[1] === "googlemail.com") {
        // Address is GMail
        if (options.gmail_remove_subaddress) parts[0] = parts[0].split("+")[0];
        if (options.gmail_remove_dots) // this does not replace consecutive dots like example..email@gmail.com
        parts[0] = parts[0].replace(/\.+/g, $7ecd988403286e44$var$dotsReplacer);
        if (!parts[0].length) return false;
        if (options.all_lowercase || options.gmail_lowercase) parts[0] = parts[0].toLowerCase();
        parts[1] = options.gmail_convert_googlemaildotcom ? "gmail.com" : parts[1];
    } else if ($7ecd988403286e44$var$icloud_domains.indexOf(parts[1]) >= 0) {
        // Address is iCloud
        if (options.icloud_remove_subaddress) parts[0] = parts[0].split("+")[0];
        if (!parts[0].length) return false;
        if (options.all_lowercase || options.icloud_lowercase) parts[0] = parts[0].toLowerCase();
    } else if ($7ecd988403286e44$var$outlookdotcom_domains.indexOf(parts[1]) >= 0) {
        // Address is Outlook.com
        if (options.outlookdotcom_remove_subaddress) parts[0] = parts[0].split("+")[0];
        if (!parts[0].length) return false;
        if (options.all_lowercase || options.outlookdotcom_lowercase) parts[0] = parts[0].toLowerCase();
    } else if ($7ecd988403286e44$var$yahoo_domains.indexOf(parts[1]) >= 0) {
        // Address is Yahoo
        if (options.yahoo_remove_subaddress) {
            var components = parts[0].split("-");
            parts[0] = components.length > 1 ? components.slice(0, -1).join("-") : components[0];
        }
        if (!parts[0].length) return false;
        if (options.all_lowercase || options.yahoo_lowercase) parts[0] = parts[0].toLowerCase();
    } else if ($7ecd988403286e44$var$yandex_domains.indexOf(parts[1]) >= 0) {
        if (options.all_lowercase || options.yandex_lowercase) parts[0] = parts[0].toLowerCase();
        parts[1] = "yandex.ru"; // all yandex domains are equal, 1st preferred
    } else if (options.all_lowercase) // Any other address
    parts[0] = parts[0].toLowerCase();
    return parts.join("@");
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("gRAWw", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $c46e40a8738951f3$var$isSlug;

var $c46e40a8738951f3$var$_assertString = $c46e40a8738951f3$var$_interopRequireDefault((parcelRequire("1khYO")));
function $c46e40a8738951f3$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $c46e40a8738951f3$var$charsetRegex = /^[^\s-_](?!.*?[-_]{2,})[a-z0-9-\\][^\s]*[^-_\s]$/;
function $c46e40a8738951f3$var$isSlug(str) {
    (0, $c46e40a8738951f3$var$_assertString.default)(str);
    return $c46e40a8738951f3$var$charsetRegex.test(str);
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("dM67q", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $a07481227fc91d05$var$isLicensePlate;

var $a07481227fc91d05$var$_assertString = $a07481227fc91d05$var$_interopRequireDefault((parcelRequire("1khYO")));
function $a07481227fc91d05$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $a07481227fc91d05$var$validators = {
    "cs-CZ": function csCZ(str) {
        return /^(([ABCDEFHKIJKLMNPRSTUVXYZ]|[0-9])-?){5,8}$/.test(str);
    },
    "de-DE": function deDE(str) {
        return /^((AW|UL|AK|GA|AÖ|LF|AZ|AM|AS|ZE|AN|AB|A|KG|KH|BA|EW|BZ|HY|KM|BT|HP|B|BC|BI|BO|FN|TT|ÜB|BN|AH|BS|FR|HB|ZZ|BB|BK|BÖ|OC|OK|CW|CE|C|CO|LH|CB|KW|LC|LN|DA|DI|DE|DH|SY|NÖ|DO|DD|DU|DN|D|EI|EA|EE|FI|EM|EL|EN|PF|ED|EF|ER|AU|ZP|E|ES|NT|EU|FL|FO|FT|FF|F|FS|FD|FÜ|GE|G|GI|GF|GS|ZR|GG|GP|GR|NY|ZI|GÖ|GZ|GT|HA|HH|HM|HU|WL|HZ|WR|RN|HK|HD|HN|HS|GK|HE|HF|RZ|HI|HG|HO|HX|IK|IL|IN|J|JL|KL|KA|KS|KF|KE|KI|KT|KO|KN|KR|KC|KU|K|LD|LL|LA|L|OP|LM|LI|LB|LU|LÖ|HL|LG|MD|GN|MZ|MA|ML|MR|MY|AT|DM|MC|NZ|RM|RG|MM|ME|MB|MI|FG|DL|HC|MW|RL|MK|MG|MÜ|WS|MH|M|MS|NU|NB|ND|NM|NK|NW|NR|NI|NF|DZ|EB|OZ|TG|TO|N|OA|GM|OB|CA|EH|FW|OF|OL|OE|OG|BH|LR|OS|AA|GD|OH|KY|NP|WK|PB|PA|PE|PI|PS|P|PM|PR|RA|RV|RE|R|H|SB|WN|RS|RD|RT|BM|NE|GV|RP|SU|GL|RO|GÜ|RH|EG|RW|PN|SK|MQ|RU|SZ|RI|SL|SM|SC|HR|FZ|VS|SW|SN|CR|SE|SI|SO|LP|SG|NH|SP|IZ|ST|BF|TE|HV|OD|SR|S|AC|DW|ZW|TF|TS|TR|TÜ|UM|PZ|TP|UE|UN|UH|MN|KK|VB|V|AE|PL|RC|VG|GW|PW|VR|VK|KB|WA|WT|BE|WM|WE|AP|MO|WW|FB|WZ|WI|WB|JE|WF|WO|W|WÜ|BL|Z|GC)[- ]?[A-Z]{1,2}[- ]?\d{1,4}|(AIC|FDB|ABG|SLN|SAW|KLZ|BUL|ESB|NAB|SUL|WST|ABI|AZE|BTF|KÖT|DKB|FEU|ROT|ALZ|SMÜ|WER|AUR|NOR|DÜW|BRK|HAB|TÖL|WOR|BAD|BAR|BER|BIW|EBS|KEM|MÜB|PEG|BGL|BGD|REI|WIL|BKS|BIR|WAT|BOR|BOH|BOT|BRB|BLK|HHM|NEB|NMB|WSF|LEO|HDL|WMS|WZL|BÜS|CHA|KÖZ|ROD|WÜM|CLP|NEC|COC|ZEL|COE|CUX|DAH|LDS|DEG|DEL|RSL|DLG|DGF|LAN|HEI|MED|DON|KIB|ROK|JÜL|MON|SLE|EBE|EIC|HIG|WBS|BIT|PRÜ|LIB|EMD|WIT|ERH|HÖS|ERZ|ANA|ASZ|MAB|MEK|STL|SZB|FDS|HCH|HOR|WOL|FRG|GRA|WOS|FRI|FFB|GAP|GER|BRL|CLZ|GTH|NOH|HGW|GRZ|LÖB|NOL|WSW|DUD|HMÜ|OHA|KRU|HAL|HAM|HBS|QLB|HVL|NAU|HAS|EBN|GEO|HOH|HDH|ERK|HER|WAN|HEF|ROF|HBN|ALF|HSK|USI|NAI|REH|SAN|KÜN|ÖHR|HOL|WAR|ARN|BRG|GNT|HOG|WOH|KEH|MAI|PAR|RID|ROL|KLE|GEL|KUS|KYF|ART|SDH|LDK|DIL|MAL|VIB|LER|BNA|GHA|GRM|MTL|WUR|LEV|LIF|STE|WEL|LIP|VAI|LUP|HGN|LBZ|LWL|PCH|STB|DAN|MKK|SLÜ|MSP|TBB|MGH|MTK|BIN|MSH|EIL|HET|SGH|BID|MYK|MSE|MST|MÜR|WRN|MEI|GRH|RIE|MZG|MIL|OBB|BED|FLÖ|MOL|FRW|SEE|SRB|AIB|MOS|BCH|ILL|SOB|NMS|NEA|SEF|UFF|NEW|VOH|NDH|TDO|NWM|GDB|GVM|WIS|NOM|EIN|GAN|LAU|HEB|OHV|OSL|SFB|ERB|LOS|BSK|KEL|BSB|MEL|WTL|OAL|FÜS|MOD|OHZ|OPR|BÜR|PAF|PLÖ|CAS|GLA|REG|VIT|ECK|SIM|GOA|EMS|DIZ|GOH|RÜD|SWA|NES|KÖN|MET|LRO|BÜZ|DBR|ROS|TET|HRO|ROW|BRV|HIP|PAN|GRI|SHK|EIS|SRO|SOK|LBS|SCZ|MER|QFT|SLF|SLS|HOM|SLK|ASL|BBG|SBK|SFT|SHG|MGN|MEG|ZIG|SAD|NEN|OVI|SHA|BLB|SIG|SON|SPN|FOR|GUB|SPB|IGB|WND|STD|STA|SDL|OBG|HST|BOG|SHL|PIR|FTL|SEB|SÖM|SÜW|TIR|SAB|TUT|ANG|SDT|LÜN|LSZ|MHL|VEC|VER|VIE|OVL|ANK|OVP|SBG|UEM|UER|WLG|GMN|NVP|RDG|RÜG|DAU|FKB|WAF|WAK|SLZ|WEN|SOG|APD|WUG|GUN|ESW|WIZ|WES|DIN|BRA|BÜD|WHV|HWI|GHC|WTM|WOB|WUN|MAK|SEL|OCH|HOT|WDA)[- ]?(([A-Z][- ]?\d{1,4})|([A-Z]{2}[- ]?\d{1,3})))[- ]?(E|H)?$/.test(str);
    },
    "de-LI": function deLI(str) {
        return /^FL[- ]?\d{1,5}[UZ]?$/.test(str);
    },
    "fi-FI": function fiFI(str) {
        return /^(?=.{4,7})(([A-Z]{1,3}|[0-9]{1,3})[\s-]?([A-Z]{1,3}|[0-9]{1,5}))$/.test(str);
    },
    "pt-PT": function ptPT(str) {
        return /^([A-Z]{2}|[0-9]{2})[ -·]?([A-Z]{2}|[0-9]{2})[ -·]?([A-Z]{2}|[0-9]{2})$/.test(str);
    },
    "sq-AL": function sqAL(str) {
        return /^[A-Z]{2}[- ]?((\d{3}[- ]?(([A-Z]{2})|T))|(R[- ]?\d{3}))$/.test(str);
    },
    "pt-BR": function ptBR(str) {
        return /^[A-Z]{3}[ -]?[0-9][A-Z][0-9]{2}|[A-Z]{3}[ -]?[0-9]{4}$/.test(str);
    }
};
function $a07481227fc91d05$var$isLicensePlate(str, locale) {
    (0, $a07481227fc91d05$var$_assertString.default)(str);
    if (locale in $a07481227fc91d05$var$validators) return $a07481227fc91d05$var$validators[locale](str);
    else if (locale === "any") {
        for(var key in $a07481227fc91d05$var$validators){
            /* eslint guard-for-in: 0 */ var validator = $a07481227fc91d05$var$validators[key];
            if (validator(str)) return true;
        }
        return false;
    }
    throw new Error("Invalid locale '".concat(locale, "'"));
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("29MrQ", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $192200cdc79fa90a$var$isStrongPassword;

var $192200cdc79fa90a$var$_merge = $192200cdc79fa90a$var$_interopRequireDefault((parcelRequire("lIjC8")));

var $192200cdc79fa90a$var$_assertString = $192200cdc79fa90a$var$_interopRequireDefault((parcelRequire("1khYO")));
function $192200cdc79fa90a$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $192200cdc79fa90a$var$upperCaseRegex = /^[A-Z]$/;
var $192200cdc79fa90a$var$lowerCaseRegex = /^[a-z]$/;
var $192200cdc79fa90a$var$numberRegex = /^[0-9]$/;
var $192200cdc79fa90a$var$symbolRegex = /^[-#!$@%^&*()_+|~=`{}\[\]:";'<>?,.\/ ]$/;
var $192200cdc79fa90a$var$defaultOptions = {
    minLength: 8,
    minLowercase: 1,
    minUppercase: 1,
    minNumbers: 1,
    minSymbols: 1,
    returnScore: false,
    pointsPerUnique: 1,
    pointsPerRepeat: 0.5,
    pointsForContainingLower: 10,
    pointsForContainingUpper: 10,
    pointsForContainingNumber: 10,
    pointsForContainingSymbol: 10
};
/* Counts number of occurrences of each char in a string
 * could be moved to util/ ?
*/ function $192200cdc79fa90a$var$countChars(str) {
    var result = {};
    Array.from(str).forEach(function(char) {
        var curVal = result[char];
        if (curVal) result[char] += 1;
        else result[char] = 1;
    });
    return result;
}
/* Return information about a password */ function $192200cdc79fa90a$var$analyzePassword(password) {
    var charMap = $192200cdc79fa90a$var$countChars(password);
    var analysis = {
        length: password.length,
        uniqueChars: Object.keys(charMap).length,
        uppercaseCount: 0,
        lowercaseCount: 0,
        numberCount: 0,
        symbolCount: 0
    };
    Object.keys(charMap).forEach(function(char) {
        /* istanbul ignore else */ if ($192200cdc79fa90a$var$upperCaseRegex.test(char)) analysis.uppercaseCount += charMap[char];
        else if ($192200cdc79fa90a$var$lowerCaseRegex.test(char)) analysis.lowercaseCount += charMap[char];
        else if ($192200cdc79fa90a$var$numberRegex.test(char)) analysis.numberCount += charMap[char];
        else if ($192200cdc79fa90a$var$symbolRegex.test(char)) analysis.symbolCount += charMap[char];
    });
    return analysis;
}
function $192200cdc79fa90a$var$scorePassword(analysis, scoringOptions) {
    var points = 0;
    points += analysis.uniqueChars * scoringOptions.pointsPerUnique;
    points += (analysis.length - analysis.uniqueChars) * scoringOptions.pointsPerRepeat;
    if (analysis.lowercaseCount > 0) points += scoringOptions.pointsForContainingLower;
    if (analysis.uppercaseCount > 0) points += scoringOptions.pointsForContainingUpper;
    if (analysis.numberCount > 0) points += scoringOptions.pointsForContainingNumber;
    if (analysis.symbolCount > 0) points += scoringOptions.pointsForContainingSymbol;
    return points;
}
function $192200cdc79fa90a$var$isStrongPassword(str) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    (0, $192200cdc79fa90a$var$_assertString.default)(str);
    var analysis = $192200cdc79fa90a$var$analyzePassword(str);
    options = (0, $192200cdc79fa90a$var$_merge.default)(options || {}, $192200cdc79fa90a$var$defaultOptions);
    if (options.returnScore) return $192200cdc79fa90a$var$scorePassword(analysis, options);
    return analysis.length >= options.minLength && analysis.lowercaseCount >= options.minLowercase && analysis.uppercaseCount >= options.minUppercase && analysis.numberCount >= options.minNumbers && analysis.symbolCount >= options.minSymbols;
}
module.exports = module.exports.default;
module.exports.default = module.exports.default;

});

parcelRequire.register("crOcW", function(module, exports) {
"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.default = $90fef3de0a32a8d1$var$isVAT;
module.exports.vatMatchers = void 0;

var $90fef3de0a32a8d1$var$_assertString = $90fef3de0a32a8d1$var$_interopRequireDefault((parcelRequire("1khYO")));
function $90fef3de0a32a8d1$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $90fef3de0a32a8d1$var$vatMatchers = {
    GB: /^GB((\d{3} \d{4} ([0-8][0-9]|9[0-6]))|(\d{9} \d{3})|(((GD[0-4])|(HA[5-9]))[0-9]{2}))$/,
    IT: /^(IT)?[0-9]{11}$/,
    NL: /^(NL)?[0-9]{9}B[0-9]{2}$/
};
module.exports.vatMatchers = $90fef3de0a32a8d1$var$vatMatchers;
function $90fef3de0a32a8d1$var$isVAT(str, countryCode) {
    (0, $90fef3de0a32a8d1$var$_assertString.default)(str);
    (0, $90fef3de0a32a8d1$var$_assertString.default)(countryCode);
    if (countryCode in $90fef3de0a32a8d1$var$vatMatchers) return $90fef3de0a32a8d1$var$vatMatchers[countryCode].test(str);
    throw new Error("Invalid country code: '".concat(countryCode, "'"));
}

});





var $493f82394832f7db$exports = {};
"use strict";
function $493f82394832f7db$var$_typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") $493f82394832f7db$var$_typeof = function _typeof(obj) {
        return typeof obj;
    };
    else $493f82394832f7db$var$_typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
    return $493f82394832f7db$var$_typeof(obj);
}
Object.defineProperty($493f82394832f7db$exports, "__esModule", {
    value: true
});
$493f82394832f7db$exports.default = void 0;

var $493f82394832f7db$var$_toDate = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("5AZVd")));

var $493f82394832f7db$var$_toFloat = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("b9wlP")));

var $493f82394832f7db$var$_toInt = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("aXy21")));

var $493f82394832f7db$var$_toBoolean = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cDMZB")));

var $493f82394832f7db$var$_equals = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("61jgp")));

var $493f82394832f7db$var$_contains = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("5sIaV")));

var $493f82394832f7db$var$_matches = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("g72VB")));

var $493f82394832f7db$var$_isEmail = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("kVDMK")));

var $493f82394832f7db$var$_isURL = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("joKBl")));

var $493f82394832f7db$var$_isMACAddress = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("eYCsE")));

var $493f82394832f7db$var$_isIP = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("gIxCs")));

var $493f82394832f7db$var$_isIPRange = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2rsAP")));

var $493f82394832f7db$var$_isFQDN = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("9NQBl")));

var $493f82394832f7db$var$_isDate = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2KIcT")));

var $493f82394832f7db$var$_isBoolean = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cNmHm")));

var $493f82394832f7db$var$_isLocale = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("a1AWM")));

var $493f82394832f7db$var$_isAlpha = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("gzCZU")));

var $493f82394832f7db$var$_isAlphanumeric = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("7PUYk")));

var $493f82394832f7db$var$_isNumeric = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("8RlnA")));

var $493f82394832f7db$var$_isPassportNumber = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("1c80o")));

var $493f82394832f7db$var$_isPort = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("e2Kqq")));

var $493f82394832f7db$var$_isLowercase = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("aeF61")));

var $493f82394832f7db$var$_isUppercase = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("3DvQa")));

var $493f82394832f7db$var$_isIMEI = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("flo0E")));

var $493f82394832f7db$var$_isAscii = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("1mWYz")));

var $493f82394832f7db$var$_isFullWidth = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7x82U")));

var $493f82394832f7db$var$_isHalfWidth = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("jh7hv")));

var $493f82394832f7db$var$_isVariableWidth = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("1Y32c")));

var $493f82394832f7db$var$_isMultibyte = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("jCxMT")));

var $493f82394832f7db$var$_isSemVer = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2iu3W")));

var $493f82394832f7db$var$_isSurrogatePair = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("hcmv6")));

var $493f82394832f7db$var$_isInt = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7j5R1")));

var $493f82394832f7db$var$_isFloat = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("deri3")));

var $493f82394832f7db$var$_isDecimal = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("bpbov")));

var $493f82394832f7db$var$_isHexadecimal = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("heG7h")));

var $493f82394832f7db$var$_isOctal = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("6vCo5")));

var $493f82394832f7db$var$_isDivisibleBy = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("8eteF")));

var $493f82394832f7db$var$_isHexColor = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2eDDw")));

var $493f82394832f7db$var$_isRgbColor = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("lYDV0")));

var $493f82394832f7db$var$_isHSL = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("hYcXt")));

var $493f82394832f7db$var$_isISRC = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2Kqyz")));

var $493f82394832f7db$var$_isIBAN = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("4xd2a")));

var $493f82394832f7db$var$_isBIC = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("5OY2Z")));

var $493f82394832f7db$var$_isMD = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("bEev4")));

var $493f82394832f7db$var$_isHash = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("lekj3")));

var $493f82394832f7db$var$_isJWT = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2JO4r")));

var $493f82394832f7db$var$_isJSON = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("hEi2B")));

var $493f82394832f7db$var$_isEmpty = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("8PGgv")));

var $493f82394832f7db$var$_isLength = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("ennxN")));

var $493f82394832f7db$var$_isByteLength = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("bOAiS")));

var $493f82394832f7db$var$_isUUID = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("5UlZM")));

var $493f82394832f7db$var$_isMongoId = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("feSlP")));

var $493f82394832f7db$var$_isAfter = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("9WiHS")));

var $493f82394832f7db$var$_isBefore = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("3B2a9")));

var $493f82394832f7db$var$_isIn = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("6lIlX")));

var $493f82394832f7db$var$_isCreditCard = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7hMzn")));

var $493f82394832f7db$var$_isIdentityCard = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("1YYIG")));

var $493f82394832f7db$var$_isEAN = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7q6Jj")));

var $493f82394832f7db$var$_isISIN = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("2CQ0D")));

var $493f82394832f7db$var$_isISBN = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("jwNFV")));

var $493f82394832f7db$var$_isISSN = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("dnaPy")));

var $493f82394832f7db$var$_isTaxID = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7jB1i")));

var $493f82394832f7db$var$_isMobilePhone = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("foPrj")));

var $493f82394832f7db$var$_isEthereumAddress = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("8FnCl")));

var $493f82394832f7db$var$_isCurrency = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cQXsU")));

var $493f82394832f7db$var$_isBtcAddress = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("6Ej0A")));

var $493f82394832f7db$var$_isISO = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("OU6T2")));

var $493f82394832f7db$var$_isRFC = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("8bbyw")));

var $493f82394832f7db$var$_isISO31661Alpha = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("estEF")));

var $493f82394832f7db$var$_isISO31661Alpha2 = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7Q6iF")));

var $493f82394832f7db$var$_isISO2 = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("9bsC2")));

var $493f82394832f7db$var$_isBase = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("eXqHg")));

var $493f82394832f7db$var$_isBase2 = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("a64ZZ")));

var $493f82394832f7db$var$_isBase3 = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cVIvk")));

var $493f82394832f7db$var$_isDataURI = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("hwQPp")));

var $493f82394832f7db$var$_isMagnetURI = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("j5m7M")));

var $493f82394832f7db$var$_isMimeType = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("ejOce")));

var $493f82394832f7db$var$_isLatLong = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("iO9To")));

var $493f82394832f7db$var$_isPostalCode = $493f82394832f7db$var$_interopRequireWildcard((parcelRequire("6D7eY")));

var $493f82394832f7db$var$_ltrim = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("eV26l")));

var $493f82394832f7db$var$_rtrim = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cdqrX")));

var $493f82394832f7db$var$_trim = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("fDF1p")));

var $493f82394832f7db$var$_escape = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("7VXXJ")));

var $493f82394832f7db$var$_unescape = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("aKIrY")));

var $493f82394832f7db$var$_stripLow = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("3I1Oz")));

var $493f82394832f7db$var$_whitelist = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("6qSw4")));

var $493f82394832f7db$var$_blacklist = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("cllFM")));

var $493f82394832f7db$var$_isWhitelisted = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("gdekX")));

var $493f82394832f7db$var$_normalizeEmail = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("aSY8z")));

var $493f82394832f7db$var$_isSlug = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("gRAWw")));

var $493f82394832f7db$var$_isLicensePlate = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("dM67q")));

var $493f82394832f7db$var$_isStrongPassword = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("29MrQ")));

var $493f82394832f7db$var$_isVAT = $493f82394832f7db$var$_interopRequireDefault((parcelRequire("crOcW")));
function $493f82394832f7db$var$_getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();
    $493f82394832f7db$var$_getRequireWildcardCache = function _getRequireWildcardCache() {
        return cache;
    };
    return cache;
}
function $493f82394832f7db$var$_interopRequireWildcard(obj) {
    if (obj && obj.__esModule) return obj;
    if (obj === null || $493f82394832f7db$var$_typeof(obj) !== "object" && typeof obj !== "function") return {
        default: obj
    };
    var cache = $493f82394832f7db$var$_getRequireWildcardCache();
    if (cache && cache.has(obj)) return cache.get(obj);
    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;
    for(var key in obj)if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;
        if (desc && (desc.get || desc.set)) Object.defineProperty(newObj, key, desc);
        else newObj[key] = obj[key];
    }
    newObj.default = obj;
    if (cache) cache.set(obj, newObj);
    return newObj;
}
function $493f82394832f7db$var$_interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
        default: obj
    };
}
var $493f82394832f7db$var$version = "13.7.0";
var $493f82394832f7db$var$validator = {
    version: $493f82394832f7db$var$version,
    toDate: $493f82394832f7db$var$_toDate.default,
    toFloat: $493f82394832f7db$var$_toFloat.default,
    toInt: $493f82394832f7db$var$_toInt.default,
    toBoolean: $493f82394832f7db$var$_toBoolean.default,
    equals: $493f82394832f7db$var$_equals.default,
    contains: $493f82394832f7db$var$_contains.default,
    matches: $493f82394832f7db$var$_matches.default,
    isEmail: $493f82394832f7db$var$_isEmail.default,
    isURL: $493f82394832f7db$var$_isURL.default,
    isMACAddress: $493f82394832f7db$var$_isMACAddress.default,
    isIP: $493f82394832f7db$var$_isIP.default,
    isIPRange: $493f82394832f7db$var$_isIPRange.default,
    isFQDN: $493f82394832f7db$var$_isFQDN.default,
    isBoolean: $493f82394832f7db$var$_isBoolean.default,
    isIBAN: $493f82394832f7db$var$_isIBAN.default,
    isBIC: $493f82394832f7db$var$_isBIC.default,
    isAlpha: $493f82394832f7db$var$_isAlpha.default,
    isAlphaLocales: $493f82394832f7db$var$_isAlpha.locales,
    isAlphanumeric: $493f82394832f7db$var$_isAlphanumeric.default,
    isAlphanumericLocales: $493f82394832f7db$var$_isAlphanumeric.locales,
    isNumeric: $493f82394832f7db$var$_isNumeric.default,
    isPassportNumber: $493f82394832f7db$var$_isPassportNumber.default,
    isPort: $493f82394832f7db$var$_isPort.default,
    isLowercase: $493f82394832f7db$var$_isLowercase.default,
    isUppercase: $493f82394832f7db$var$_isUppercase.default,
    isAscii: $493f82394832f7db$var$_isAscii.default,
    isFullWidth: $493f82394832f7db$var$_isFullWidth.default,
    isHalfWidth: $493f82394832f7db$var$_isHalfWidth.default,
    isVariableWidth: $493f82394832f7db$var$_isVariableWidth.default,
    isMultibyte: $493f82394832f7db$var$_isMultibyte.default,
    isSemVer: $493f82394832f7db$var$_isSemVer.default,
    isSurrogatePair: $493f82394832f7db$var$_isSurrogatePair.default,
    isInt: $493f82394832f7db$var$_isInt.default,
    isIMEI: $493f82394832f7db$var$_isIMEI.default,
    isFloat: $493f82394832f7db$var$_isFloat.default,
    isFloatLocales: $493f82394832f7db$var$_isFloat.locales,
    isDecimal: $493f82394832f7db$var$_isDecimal.default,
    isHexadecimal: $493f82394832f7db$var$_isHexadecimal.default,
    isOctal: $493f82394832f7db$var$_isOctal.default,
    isDivisibleBy: $493f82394832f7db$var$_isDivisibleBy.default,
    isHexColor: $493f82394832f7db$var$_isHexColor.default,
    isRgbColor: $493f82394832f7db$var$_isRgbColor.default,
    isHSL: $493f82394832f7db$var$_isHSL.default,
    isISRC: $493f82394832f7db$var$_isISRC.default,
    isMD5: $493f82394832f7db$var$_isMD.default,
    isHash: $493f82394832f7db$var$_isHash.default,
    isJWT: $493f82394832f7db$var$_isJWT.default,
    isJSON: $493f82394832f7db$var$_isJSON.default,
    isEmpty: $493f82394832f7db$var$_isEmpty.default,
    isLength: $493f82394832f7db$var$_isLength.default,
    isLocale: $493f82394832f7db$var$_isLocale.default,
    isByteLength: $493f82394832f7db$var$_isByteLength.default,
    isUUID: $493f82394832f7db$var$_isUUID.default,
    isMongoId: $493f82394832f7db$var$_isMongoId.default,
    isAfter: $493f82394832f7db$var$_isAfter.default,
    isBefore: $493f82394832f7db$var$_isBefore.default,
    isIn: $493f82394832f7db$var$_isIn.default,
    isCreditCard: $493f82394832f7db$var$_isCreditCard.default,
    isIdentityCard: $493f82394832f7db$var$_isIdentityCard.default,
    isEAN: $493f82394832f7db$var$_isEAN.default,
    isISIN: $493f82394832f7db$var$_isISIN.default,
    isISBN: $493f82394832f7db$var$_isISBN.default,
    isISSN: $493f82394832f7db$var$_isISSN.default,
    isMobilePhone: $493f82394832f7db$var$_isMobilePhone.default,
    isMobilePhoneLocales: $493f82394832f7db$var$_isMobilePhone.locales,
    isPostalCode: $493f82394832f7db$var$_isPostalCode.default,
    isPostalCodeLocales: $493f82394832f7db$var$_isPostalCode.locales,
    isEthereumAddress: $493f82394832f7db$var$_isEthereumAddress.default,
    isCurrency: $493f82394832f7db$var$_isCurrency.default,
    isBtcAddress: $493f82394832f7db$var$_isBtcAddress.default,
    isISO8601: $493f82394832f7db$var$_isISO.default,
    isRFC3339: $493f82394832f7db$var$_isRFC.default,
    isISO31661Alpha2: $493f82394832f7db$var$_isISO31661Alpha.default,
    isISO31661Alpha3: $493f82394832f7db$var$_isISO31661Alpha2.default,
    isISO4217: $493f82394832f7db$var$_isISO2.default,
    isBase32: $493f82394832f7db$var$_isBase.default,
    isBase58: $493f82394832f7db$var$_isBase2.default,
    isBase64: $493f82394832f7db$var$_isBase3.default,
    isDataURI: $493f82394832f7db$var$_isDataURI.default,
    isMagnetURI: $493f82394832f7db$var$_isMagnetURI.default,
    isMimeType: $493f82394832f7db$var$_isMimeType.default,
    isLatLong: $493f82394832f7db$var$_isLatLong.default,
    ltrim: $493f82394832f7db$var$_ltrim.default,
    rtrim: $493f82394832f7db$var$_rtrim.default,
    trim: $493f82394832f7db$var$_trim.default,
    escape: $493f82394832f7db$var$_escape.default,
    unescape: $493f82394832f7db$var$_unescape.default,
    stripLow: $493f82394832f7db$var$_stripLow.default,
    whitelist: $493f82394832f7db$var$_whitelist.default,
    blacklist: $493f82394832f7db$var$_blacklist.default,
    isWhitelisted: $493f82394832f7db$var$_isWhitelisted.default,
    normalizeEmail: $493f82394832f7db$var$_normalizeEmail.default,
    toString: toString,
    isSlug: $493f82394832f7db$var$_isSlug.default,
    isStrongPassword: $493f82394832f7db$var$_isStrongPassword.default,
    isTaxID: $493f82394832f7db$var$_isTaxID.default,
    isDate: $493f82394832f7db$var$_isDate.default,
    isLicensePlate: $493f82394832f7db$var$_isLicensePlate.default,
    isVAT: $493f82394832f7db$var$_isVAT.default,
    ibanLocales: $493f82394832f7db$var$_isIBAN.locales
};
var $493f82394832f7db$var$_default = $493f82394832f7db$var$validator;
$493f82394832f7db$exports.default = $493f82394832f7db$var$_default;
$493f82394832f7db$exports = $493f82394832f7db$exports.default;
$493f82394832f7db$exports.default = $493f82394832f7db$exports.default;


var $512613655505982f$exports = {};
"use strict";
Object.defineProperty($512613655505982f$exports, "__esModule", {
    value: true
});
$512613655505982f$exports.OperationalError = void 0;
class $512613655505982f$var$OperationalError extends Error {
    constructor(message, description){
        super(message + ". " + description);
        this.message = message;
        this.description = description;
    }
}
$512613655505982f$exports.OperationalError = $512613655505982f$var$OperationalError;


var $4b82b17c40631547$exports = {};
"use strict";
var $4b82b17c40631547$var$__importDefault = $4b82b17c40631547$exports && $4b82b17c40631547$exports.__importDefault || function(mod) {
    return mod && mod.__esModule ? mod : {
        "default": mod
    };
};
Object.defineProperty($4b82b17c40631547$exports, "__esModule", {
    value: true
});
$4b82b17c40631547$exports.getDirContent = $4b82b17c40631547$exports.writeFile = $4b82b17c40631547$exports.fileToList = $4b82b17c40631547$exports.readFileFromStream = $4b82b17c40631547$exports.readFile = void 0;


const $4b82b17c40631547$var$readline_1 = $4b82b17c40631547$var$__importDefault($ifjKf$readline);
const $4b82b17c40631547$var$readFile = async (path, until = [], exclude = [])=>{
    if (until.length === 0) return [
        (await $ifjKf$fs.promises.readFile(path)).toString(),
        ""
    ];
    const fileStream = (0, $ifjKf$fs.createReadStream)(path);
    const rl = $4b82b17c40631547$var$readline_1.default.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    return await $4b82b17c40631547$var$readFileFromStream(rl, until, exclude);
};
$4b82b17c40631547$exports.readFile = $4b82b17c40631547$var$readFile;
const $4b82b17c40631547$var$readFileFromStream = async (rl, until = [], exclude = [])=>{
    const lines = [];
    const rest = [];
    let cur = lines;
    for await (const line of rl){
        if (until.some((elem)=>line.includes(elem))) cur = rest;
        else if (exclude.some((elem)=>line.includes(elem))) ;
        else cur.push(line);
    }
    return [
        lines.join("\n"),
        rest.join("\n")
    ];
};
$4b82b17c40631547$exports.readFileFromStream = $4b82b17c40631547$var$readFileFromStream;
const $4b82b17c40631547$var$fileToList = async (path)=>{
    const [content, _] = await $4b82b17c40631547$var$readFile(path);
    return content.trim().split("\n");
};
$4b82b17c40631547$exports.fileToList = $4b82b17c40631547$var$fileToList;
const $4b82b17c40631547$var$writeFile = async (path, content)=>{
    await $ifjKf$fs.promises.writeFile(path, content);
};
$4b82b17c40631547$exports.writeFile = $4b82b17c40631547$var$writeFile;
const $4b82b17c40631547$var$getDirContent = async (path)=>{
    return await $ifjKf$fs.promises.readdir(path);
};
$4b82b17c40631547$exports.getDirContent = $4b82b17c40631547$var$getDirContent;


const $287b1134e49a3102$var$paths = {
    songs: "",
    pdf: "",
    tex: ""
};
let $287b1134e49a3102$var$index = [];
const $287b1134e49a3102$export$80d376111cc09ad7 = async (receipe)=>{
    receipe = $287b1134e49a3102$var$validateReceipe(receipe);
    $287b1134e49a3102$var$readPaths(receipe);
    $287b1134e49a3102$var$index = await $287b1134e49a3102$var$readIndex();
    console.debug("Songs:", receipe.songs);
    console.debug("Options:", receipe.options);
    await $287b1134e49a3102$var$createDirs();
    const songs = await $287b1134e49a3102$var$getSongsStatus(receipe.songs);
    console.log("Parsed songs:", songs);
    await $287b1134e49a3102$var$checkInexistingSongs(songs);
    await $287b1134e49a3102$var$copyAndTransposeSongs(songs);
    await $287b1134e49a3102$var$writeSongsList(songs);
    try {
        return {
            result: true,
            ...await $287b1134e49a3102$var$generatePdf(receipe, songs)
        };
    } catch (err) {
        console.error(err);
        if (err instanceof (0, $512613655505982f$exports.OperationalError)) return {
            result: false,
            message: err.message,
            description: err.message
        };
        else if (err instanceof Error) return {
            result: false,
            message: err.name,
            description: err.message
        };
        else return {
            result: false,
            message: "Error",
            description: JSON.stringify(err)
        };
    }
};
const $287b1134e49a3102$var$readIndex = async ()=>{
    const [indexFile, _] = await (0, $4b82b17c40631547$exports.readFile)(`${$287b1134e49a3102$var$paths.songs}/songs.json`);
    return JSON.parse(indexFile);
};
const $287b1134e49a3102$var$validateReceipe = (receipe)=>{
    const songs = receipe.songs.map((song)=>{
        if (song.transpose !== undefined && !((0, (/*@__PURE__*/$parcel$interopDefault($493f82394832f7db$exports))).isNumeric(song.transpose) && Number(song.transpose) >= -5 && Number(song.transpose) <= 5)) throw new (0, $512613655505982f$exports.OperationalError)("Invalid transposition", `Found ${song.transpose} for song ${song.id}`);
        return {
            transpose: song.transpose,
            id: (0, (/*@__PURE__*/$parcel$interopDefault($493f82394832f7db$exports))).whitelist(song.id, "[w\n_]+")
        };
    });
    const columns = receipe.options.columns;
    if (columns !== undefined && !((0, (/*@__PURE__*/$parcel$interopDefault($493f82394832f7db$exports))).isNumeric(columns) && Number(columns) <= 3 && Number(columns) >= 1)) throw new (0, $512613655505982f$exports.OperationalError)("Invalid number of columns", `Expected a number between 1 and 3, found ${columns}`);
    return {
        songs: songs,
        options: receipe.options
    };
};
const $287b1134e49a3102$var$readPaths = (receipe)=>{
    $287b1134e49a3102$var$paths.songs = receipe.options.songs;
    $287b1134e49a3102$var$paths.pdf = receipe.options.out;
    $287b1134e49a3102$var$paths.tex = receipe.options.tex;
};
const $287b1134e49a3102$var$createDirs = async ()=>{
    const dirs = [
        $287b1134e49a3102$var$paths.pdf,
        `${$287b1134e49a3102$var$paths.pdf}/canzonieri`,
        `${$287b1134e49a3102$var$paths.pdf}/tmp`
    ];
    let alreadyExists = false;
    for (const d of dirs)try {
        await (0, $ifjKf$fs.promises).mkdir(d);
        alreadyExists = false;
    } catch (error) {
        alreadyExists = true;
    }
    return !alreadyExists;
};
const $287b1134e49a3102$var$checkInexistingSongs = async (songs)=>{
    let verified = false;
    if (songs.length === 0) throw new (0, $512613655505982f$exports.OperationalError)("Empty songs list", "No song was specified");
    const nonExistingSongs = songs.filter((song)=>song.type === "song" && song.content?.exists === false).map((check)=>check.content?.id);
    if (nonExistingSongs.length !== 0) {
        const desc = `Invalid songs list. The following were not found: ${nonExistingSongs.join(",")}`;
        throw new Error(desc);
    }
    verified = true;
    return verified;
};
const $287b1134e49a3102$var$getSongsStatus = async (songs)=>{
    return await Promise.all(songs.map(async (song)=>await $287b1134e49a3102$var$getSongStatus(song)));
};
const $287b1134e49a3102$var$getSongStatus = async (song)=>{
    if (song.id === "sclearpage" || song.id === "brk") return {
        type: song.id
    };
    else if (song.id.trim().length === 0) return {
        type: "space"
    };
    else if (song.id.startsWith("%")) return {
        type: "comment",
        content: {
            id: song.id,
            exists: true,
            transpose: 0
        }
    };
    else {
        const songInIndex = $287b1134e49a3102$var$index.find((songIdx)=>songIdx.id === song.id);
        if (songInIndex !== undefined) return {
            type: "song",
            content: {
                id: song.id,
                exists: true,
                transpose: Number(song?.transpose) ?? 0
            }
        };
        else return {
            type: "song",
            content: {
                id: song.id,
                exists: false,
                transpose: 0
            }
        };
    }
};
const $287b1134e49a3102$var$copyAndTransposeSongs = async (songs)=>{
    const songsPath = `${$287b1134e49a3102$var$paths.songs}/archivio-canzoni`;
    const actualSongs = songs.filter((song)=>song.type === "song");
    for await (const song of actualSongs)if (song.content.transpose !== 0) {
        let [songContent, _] = await (0, $4b82b17c40631547$exports.readFile)(`${songsPath}/${song.content.id}.tex`);
        const matches = songContent.match(/([\w\W\n]+)(\\beginsong{[^}]+}(\[[^\]]+\])?)([\w\W\n]+)/);
        if (matches === null) throw new (0, $512613655505982f$exports.OperationalError)("Invalid song content", `Song ${song.content?.id} is not correctly formatted`);
        else songContent = matches[1] + matches[2] + "\n\\transpose{" + song.content?.transpose + "}" + matches[4];
        await (0, $4b82b17c40631547$exports.writeFile)(`${$287b1134e49a3102$var$paths.pdf}/tmp/${song.content?.id}.tex`, songContent);
    } else (0, $ifjKf$fspromises.copyFile)(`${songsPath}/${song.content?.id}.tex`, `${$287b1134e49a3102$var$paths.pdf}/tmp/${song.content?.id}.tex`);
};
const $287b1134e49a3102$var$writeSongsList = async (songs)=>{
    const formattedSongs = songs.filter((song)=>song.type === "sclearpage" || song.type === "brk" || song.type === "song").map((song)=>{
        if (song.type === "sclearpage") return "\\sclearpage";
        else if (song.type === "brk") return "\\brk";
        else return `\\input{../../${$287b1134e49a3102$var$paths.pdf}/tmp/${song.content?.id}.tex}`;
    }).join("\n");
    await (0, $4b82b17c40631547$exports.writeFile)(`${$287b1134e49a3102$var$paths.tex}/_songs_list.tex`, formattedSongs);
};
const $287b1134e49a3102$var$generatePdf = async (receipe, status)=>{
    const runOptions = await $287b1134e49a3102$var$buildDocumentClass(receipe);
    let jobName = receipe.songs[0].id;
    if (runOptions.format !== "singoli") jobName = `canzoniere_${$287b1134e49a3102$var$generateHash(status)}`;
    const outDir = runOptions.format === "singoli" ? $287b1134e49a3102$var$paths.pdf : `${$287b1134e49a3102$var$paths.pdf}/canzonieri`;
    let command = `cd ${$287b1134e49a3102$var$paths.tex} && pdflatex -shell-escape -jobname ${jobName} -output-directory ../../${outDir} canzoniere.tex`;
    if (receipe.options?.runner === "tectonic") command = `tectonic --outdir ${outDir} ${$287b1134e49a3102$var$paths.tex}/canzoniere.tex`;
    console.log(`Executing ${command}`);
    try {
        (0, $ifjKf$child_process.execSync)(command, {
            timeout: 30000
        });
        $287b1134e49a3102$var$removeTmpFiles(outDir, jobName);
        return {
            path: `${outDir}/${jobName}.pdf`
        };
    } catch (err) {
        console.log(err);
        $287b1134e49a3102$var$removeTmpFiles(outDir, jobName, false);
        if ("status" in err && err.status === "ETIMEDOUT") throw new (0, $512613655505982f$exports.OperationalError)("Timeout reached while running pdflatex", "See the logs in the output directory");
        else throw new (0, $512613655505982f$exports.OperationalError)("Error while running pdflatex", `Error Code: ${err?.status ?? "unkwnown"}. See the logs in the output directory`);
    }
};
const $287b1134e49a3102$var$buildDocumentClass = async (receipe)=>{
    const mapper = new Map([
        [
            "format",
            {
                name: "__value__"
            }
        ],
        [
            "chords",
            {
                ifTrue: "chorded",
                ifFalse: "lyric"
            }
        ],
        [
            "font",
            {
                name: "__value__"
            }
        ],
        [
            "minorsign",
            {
                prefix: "minorsign=",
                name: "__value__"
            }
        ],
        [
            "columns",
            {
                prefix: "columns=",
                name: "__value__"
            }
        ],
        [
            "cover",
            {
                ifTrue: "cover",
                ifFalse: "nocover"
            }
        ],
        [
            "indexes",
            {
                suffix: "index",
                name: "__join__"
            }
        ], 
    ]);
    const runOptions = {
        format: "standard",
        chords: "chorded",
        font: "libertinus",
        minorsign: "minorsign=-",
        columns: "columns=2",
        cover: "cover",
        indexes: ""
    };
    for (const option of mapper.keys())if (option in receipe.options) {
        let content = receipe.options[option];
        if (mapper.get(option)?.ifTrue !== undefined) content = content === "true" ? mapper.get(option)?.ifTrue : mapper.get(option)?.ifFalse;
        let arrContent = [];
        if (mapper.get(option)?.name === "__join__") arrContent = content.split(",");
        else if (!Array.isArray(content)) arrContent = [
            content
        ];
        else arrContent = content;
        if (mapper.get(option)?.prefix !== undefined) arrContent = arrContent.map((each)=>mapper.get(option)?.prefix + each);
        if (mapper.get(option)?.suffix !== undefined) arrContent = arrContent.map((each)=>each + mapper.get(option)?.suffix);
        runOptions[option] = arrContent.join(",");
    }
    if (receipe.songs.length === 1) runOptions.format = "singoli";
    await (0, $4b82b17c40631547$exports.writeFile)(`${$287b1134e49a3102$var$paths.tex}/_preamble.tex`, `\\documentclass[${Object.values(runOptions).join(",")}]{canzoniereonline}`);
    console.debug("Latex options:", runOptions);
    return runOptions;
};
const $287b1134e49a3102$var$generateHash = (status)=>{
    const sha = (0, $ifjKf$crypto.createHash)("sha1");
    const payload = status.filter((song)=>song.type === "song").map((song)=>song.content?.id ?? "").join("");
    return sha.update(payload).digest("hex");
};
const $287b1134e49a3102$var$removeTmpFiles = async (outDir, jobName, exitedSuccessfully = true)=>{
    const extensions = [
        exitedSuccessfully ? [
            "log"
        ] : [],
        "aux",
        "out",
        "idx",
        "ilg",
        "ind", 
    ].map((ext)=>`${jobName}.${ext}`);
    const allFiles = [
        ...extensions,
        "autori.idx",
        "autori.ind",
        "autori.ilg",
        "tematico.idx",
        "tematico.ind",
        "tematico.ilg", 
    ];
    allFiles.forEach(async (file)=>{
        try {
            await (0, $ifjKf$fs.promises).unlink(`${outDir}/${file}`);
        } catch (e) {}
    });
    (0, $ifjKf$fs.promises).rm(`${$287b1134e49a3102$var$paths.pdf}/tmp`, {
        recursive: true
    });
};




const $da9ffd6d191262fd$export$72c04af63de9061a = async (path, until = [], exclude = [])=>{
    const fileStream = (0, $ifjKf$fs.createReadStream)(path);
    const rl = (0, ($parcel$interopDefault($ifjKf$readline))).createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    return await $da9ffd6d191262fd$export$d10960a3a64294b6(rl, until, exclude);
};
const $da9ffd6d191262fd$export$d10960a3a64294b6 = async (rl, until = [], exclude = [])=>{
    const lines = [];
    const rest = [];
    let cur = lines;
    for await (const line of rl){
        if (until.some((elem)=>line.includes(elem))) cur = rest;
        else if (exclude.some((elem)=>line.includes(elem))) ;
        else cur.push(line);
    }
    return [
        lines.join("\n"),
        rest.join("\n")
    ];
};
const $da9ffd6d191262fd$export$7f383fb8510d2ba3 = async (path)=>{
    const [content, _] = await $da9ffd6d191262fd$export$72c04af63de9061a(path);
    return content.trim().split("\n");
};
const $da9ffd6d191262fd$export$552bfb764b5cd2b4 = async (path, content)=>{
    await (0, $ifjKf$fs.promises).writeFile(path, content);
};
const $da9ffd6d191262fd$export$c4d30b20cd3191de = async (path)=>{
    return await (0, $ifjKf$fs.promises).readdir(path);
};



const $c54b986c70570c10$var$help = ()=>{
    return `
        Canzoniereonline - Command Line Interface

        Usage: canzoniere_cli [OPTIONS] SONGS
        OPTIONS: valid options
        SONGS: space-separated list of song identifiers
    `;
};
const $c54b986c70570c10$var$run = async ()=>{
    const args = $ifjKf$process.argv.slice(2);
    let songs = args.filter((arg)=>!arg.startsWith("--"));
    const options = args.filter((arg)=>arg.startsWith("--")).map((arg)=>arg.replace("--", "")).map((arg)=>arg.includes("=") ? arg.split("=") : [
            arg,
            arg
        ]);
    const optionsObj = Object.fromEntries(options);
    if (optionsObj.help !== undefined) {
        console.log($c54b986c70570c10$var$help());
        return {
            result: true
        };
    } else if (optionsObj.file !== undefined) songs = await (0, $da9ffd6d191262fd$export$7f383fb8510d2ba3)(optionsObj.file);
    const result = await (0, $287b1134e49a3102$export$80d376111cc09ad7)({
        songs: songs.map((s)=>{
            const [id, transpose] = s.split("+");
            return {
                id: id,
                transpose: transpose ?? "0"
            };
        }),
        options: optionsObj
    });
    return result;
};
const $c54b986c70570c10$var$runAndOutput = async ()=>{
    const result = await $c54b986c70570c10$var$run();
    console.log(result);
    return result;
};
$c54b986c70570c10$var$runAndOutput();


//# sourceMappingURL=canzoniere_cli.js.map
